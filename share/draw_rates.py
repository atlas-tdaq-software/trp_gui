#!/usr/bin/env python
# version: 0.3
# author:  Tomasz Bold

import sys
import os
import re
import optparse
import commands
import string
from datetime import datetime
from datetime import timedelta
import time
import coca
import ROOT

preamble=""" %prog options
This is the script to draw rates from TRP archive.
The archive is actually ROOT file with TTrees.

The archives can be accessed through root deamon  (rootd) at P1 or they have to be local files.
Default is to access archive from last run through rootd.

The script by default gives python prompt after shor help info.
Rates can be added to teh plot interacivelly.

Example: use archive
draw_rates.py -a 1run - last run (default)
draw_rates.py -a 4runs - last 4 runs

draw_rates.py -a 1day  - last 1 day
draw_rates.py -a 2days - last 2 days  (max 7 days)

draw_rates.py -a 123430         - explicit run number
draw_rates.py -a 123430-123440  - explicit runs range
draw_rates.py -a 123430,1234302 - explicit run list
draw_rates.py -a 123430-        - open ended range of runs 

Example: use local file
draw_rates.py -f TriggerRates_ATLAS_132916.root

Example: list possible rate tables
draw_rates.py -l

Example: draw total output rate of HLT
draw_rates.py -r \"HLT HLT_total_output\" 

Example (default): draw total_output and total_input rate of L2 & EF
draw_rates.py -r \"L2 L2_tot.*put, EF Ef_tot.*put\"

Example:  draw output rate of all inclusive chain groups at HLT
draw_rates.py -r \"HLT grp_Inclusive_.*output\"

Example: draw input/output rate  of single chain group at L2 & EF
draw_rates.py -r \"L2 grp_Inclusive_Muons_.*put, EF grp_Inclusive_Muons_.*put\"

Example: draw input rate  of all muon related chains
draw_rates.py -r \"HLT HLT_.*mu.*input\"

"""

optparser = optparse.OptionParser(usage=preamble)
optparser.set_defaults(verbose=0, dataset="TRP-Rates", connStr=coca.defConnStr, dest=".")

optparser.add_option("-f", "--file",
                     help="read file (can be coma space separated list)", metavar="FILE")

optparser.add_option("-a", "--archive", default="1run",
                     help="read data from archive with specified range of runs (dfault 1run)")

optparser.add_option('-c', dest='connStr',  metavar="STRING",
                      help="connection string [%default]")

optparser.add_option('-d', dest='dataset', metavar="STRING",
                      help="dataset name [%default]")

optparser.add_option("-l", "--list", action="store_true",  default=False,
                     help="list rates in file")

optparser.add_option("-r", "--rates", nargs=1,
                     help="rates to plot, rate can be specified with regex i.e. tot.* or .*output", default="HLT HLT_tot.*put",
                     metavar="\"TREE1 rate1[, TREE2 rate2]\"")

optparser.add_option("-o", "--output", nargs=1, default="rate.png",
                     help="Output graphics file; file suffix will determine format [jpg|png|root|C|svg|pdf|ps|eps]",
                     metavar="FILE.SUFFIX")

optparser.add_option("-q", "--quiet", action="store_true", default=False,
                     help="quiet mode, make drawing store file and quit, otherwise enter interactive mode (good for fine tunning the plot)")

optparser.add_option("", "--nozero", action="store_true", default=False,
                     help="Supress zero rates")

optparser.add_option("", "--nosmall", action="store_true", default=False,
                     help="Supress rates which in average are smaller than threshold")

optparser.add_option("", "--noextralabels", action="store_true", default=False,
                     help="Supress LBN & run labels overlayed on the axis")

optparser.add_option("", "--zero", default=1.e-5, type='float',
                     help="Small rates threshold, if below that valye they will be supressed on the plots")

optparser.add_option("", "--zoom", default="",
                     help="Zoom in: i.e. \"LumiBlock>20\" or \"Run>141810\"")

optparser.add_option("", "--smooth", type="int", default=0,
                     help="Use TProfile with SMOOTH number of bins instead of TGraph")

optparser.add_option("", "--dump", nargs=1, default="",
                     help="Dump rates in CSV text file", metavar="TEXTFILE")

optparser.add_option("", "--modifier", nargs=1, default="",
                     help="Python script to customize plots", metavar="SCRIPTFILE")



optparser.disable_interspersed_args()

(options, args) = optparser.parse_args()

class archives_options(object):
    cocapath = []
    p1_location = '/data/trp'
    atp1 = os.environ.get('TDAQ_SETUP_POINT1',0)

class CocaArchive(object):
    def all_runs(self):

        runrange = options.archive
        cocainfo = False

        if 'day' in runrange or 'run' in runrange:
            cocainfo = True
        elif '-' in runrange:
            runs = [ n for n in runrange.split('-')]
            if runs[1] == '':
                cocainfo = True
            else:
                firstrun = int(runs[0])
                lastrun  = int(runs[1])+1
                runs = range(firstrun, lastrun)
        elif ',' in runrange:
            runs = runrange.split(',')
        elif isinstance(int(runrange), int):
            firstrun = int(runrange)
            lastrun  = int(runrange)+1
            runs = range(firstrun, lastrun)
        else:
            pass        

        trpruns = []
        db = coca.DBClient(options.connStr)
        locstr = 'tdq-mon' if archives_options.atp1 else '/eos/'
        loc = []
        if cocainfo:
            # get all runs from the last week (now hard coded)
            since = int(time.time())-3600*24*7
            for f in db.files(options.dataset,'*','TriggerRates_ATLAS*',since=since):
                loc += db.fileLocations(f.relPath(),options.dataset)
        else:
    	    for i in runs:
                fname = 'TriggerRates_ATLAS_%s.root' % str(i)
                loc += db.fileLocations(fname, options.dataset)
            
        # Filter depending on location (eos vs pc-tdq-mon)
        trpruns += filter(lambda x:x.find(locstr)!=-1, loc)
                        
        if len(trpruns) > 0:
           archives_options.cocapath = trpruns[0].rsplit('/',1)[:-1]

        namedate = []
 
        eosls_cmd = 'GROUP=zp /afs/cern.ch/project/eos/installation/atlas/bin/eos.select fileinfo '
        post_cmd = ' | grep Change'

        print "... archive scanned "
        print "... number of runs scanned "+str(len(trpruns))
        
        for f in trpruns: 
           if not archives_options.atp1:
               statusline = commands.getoutput( eosls_cmd+f[24:]+post_cmd ).split(' ')
               timestamp = float(statusline[-1])
               fdate = datetime.fromtimestamp(timestamp)
               namedate.append( (f, fdate) )
           else:   # at P1
               trp_file = f.rsplit('/',1)[1]
               afs_cmd = 'ssh -q pc-tdq-mon-69.cern.ch \"/bin/ls -ltr --time-style=long-iso '+\
                  archives_options.p1_location+'/'+trp_file[:25]+'*\"'
               result = commands.getoutput(afs_cmd)
               sp = result.split()
               dfrag = ' '.join(sp[5:7]).strip()
               fdate = datetime.strptime( dfrag, '%Y-%m-%d %H:%M')
               namedate.append( (f, fdate) )

        return namedate        


class trp_archives:

    def __init__(self):
        archive = CocaArchive()
        self.archive = archive


    def __range_specifier_decoded(self, range):
        if 'day' in range:
            return 'lastdays',  "".join([c for c in range if c in string.digits])
        elif 'run' in range:
            return 'lastruns', [c for c in range if c in string.digits]
        elif '-' in range:
            runs = [ n for n in range.split('-')]
            if runs[1] == '':
                runs[1] = '1000000'
            return 'runsrange', runs
        elif ',' in range:
            runs = range.split(',')
            return 'runlist', runs
        else:
            return 'run', range


    def __get_from_archive(self, last):
        decoded = self.__range_specifier_decoded(last)
        print "... runs range decoded:", decoded

        result = []
        namedate = self.archive.all_runs()

        if decoded[0] == 'run':
            result = [archives_options.cocapath[0]+'/'+'TriggerRates_ATLAS_%s.root' % decoded[1]]
        elif decoded[0] == 'lastruns':
            result =  [ e[0]  for e in namedate[-1*int(decoded[1][0]):] ]
        elif decoded[0] == 'runsrange':
            inrange=False
            for f in namedate:
                if inrange:
                    result.append(f[0])
                if decoded[1][0] in f[0]:
                    inrange=True
                    result.append(f[0])
                if decoded[1][1] in f[0]:
                    break
        elif decoded[0] == 'runlist':
            result = [archives_options.cocapath[0]+'/'+'TriggerRates_ATLAS_%s.root' % r for r in decoded[1]]            
        elif decoded[0] == 'lastdays' or decoded[0] == 'lasthours':
            now = datetime.now()            
            for f in namedate:
                interval=now-f[1]
                if  interval.days < int(decoded[1]):
                    result.append(f[0])
                    
        return result


    def get_files(self, range='1run'):
        """
        Gets list of files from TRP archives.
        The files are prefixed by host and protocol specifier according to the archives_options.
        
        The range can be specified in few ways:
        explicitly:
          134523
          
        range of runs:
          134523-134550

        open ended range of runs:
          134523-

        comma-separate list:
          134523,134525
          
        last runs:
          1run - last run
          3runs - last 3 runs

        last days:
          1day  - lat day
          7days - last week (max.), otherwise specify run range
        """
       
        files = self.__get_from_archive(range)
        return files

    #def get_runs(self, last):
    #    decoded = self.__range_specifier_decoded(last)
    #    print "... runs range decoded:", decoded
    #    return decoded

class ShapeAndColor:
    __shapes = [ROOT.kFullCircle,
                ROOT.kOpenSquare, ROOT.kOpenTriangleUp, 
                ROOT.kOpenCircle, ROOT.kOpenStar,
                ROOT.kOpenDiamond,                 
                ROOT.kPlus, ROOT.kStar, ROOT.kCircle, ROOT.kMultiply ]
    
    __colors = [ROOT.TColor.GetColor(0, 0, 0),
                ROOT.TColor.GetColor(0xff, 0x33, 0x33),
                ROOT.TColor.GetColor(0x33, 0x66, 0),
                ROOT.TColor.GetColor(0xcc, 0x99, 0x99),
                ROOT.TColor.GetColor(00, 0xcc, 0xff),
                ROOT.TColor.GetColor(0x99, 0x00, 0xcc),
                ROOT.TColor.GetColor(00, 0x99, 0x99),
                ROOT.TColor.GetColor(0x66, 0xff, 0x33),
                ROOT.TColor.GetColor(0xcc, 0xcc, 0xcc)
                ]
    __cl = 0;
    __sh = 0
    __usemarker = "P"
    def reset(self):
        self.__cl = 0
        self.__sh = 0
        self.__usemarker = "P"
        
    def get(self):
        sh, cl = self.__shapes[self.__sh], self.__colors[self.__cl]
        self.__sh += 1; self.__sh %= len(self.__shapes)
        self.__cl += 1; self.__cl %= len(self.__colors)
        linewidth = 0
        if self.__usemarker == "P":
            self.__usemarker = ""
            linewidth = 3
        else:
            self.__usemarker = "P"
            linewidth = 1
        return sh, cl, "P", 2
            #return sh, cl, self.__usemarker, linewidth

ShapeAndColor = ShapeAndColor()


class trp_rates:
    __chains = {}
    __graphs = []
    __profiles = []
    __legend = None
    __canvas = None
    __title = None
    __frameh = None
    __remembered_canvases = []
    __rungraph = None
    __runlabels = []

    __lbngraph = None
    __lbnlables = []
    
    __maxrate = 0.
    __minrate = 0.
    __mintime = time.time()    
    __maxtime = 0
    __minrun = 0
    __maxrun = 0

    __zoom = ""
    __last_draw = ""
    __sp = None

    __vv = ROOT.TVectorT('double')()

    def title(self):
        return self.__title

    def legend(self):
        return self.__make_legend()

    def canvas(self):        
        return self.__make_canvas()


    def frame(self):
        return self.__frameh   

    def __init__(self, chains):
        self.__chains.update([(c.GetTitle(), c) for c in chains])
        ROOT.gROOT.SetStyle('Plain')
        ROOT.gStyle.SetOptTitle(0)
        ROOT.gStyle.SetPalette(1)
        ROOT.gStyle.SetOptStat(0)
        

    def __make_legend(self):
        self.__legend = ROOT.TLegend(0.01, 0.01, 0.99, 0.12, "", "NDC")
        self.__legend.SetNColumns(4)
        self.__legend.SetFillColor(ROOT.kWhite)
        self.__legend.SetShadowColor(ROOT.kWhite)
        self.__legend.SetLineColor(ROOT.kWhite)
        self.__legend.SetLineStyle(0)
        self.__legend.SetTextSize(0.04)
        return self.__legend

    def __make_canvas(self):
        if not self.__canvas:
            self.__canvas = ROOT.TCanvas('rate', "Rates", 1100, 500)
            self.__canvas.cd()            
            self.__canvas.SetGridx(1)
            self.__canvas.SetGridy(1)
            self.__canvas.SetBottomMargin(0.25)
            self.__canvas.SetBottomMargin(0.25)
            self.__canvas.SetRightMargin(0.03)
            self.__canvas.SetLeftMargin(0.1)
        
        self.__canvas.Draw()
        return self.__canvas
    
    def __change_times(self, gr):
        changes = []
        points = gr
        runsstarts = []
        time = ROOT.Double()
        val = ROOT.Double()
        pval = -1
        for i in xrange(gr.GetN()):
            gr.GetPoint(i, time, val)
            if val != pval:
                changes.append( (int(time), int(val)) )
            pval = int(val)
        return changes
    
    def __draw_labels(self, dpoints, angle=0, size=0.03):
        cont = []
        if options.noextralabels:
            return cont

        points = dpoints
        if len(points) > 20:            
            points = dpoints[::(len(dpoints)/20)]
            print ".... too many lables, will draw every: ",  (len(dpoints)/20), len(dpoints), len(points)
            # running to get 1st drawn
            points[0] = dpoints[0]
            points[-1] = dpoints[-1]
            
        for r in points:
            rlab = ROOT.TText(r[0], self.__minrate, str(r[1]))
            rlab.SetTextAngle(angle)
            rlab.SetTextSize(size)
            cont.append(rlab)
            rlab.Draw()
            #rlab.SetNDC(1)
            
        return cont
        
    def __draw_lbns(self):
        if not self.__lbngraph:
            return
        lbnstarts = self.__change_times(self.__lbngraph)
        self.__lbnlabels = self.__draw_labels(lbnstarts, 90, size=0.04)
        
    def __draw_runs(self):
        if self.__minrun == self.__maxrun:
            return
        runsstarts = self.__change_times(self.__rungraph)
        self.__runlabels = self.__draw_labels(runsstarts, 90)        
        pass
        
    def __enquire_ranges(self, tree, rate):
        comp = re.compile(rate) 
        bnames = [b.GetName() for b in list(tree.GetListOfBranches()) if comp.match(b.GetName())]
        if len(bnames) == 0:
            print '.... no rate selected from tree: ', tree.GetName(),' for rates selection: ', rate
            return
        # loop over branches in tree to find all matching, and to find min/max    
        for br in bnames:
            print '..... will draw ', tree.GetTitle(), br
            tree.Draw(br+":TimeStamp", self.__zoom)
            newgraph = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
            newgraph.SetName(tree.GetTitle().replace("ISS_TRP.","")+' '+br)
            newgraph.SetTitle(br)

            # get time range (this does not need to be repeated if mintime is already there
            lastpoint=newgraph.GetN()-1
            x = ROOT.Double()
            y = ROOT.Double()            
            newgraph.GetPoint(0,x,y)      
            self.__maxtime = float(max(self.__maxtime,x))
            self.__mintime = float(min(self.__mintime,x))                        
            ret = newgraph.GetPoint(lastpoint,x,y)
            if ret == -1:
                print "WARNING Wrong point, plots will be badly ranged", lastpoint
            self.__maxtime = float(max(self.__maxtime, x))
            self.__mintime = float(min(self.__mintime, x))                        

            ratesarray = self.__vv.Use(newgraph.GetN(), newgraph.GetY())
            rmin = ratesarray.Min()
            rmax = ratesarray.Max()
            
            self.__maxrate = max(self.__maxrate, rmax)
            self.__minrate = min(self.__minrate, rmin)
            mean = 0.
            if options.nosmall:
                mean = ratesarray.Sum()
                if mean != 0.:
                    mean /= ratesarray.GetNoElements()            
            
            if options.nozero and rmax < options.zero:                
                print "........    supressing rate because at maximum ", rmax , " < ", options.zero, " ", tree.GetName(), br
            elif options.nosmall and  mean < options.zero:
                print "........    supressing rate because in average ", mean , " < ", options.zero, " ", tree.GetName(), br
            else:
                self.__graphs += [newgraph]

            # if asked make TProfile too
            if options.smooth:
                p = ROOT.TProfile(newgraph.GetName(), newgraph.GetTitle(), options.smooth, self.__mintime*1., self.__maxtime*1.)
                weights = ROOT.TVectorT('double')(newgraph.GetN())
                for i in xrange(newgraph.GetN()):
                    weights[i]=1.
                p.FillN(newgraph.GetN(), newgraph.GetX(), newgraph.GetY(), weights.GetMatrixArray())
                self.__profiles.append(p)
                
        tree.Draw("Run:TimeStamp", self.__zoom)
        newgraph = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
        self.__rungraph = newgraph
        self.__minrun = int(self.__vv.Use(newgraph.GetN(), newgraph.GetY()).Min())
        self.__maxrun = int(self.__vv.Use(newgraph.GetN(), newgraph.GetY()).Max())
        if self.__minrun == self.__maxrun:
            tree.Draw("LumiBlock:TimeStamp", self.__zoom)
            self.__lbngraph = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))

        
        self.__mintime = int(self.__mintime)
        self.__maxtime = int(self.__maxtime)

    def __butify_frame(self, frameh):
        titletext='Trigger rates '
        if self.__minrun != self.__maxrun:
            titletext += 'runs '+str(self.__minrun)+':'+str(self.__maxrun)
        else:
            titletext += 'run '+str(self.__minrun)

        self.__title.SetTitle(titletext)
        frameh.GetXaxis().SetTimeDisplay(1)
        frameh.GetXaxis().SetLabelOffset(0.02)
        
        frameh.GetYaxis().SetLabelOffset(0.02)
        frameh.GetYaxis().SetTitleOffset(1.1)
        frameh.GetYaxis().SetTitle('Rate [Hz]')
        frameh.GetXaxis().SetTickLength(0)

    def __make_frame(self):
        if self.__frameh:
            del self.__frameh
        if self.__minrate == 0.:
            self.__minrate = options.zero       
        
        timemargin = int( (self.__maxtime-self.__mintime)*0.05)
        #timemargin = 0        
        self.__frameh = ROOT.TH2F('frame', ';;Rate [Hz]', 100, self.__mintime-timemargin, self.__maxtime+timemargin, 200,\
                           ( self.__minrate, self.__minrate+1.e-5*self.__maxrate)[options.nozero], self.__maxrate*1.2)
        self.__frameh.Draw()
        self.__make_title()
        self.__butify_frame(self.__frameh)

        self.__canvas.Update()
        
    def __make_title(self):
        if self.__title:
            return
        self.__title = ROOT.TText(0.02, 0.95, "title")
        self.__title.SetTextColor(ROOT.kBlack)
        self.__title.SetTextAlign(12)
        self.__title.SetNDC(1)
        self.__title.Draw()
        
        
    def __reset(self):
        for g in self.__graphs:
            del g
        self.__maxrate = 0
        self.__minrate = 0
        
    def draw(self, directives=options.rates):
        """
        Draw set of rates.
        """
        self.__last_draw = directives
        self.__saved_graphs = self.__graphs
        self.__graphs = []
        self.__saved_profiles = self.__profiles
        self.__profiles = []
        self.__maxrate = 0
        self.__mintime = time.time()    
        self.__maxtime = 0
        self.add(directives)

    def redraw(self):
        """
        Redraws last drawn rates
        """
        self.draw(self.__last_draw)

    
    def add(self, directives):
        """
        Add rate to currently drawn.
        """
        rates = [p.split() for p in directives.split(',')]
        self.__make_canvas()
        self.__make_legend()

        for r in rates:
            treename = r[0]
            rate = r[1]
            tree = None
            for k in self.__chains.keys():
                if treename == k:
                    tree = self.__chains[k]
                    break
            if not tree:
                print "... warning nothing known about tree: ", treename, " possible trees are: ", self.__chains.keys()
                continue
            self.__enquire_ranges(tree, rate)
            if  self.__maxrate - self.__minrate == 0:
                self.__maxrate += 1.;
        # print ".... minrate, maxrate", self.__minrate, self.__maxrate
        # print ".... xaxis from ", time.ctime(self.__mintime), self.__mintime," to " ,time.ctime(self.__maxtime), self.__maxtime
        if  self.__maxtime == 0:
            print ".... nothing to draw ...."
            return
        self.__make_frame()
        self.__canvas.Update() 
        if options.smooth:
            for pr in self.__profiles:
                shape, color, markeropt, linewidth = ShapeAndColor.get()
                pr.SetLineColor(color)
                pr.Draw("HIST same")
                self.__legend.AddEntry(pr, pr.GetTitle(), "l")
        else:
            for gr in self.__graphs:
                shape, color, markeropt, linewidth = ShapeAndColor.get()
                # print '.... drawing ', newgraph.GetName(), ' with this style: ',  shape, color
                
                gr.SetMarkerStyle(shape)
                gr.SetMarkerColor(color)
                gr.SetLineColor(color)
                gr.SetMarkerSize(0.5)
                gr.SetLineWidth(linewidth)
                gr.Draw(markeropt+"same")
                self.__legend.AddEntry(gr, gr.GetTitle(), markeropt)

        self.__draw_runs()
        self.__draw_lbns()
        [ i.SetMarkerSize(5.) for i in list(self.__legend.GetListOfPrimitives())]
        
        self.__legend.Draw()
        self.__canvas.Update()
        ShapeAndColor.reset()
        pass


    def save(self, name=None, all=True):
        """
        Saves current canvas to the file fiven in command line or the one given.
        """
        print ".... saving graphics file"
        output_name = name or options.output
        if self.__canvas:
            self.__canvas.SaveAs(output_name)
            
        if all:
            for c in self.__remembered_canvases:
                c.SaveAs(c.GetName()+'_'+output_name)

        
    def list(self, br='.*_.*'):
        """
        Lists possible rates.
        """
        comp = re.compile(br)
        for name,tree in self.__chains.iteritems():
            print tree.GetName(),' ', tree.GetTitle()
            branches = [b.GetName() for b in list(tree.GetListOfBranches()) if comp.match(b.GetName())]
            for b in xrange(0, len(branches),5):
                print branches[b:b+5]                
    def zoom(self, cond=""):
        """
        Restrict plotting to the range. The range is actually condition passed to the TTree::Draw.
        Note that it will not expand intially chose archive range. So best is to access larger chunk of the archive
        and then zoom.

        Example conditions:
        Run==141262
        LumiBlock>20 && LumiBlock< 40 && Run=141262
        """
        self.__zoom=cond
        
        
    def spectrum(self):
        """
        Instead of the X/Y plot make 2D spectrum plot, usefull if many rates to be drawn together.
        """
        if len(self.__graphs) < 1:
            print "... does not make sense to draw spectrum for 0 rates"
            return
        
        print '.... booking spectral plot'
        sp = ROOT.TH2F("spectrum", "Rates spectrum", 2000, self.__mintime, self.__maxtime+1,
                             len(self.__graphs), 0, len(self.__graphs))
        
        for x in zip(xrange(1,1+len(self.__graphs)), self.__graphs):
            ratebin   = x[0]
            graph = x[1]
            sp.GetYaxis().SetBinLabel(ratebin, graph.GetName())

            currentbin = 1
            sum = 0.
            count = 0.
            for point in xrange(graph.GetN()):
                time = ROOT.Double()
                rate = ROOT.Double()
                ret = graph.GetPoint(point, time, rate)
                
                sum += rate
                count += 1.
                
                globalbin = sp.FindBin(time, ratebin-1)                
                if globalbin != currentbin:
                    value = 0.
                    if count != 0.:
                        value = sum/count
                        
                    sp.SetBinContent(globalbin, value)            
                    currentbin = globalbin
                    sum = 0.
                    count = 0.
                    
        print '.... drawing spectral plot'            
        self.__canvas.SetLeftMargin(0.3)
        self.__canvas.SetRightMargin(0.15)
        sp.Draw("colz");        
        self.__butify_frame(sp)
        sp.GetYaxis().SetTitle("");
        sp.GetYaxis().SetLabelOffset(0.01)
        self.__canvas.Update()
        self.__make_title()
        self.__frameh  = sp

    def average(self):
        """
        Plot barchart of average rates        
        """
        if len(self.__graphs) < 1:
            print "... does not make sense to draw average for 0 rates"
            return

        sz = len(self.__graphs)
        bar = ROOT.TH1F("average", "Average rates", sz, 0, sz )
        bin = 0;
        for g in self.__graphs:
            bin +=1
            bar.GetXaxis().SetBinLabel(bin, g.GetTitle())    
            bar.SetBinContent(bin, g.GetMean(2))    
        self.__canvas.SetBottomMargin(0.3)
        self.__canvas.SetRightMargin(0.05)
        bar.GetYaxis().SetTitle("Rate [Hz]");
        bar.Draw('bar1')
        self.__butify_frame(bar)
        self.__frameh  = bar
        self.__frameh.SetFillColor(33)
        self.__canvas.Update()
        self.__make_title()

    def remember_as(self, newname):
        """
        Saves current canvas with new name, new draw command will make new canvas.
        So made canvases will be saved with the name prefixing the name given to save.
        """
        newc = None
        if self.__canvas:
            newc = self.__canvas.DrawClone()
            newc.SetName(newname)
            newc.SetTitle(newname)
            self.__remembered_canvases.append(newc)
            self.__canvas = None
        return newc
            
    def dump(self, directives, filename='rates.txt'):
        print '... dump rates: ', directives, ' to file: ', filename  
        rates = [p.split() for p in directives.split(',')]
        for r in rates:
            treename = r[0]
            rate = r[1]
            tree = None
            for k in self.__chains.keys():
                if treename == k:
                    tree = self.__chains[k]
                    break
            if not tree:
                print "... warning nothing known about tree: ", treename, " possible trees are: ", self.__chains.keys()
                continue
            else:
                print '... appending rates to output file (count):',  tree.GetEntries()
                comp = re.compile(rate) 
                bnames = [b.GetName() for b in list(tree.GetListOfBranches()) if comp.match(b.GetName())]
                if len(bnames) == 0:
                    print '.... no rate selected from tree: ', tree.GetName(),' for rates selection: ', rate
                    return
                f = file(filename, 'a+')
                f.write("Run,LumiBlock,TimeStamp,"+','.join(bnames)+"\n")
                for e in xrange(tree.GetEntries()):
                    tree.GetEvent(e)
                    #print "entry: ",  e, " tree.Run", tree.Run
                    f.write("%d,%d,%d" % (tree.Run, tree.LumiBlock, int(tree.TimeStamp)))
                    for b in bnames:
                        f.write(',')
                        f.write("%f"%eval('tree.'+b))
                    f.write('\n')
                f.close()
                       


def d2t(dt):
    """
    Converts date time from format: 2009-12-05 12:00:00 to seconds from EPOCH.

    This conversion is handy when zoom-ing.
    """
    from time import strptime
    from calendar import timegm
    return  str(timegm(strptime(dt, "%Y-%m-%d %H:%M:%S")))


        
# ###############################################################
# execution
print "... getting to archive"
arch = trp_archives()
files = []
if not options.file:
    files = arch.get_files(options.archive)
else:
    print '... option files: ', options.file
    files.extend([ f.strip() for f in options.file.split(',')] )
print '... archive files: ', files
if len(files) == 0:
    print '... no archive files, exitting .... '
    sys.exit(1)

chains = [ ROOT.TChain('L1_Rate', 'L1'), ROOT.TChain('HLT_Rate', 'HLT'),
           ROOT.TChain('Lu_Rate_Luminosity', 'Lumi') ]

for c in chains:
    for f in files:
        c.Add(f)
        
r = trp_rates(chains)
if options.zoom != "":
    r.zoom(options.zoom)

if options.dump != "":
    r.dump(options.rates, options.dump)
    for c in chains:
        c.Reset()
    sys.exit(0)
    pass

if options.list:
    r.list()
else:
    if options.rates != "":
        r.draw(options.rates)
    if options.modifier != "":
        print '... executing modifier: ', options.modifier
        exec(file(options.modifier))
        
    if not options.quiet:
        import code
        print "."*80
        print "You can now play with the drawing, resize, move legend .... "
        print "   r.save() to save file as: "+options.output
        print "   r.save('file.(jpg|png|pdf)') to save with custom name"
        print "   r.list() to list all possible rates"
        print "   r.list('HLT_mu.*') to list HLT muon chains rates, r.list('ATLAS.*') to list Lumi values"
        print "   r.add(\""+options.rates+"\") to add more rates to the plot"
        print "   r.draw(\""+options.rates+"\") to draw new plots"
        print "   r.zoom(\"LumiBlock>10 && LumiBlock<20\") restrict plot to the range of LBNs"
        print "   r.zoom(\"Run==141561\") restrict plot to one run (you can combine it)"
        print "   r.redraw() redraw recently drawn plot"
        print "If the plot is to cluttered you can draw few rates then ClonePad with mouse and draw another rates"        
        print "."*80
        print ""

        code.interact(local=globals())
    else:        
        r.save()


for c in chains:
    c.Reset()
