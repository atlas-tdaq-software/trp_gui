#include "is_server.h"  // #include <is/server.h>

#include "trp_isthread.h"
#include "trp_ipcthread.h"

TRP_IPCThread::TRP_IPCThread(const IPCPartition *p, 
                             std::string s,
                             const std::deque<TRP_ISThread*>& t,
                             QObject *parent)
  : QThread(parent), partition(p), server(s), threads(t) {
  stop = false;
  partition_exists = false;
  server_exists = false;
  for (unsigned int i = 0; i < threads.size(); ++i) {
    thread_exists.push_back(false);
  }
}

void TRP_IPCThread::quit() {
  stop = true;
}

void TRP_IPCThread::partitionDisappeared() {
  server_exists = false;
  partition_exists = false;
}

void TRP_IPCThread::run() {
  bool check_server;
  while (!stop) {
    if (partition->isValid()) {  
      if (partition_exists) {  

        check_server = false;
        try {
          if (is::server::exist(*partition, server)) {
            check_server = true;
          }
        } catch (...) { }
        
        if (check_server) {
          if (server_exists) {
            for (unsigned int i = 0; i < threads.size(); i++) {
              if (threads[i]->exists()) {
                if (!thread_exists[i]) {
                  thread_exists[i] = true;
                  emit threadUp(i);
                }
              } else {
                if (thread_exists[i]) {
                  thread_exists[i] = false;
                  emit threadDown(i);
                }
              }
            }
          } else {
            server_exists = true;
            emit serverUp();
          }
        } else {
          if (server_exists) {
            server_exists = false;
            for (unsigned int i = 0; i < threads.size(); ++i) {
              if (thread_exists[i]) {
                thread_exists[i] = false;
                emit threadDown(i);
              }
            }
            emit serverDown();
          }
        }
      } else { 
        partition_exists = true;
      }
    } else {
      if (partition_exists) {
        partition_exists = false;
        if (server_exists) {
          server_exists = false;
          for (unsigned int i = 0; i < threads.size(); ++i) {
            if (thread_exists[i]) {
              thread_exists[i] = false;
              emit threadDown(i);
            }
          }
          emit serverDown();
        }
      }
    }
    sleep(1);
  }
}
