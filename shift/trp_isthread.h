/** @file trp_isthread.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_ISThread: thread class for subscribing to IS
 */

#ifndef TRP_ISTHREAD_H
#define TRP_ISTHREAD_H

#include <string>
#include <deque>
#include <memory>
#include <QThread>
#include <QMutex>
#include <vector>
#include <set>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TLeaf.h>

#include <is/infodictionary.h>
#include <TRP/TimePoint_IS.h>

#include "./trp_typeAliases.h"


class QPointF;
class ISInfoReceiver;
class ISCallbackInfo;

class TRP_ISThread : public QThread {
  Q_OBJECT

public:
  TRP_ISThread(QString thread_name,
               QString thread_alias,
               QString thread_unit, 
               QString server_name, 
               unsigned int history_size, 
               std::string partitionName,
               TRP_TPDataPtr,
               QObject *parent = 0);
  // const std::shared_ptr<TRP_TPData>&,

			   
  TRP_ISThread(QString thread_name,
               QString thread_alias,
               QString thread_unit,
               QString server_name,
               unsigned int history_size,
               std::string partitionName,
               //const std::shared_ptr<TRP_TPData>&,
               TRP_TPDataPtr,
               TFile *rootFile = nullptr, 
               QObject *parent = 0);
			   
  virtual ~TRP_ISThread();

  TRP_ISThread(const TRP_ISThread &) = delete;
  TRP_ISThread& operator = (const TRP_ISThread &) = delete;

  void quit();

  void setDictionary(std::string partitionName) {
    if ( &dictionary == 0 )
      dictionary = IPCPartition(partitionName);
  }
  void setReceiver(ISInfoReceiver *r) {receiver = r;}

  bool exists();
  std::string getFullName() const {return full_name;}

  QString getServer() const {return server;}

  void is_callback(ISCallbackInfo *isc);

  TRP_TPDataPtr getTimePointData() const noexcept;

  std::string getSumary() const noexcept;
  
  void setThreadNumber(int i) { thread_number = i; }
  
  void readRootFile(std::vector <TimePoint_IS> &tp_vec);

  void updateHistoryPoint(unsigned int point);
  void updateHistoryStep(int step);


private:
  void run();
  void store_unique_timepoints(std::vector <TimePoint_IS>&);

  QMutex m_mutex;
  QString name, alias, unit, server;
  QString name_x, name_y;
  std::string partitionName;
  ISInfoDictionary dictionary;
  ISInfoReceiver *receiver;

  TFile *file;
  
  unsigned int n_history;
  TRP_TPDataPtr m_tpdata;

  std::string full_name;

  bool stop;
  bool history_enabled;
  unsigned int history_point;
  int history_step;
  bool history_diff;
  int thread_number; //Used for updating the correct index of QDateTimeVec in TablePage
  bool firstcallback;
  bool m_doSummary {false};

signals:
  void gotData();
  void updateSpinboxLimits(int);
  void updateHistoryTimeLabel(int);

private slots:
  void updateHistoryDiff(int);
  void lockHistoryPoint();


};

#endif  // define TRP_ISTHREAD_H
