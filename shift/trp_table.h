/** @file: trp_table.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Table: QTableView to display contents of a TimePointData object
 */

#ifndef TRP_TABLE_H
#define TRP_TABLE_H

#include <QTableView>
#include <QRegExp>
#include <deque>

#include "trp_config.h"
#include "trp_typeAliases.h"
#include <ostream>
#include <string>

class QSortFilterProxyModel;
class TRP_ISThread;
class TRP_BaseModel;
class TRP_Model;
class TRP_ChainModel;
// IRH 2011-03-14
class TRP_Delegate;

class TRP_Table : public QTableView {
  Q_OBJECT

public:
  TRP_Table(const TRP_TPDataPtr& thread, QWidget *parent = 0);
  TRP_Table(const TRP_TPDataContainer& threads, const TRP_Config *c, QWidget *parent = 0);

  TRP_Table(const TRP_Table &) = delete;
  TRP_Table& operator = (const TRP_Table &) = delete;

  TRP_BaseModel * getModel() const;
  QSortFilterProxyModel * getProxyModel() const noexcept;

  TRP_Config::Trend getXaxisTrend() const noexcept;

  TRP_Config::Trend getXtrend() const noexcept;
  void clearXtrend();


  // IRH 2011-03-14
  QRegExp getFilterRegexp() const noexcept;

  void applyFilter(const QString &text);
  void applyFilter();
  // sue 10.8.2012
  void applyHide(const bool hidedisabled);

  TRP_TPDataPtr getThread() const noexcept;

  std::string display() const;

protected:
  virtual void showEvent(QShowEvent* event);
  virtual void resizeEvent(QResizeEvent* event);

  void setColumnWidths();
             
private:
  TRP_TPDataPtr m_tpData;
  TRP_BaseModel* m_tablemodel;
  QSortFilterProxyModel* m_proxymodel;
  QRegExp m_tableregexp;
  int m_minVerticalHeaderWidth;
  // IRH 2011-03-14
  TRP_Delegate* m_tabledelegate;  // determines visual aspect of table
  TRP_Config::Trend  m_xtrend;

  void init(unsigned int);
  QString       m_filterText;
  bool          m_hidedisabled;

private slots:
  void updateItems();
  void selectXaxisTrend(const QModelIndex &);

};


std::ostream& operator << (std::ostream&, const TRP_Table&);
std::ostream& operator << (std::ostream&, const TRP_Table*);

#endif  // define TRP_TABLE_H

