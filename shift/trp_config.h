/** @file trp_config.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Config: configuration reader for TRP GUI
 */

#ifndef TRP_CONFIG_H
#define TRP_CONFIG_H

#include <deque>
#include <QXmlStreamReader>
#include <ostream>

class QFile;

class TRP_Config : public QXmlStreamReader {
public:
  TRP_Config(QString conf_file, QString part_name, unsigned int hist_limit);
  TRP_Config(std::vector<QString> threadlist);

private:
  TRP_Config(const TRP_Config &);
  TRP_Config& operator = (const TRP_Config &);

public:
  QString triggerDBName() const;
  QString partitionName() const;
  QString serverName() const;
  unsigned int historySize() const;

  QString warnings() const;

  struct Thread {
    QString name;
    QString alias;
    QString unit;
  };

  std::deque<Thread> threads;

  struct Trend {
    unsigned int n_thread;
    QString x_name;
    QString y_name;
  };

  struct Plot {
    QString title;
    std::deque<Trend> trends;
  };

  //struct CorrPlot {
  //  QString title;
  //  std::deque<Trend> trends;
  //};

  struct PlotPage {
    QString title;
    std::deque<Plot> plots;
  };

  //struct CorrPlotPage {
  //  QString title;
  //  std::deque<CorrPlot> corrplots;
  //};

   std::deque<PlotPage> plot_pages;
   //   std::deque<CorrPlotPage> corr_plot_pages;

signals:
  void valueChanged(Trend);

private:
  void setDefaults();
  bool openConfigFile();
  bool openDefaultConfigFile();
  void readConfigFile();
  void readGeneral();
  void readThreads();
  void readPlotPage();
  void readCorrPlotPage();
  TRP_Config::Plot readPlot();
  TRP_Config::Plot readCorrPlot();
  TRP_Config::Trend readTrend();

  QString config_file_path;
  QString default_config_path;
  QString default_config_path2;
  QString default_config_path_p1;
  QFile *config_file;
  QString warning_text;

  QString partition_name, server_name, triggerdb_name;
  unsigned int history_size;

};

std::ostream& operator << (std::ostream&, const TRP_Config::Trend &);
#endif  // define TRP_CONFIG_H
