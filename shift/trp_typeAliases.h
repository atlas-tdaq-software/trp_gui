#ifndef TRP_TYPEALIASES_H
#define TRP_TYPEALIASES_H
#include <deque>
#include <memory>
#include "trp_tpdata.h"
#include <QPointF>

// using TRP_TPDataPtr = std::shared_ptr<TRP_TPData>;
using TRP_TPDataPtr = TRP_TPData*;
using TRP_TPDataContainer = std::deque<TRP_TPDataPtr>;
using pQPointF = std::shared_ptr<QPointF>;
using QPointFDeque = std::deque<pQPointF>;
#endif
