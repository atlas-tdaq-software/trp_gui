<html>

<head>
<link rel="stylesheet" type="text/css" href="trp_help.css"> 
</head>

<body>

<h1>TRP GUI Help</h1>

<h2>Index</h2>

<ul>
  <li> <a href="#main_window">Main Window</a>
  <li> <a href="#plots">Plots</a>
    <ul>
    <li> <a href="#saving_plot">Saving a Plot</a>
    <li> <a href="#zooming">Zooming and Unzooming</a>
    <li> <a href="#identifying_trends">Identifying Trends</a>
    <li> <a href="#spike">Understanding a Spike</a>
    </ul>
  <li> <a href="#tables">Tables</a>
    <ul>
    <li> <a href="#table_plotting">Making a New Plot</a>
    <li> <a href="#table_correlation_plotting">Making a Correlation Plot</a>
    <li> <a href="#table_filtering">Filtering</a>
    </ul>
  <li> <a href="#config_gui">Configuring the GUI</a>
</ul> 

<a name="main_window"/>
<h2>Main Window</h2>

<p>
To resize the main window, drag any of its corners or borders
with the left mouse button.
</p>

<p>
To grab a snapshot of the main window, select "Grab Screenshot" 
from the File menu.  A "Save Image As ..." dialog will appear.  
Type a file name, noting the directory in which the file is saved.
The image type will depend on the suffix of the file name chosen.
</p>

<p>
The tabs displayed can be configured as described in Section 
<a href="#config_gui">Configuring the GUI</a>.
</p>

<a name="plots"/>
<h2>Plots</h2>

<a name="saving_plot"/>
<h3>Saving a Plot</h3>

<p>
To save a plot (e.g., for posting on ATLOG), right-click on the plot, 
and select "Save Image".  A preview window will open, displaying 
the plot and a legend.  Click the "Save Image" button.
A "Save Image As ..." dialog will appear.
Type a file name, noting the directory in which the file is saved.
The image type will depend on the suffix of the file name chosen.
</p>

<a name="zooming"/>
<h3>Zooming and Unzooming</h3>

<p>
To zoom into a plot, drag the left mouse button 
horizontally in the region below the horizontal axis,
or vertically in the region to the left of the vertical axis.  
</p>

<p>
To unzoom, right-click on the plot and select "Unzoom".
</p>

<p>
If, when zooming into a time axis, the mouse is released
beyond the maximum of the axis, the maximum of the plot
will not be fixed.  In other words, the zoomed-in plot 
will continue to update.
</p>

<a name="identifying_trends"/>
<h3>Identifying Trends</h3>

<p>
To identify a trend, click on a point in the trend with the left 
mouse button.  A tooltip will appear, displaying the name of the trend
while the mouse button is pressed.
</p>

<a name="spike"/>
<h3>Understanding a Spike</h3>

<p>
When a spike appears in a plot of trigger rates,
more than one trigger may be involved.  This, however, may not be 
immediately obvious: if several triggers drawn on the same plot have 
identical rates, only the trend drawn last will be visible.
To determine which triggers contribute to a spike,
drag the middle mouse button inside the plot,
and select a rectangular region that encloses the spike.
When the middle mouse button is released, 
a message window will appear, listing the names of the trends
that have points in the selected region of the plot.
</p>

<a name="tables"/>
<h2>Tables</h2>

<p>
The Tables tab contains one or more sub-tabs, 
each of which displays a table.
</p>

<a name="table_plotting"/>
<h3>Making a New Plot</h3>

<p>
Select any combination of cells from any combination of the available
tables by left-clicking the cells or dragging the left mouse button.
Pressing the Ctrl button while clicking the cells will allow you
to select non-contiguous cells.  Next, click the "Plot" button.  
A new window will open, displaying the trends corresponding to the cells
selected in the tables.
</p>

<p>
Before making a new selection, click the "Clear" button 
to ensure that all the previously highlighted cells in all the tables 
are cleared.  
</p>

<a name="table_correlation_plotting"/>
<h3>Making a Correlation Plot</h3>

<p>
Select any combination of cells from any combination of the available
tables by left-clicking the cells or dragging the left mouse button.
Pressing the Ctrl button while clicking the cells will allow you
to select non-contiguous cells.  These will be plotted on the y-axis.  
Select a cell for the x-axis by double clicking.  If this cell is
in the same tab as the previously selected cells, use the Ctrl key.
Next, click the "Plot Correlation" button.  
A new window will open, displaying the trends corresponding to the cells
selected in the tables.
</p>

<p>
Before making a new selection, click the "Clear" button 
to ensure that all the previously highlighted cells in all the tables 
are cleared.  The cell selected by double-clicking will however be
saved, until you make a new double-click selection.
</p>


<a name="table_filtering"/>
<h3>Filtering</h3>

<p>
In the field labelled "Filter", type some text or a regular expression.
If all characters are lower-case the search will be case-insensitive, otherwise
case-sensitive. The entries in the tables will then be filtered to display
only those entries that have names matching the text or regular expression.
</p>

<p>
To un-filter, simply erase the text in the "Filter" field.
</p>

<a name="config_gui"/>
<h2>Configuring the GUI</h2>

<p>
The TRP GUI can be configured to have one or more plot tabs. 
Each plot tab can be configured to display 
one or more plots.  Each of these plots can in turn be configured
display one or more trends.  Plot tabs should be used
to display default plots that all shifters should monitor.
</p>

<p>
The configuration file is specified with the -c option:
<br>
<kbd>trp -c /path/file.xml</kbd>
<br>
The XML file should be well-formed.  In particular, it must contain 
exactly one root element called <code>&lt;settings&gt;</code>.
</p>

<p>
Use the <code>&lt;general&gt;</code> element
to specify the TDAQ partition, the TRP server, and the trigger database,
as well as the history size 
(i.e., the maximum number of points to display in the plots).
Note that if a partition is specified on the command line with the -p option, 
it will override the partition specified in the configuration file.
If no partition is specified on the command line or in the configuration file,
then the environment variable $TDAQ_PARTITION will be used.
If no such environment variable is defined, the partition name
will be set to "ATLAS".
</p>

<p>
Use the <code>&lt;threads&gt;</code> element 
to list the TRP threads to subscribe to.  
For each thread, a table will be displayed in the Tables tab.
Use the <code>alias</code> attribute of the <code>&lt;thread&gt;</code>
element to specify the name that will be displayed on the sub-tab of the table.
</p>

<p>
Each plot tab is defined within a <code>&lt;plotpage&gt;</code> element.
The name of the tab is specified by the <code>&lt;title&gt;</code> element.
To add plots to a plot tab, use nested <code>&lt;plot&gt;</code> elements
within the <code>&lt;plotpage&gt;</code> element.
A <code>&lt;title&gt;</code> for the plot is optional.
Add one or more <code>&lt;trend&gt;</code> elements to the plot.
For each trend, specify the <code>&lt;thread&gt;</code> number, as well as 
an <code>&lt;x&gt;</code> and a <code>&lt;y&gt;</code> title.
Note that the thread number 0 corresponds to the first thread listed in the 
<code>&lt;threads&gt;</code> element described in the previous paragraph.
</p>

<p>
A sample configuration file follows.
</p>

<hr> <code>
&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;<br>
&lt;settings&gt;<br>
&nbsp; &lt;general&gt;<br>
&nbsp; &nbsp; &lt;triggerdb&gt;TRIGGERDB&lt;/triggerdb&gt;<br>
&nbsp; &nbsp; &lt;partition&gt;TDAQ&lt;/partition&gt;<br>
&nbsp; &nbsp; &lt;server&gt;ISS_TRP&lt;/server&gt;<br>
&nbsp; &nbsp; &lt;history&gt;200&lt;/history&gt;<br>
&nbsp; &lt;/general&gt;<br>
&nbsp; &lt;threads&gt;<br>
&nbsp; &nbsp; &lt;thread alias="L1"&gt;L1_Rate&lt;/thread&gt;<br>
&nbsp; &nbsp; &lt;thread alias="L2"&gt;L2_Rate&lt;/thread&gt;<br>
&nbsp; &nbsp; &lt;thread alias="EF"&gt;EF_Rate&lt;/thread&gt;<br>
&nbsp; &lt;/threads&gt;<br>
&nbsp; &lt;plotpage&gt;<br>
&nbsp; &nbsp; &lt;title&gt;Plots&lt;/title&gt;<br>
&nbsp; &nbsp; &lt;plot&gt;<br>
&nbsp; &nbsp; &nbsp; &lt;title&gt;Plot 1&lt;/title&gt;<br>
&nbsp; &nbsp; &nbsp; &lt;trend&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;thread&gt;0&lt;/thread&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;x&gt;total&lt;/x&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;y&gt;input&lt;/y&gt;<br>
&nbsp; &nbsp; &nbsp; &lt;/trend&gt;<br>
&nbsp; &nbsp; &nbsp; &lt;trend&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;thread&gt;1&lt;/thread&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;x&gt;total&lt;/x&gt;<br>
&nbsp; &nbsp; &nbsp; &nbsp; &lt;y&gt;input&lt;/y&gt;<br>
&nbsp; &nbsp; &nbsp; &lt;/trend&gt;<br>
&nbsp; &nbsp; &lt;/plot&gt;<br>
&nbsp; &lt;/plotpage&gt;<br>
&lt;/settings&gt;
</code> <hr>

</body>

</html>
