#include <iostream>
#include <QtGui>

#include "trp_model.h"
#include <sstream>

TRP_BaseModel::TRP_BaseModel(QObject *parent)
   : QAbstractItemModel(parent) {
   }

QModelIndex TRP_BaseModel::index(int row, int column, const QModelIndex &parent) const {
   Q_UNUSED(parent);
   return createIndex(row, column);
}

QModelIndex TRP_BaseModel::parent(const QModelIndex &index) const {
   Q_UNUSED(index);
   return QModelIndex();
}

// history
void TRP_BaseModel::emitUpdateHistoryTimeLabel(unsigned int point) {
   history_point = point;
   emit updateHistoryTimeLabel(point);
}

std::string TRP_BaseModel::display() const {
  std::stringstream ss;
  ss << "TRP_BaseModel:"
     << " row count " << rowCount()
     << " column count " << columnCount()
     << " + more not yet implemented";
  return ss.str();
}

std::ostream& operator << (std::ostream& ostr, const TRP_BaseModel& bm){
  ostr << bm.display();
  return ostr;
}

std::ostream& operator << (std::ostream& ostr, const TRP_BaseModel* bm){
  ostr << (*bm);
  return ostr;
}


// IRH 2011-03-14 added new class
TRP_Delegate::TRP_Delegate(QObject *parent)
   : QAbstractItemDelegate(parent), m_tt(0), m_ttp(0) {

      m_tt  = qobject_cast<TRP_Table*>(parent);
      m_ttp = qobject_cast<TRP_TablePage*>(m_tt->parent());
      m_thread = m_tt->getThread();

   }

// IRH 2011-03-14
void TRP_Delegate::paint ( QPainter * painter, 
      const QStyleOptionViewItem & option, 
      const QModelIndex & index ) const {
   static QFont font("Helvetica");
   font.setPixelSize(12);
   static QFontMetrics metrics(font);
   static int flags = Qt::TextSingleLine | Qt::AlignLeft;

   painter->save();  
   // item colour gray for L1 disabled (PS=-1)
   // item colour pink for L1 prescaled (PS>0)
   if (m_thread->isHistoryEnabled() &&
         m_thread->isHistoryDiff()    && 
         m_thread->getHistoryStep() !=0 ) {
      if (qAbs(index.data().toDouble()) > 1e10)   
         painter->fillRect(option.rect, Qt::lightGray);
      if (qAbs(index.data().toDouble())>0.10 && 
            qAbs(index.data().toDouble())<1e10) 
         painter->fillRect(option.rect, QColor(244, 164, 96, 100));
   }

   //sue 10.8.2012
   //if (m_ttp->getTabWidget()->currentIndex()==m_ttp->getTabHideCheckbox()) {
   //L1 index =0
   if (m_ttp->getTabWidget()->currentIndex()==0) {
     if (index.column()==4) {
       if (index.data().toInt() < 0 ) {
	 painter->fillRect(option.rect, Qt::lightGray);
       }
       if (index.data().toInt() > 1) {
	 painter->fillRect(option.rect, QColor(132, 112, 255, 100));
       }
     }
   }
   

   if(m_tt->getModel()->isChainTable() ){
      if (m_tt->getModel()->isTitleColumn( index.column() ) ) {
         if (index.data().toString().length()==0) {
            m_tt->hideRow(index.row());
            //painter->fillRect(option.rect, Qt::white);
         }else{
            painter->fillRect(option.rect, Qt::gray);
         }
      }
   }else{
      if(index.column()==0){
         QRegExp filter_regexp = m_tt->getFilterRegexp();
         if (index.data().toString().contains(filter_regexp)) {
            painter->fillRect(option.rect, Qt::gray);
            m_tt->showRow(index.row());
         } else {
            m_tt->hideRow(index.row());
         }
      }         
   }
   //
   std::deque <TRP_Table *> tables = m_ttp->getTables();
   tables[0]->applyFilter(); // L1 table only
   //for (unsigned int i = 0; i < tables.size(); ++i) {
   //   tables[i]->applyFilter();
   //}

   QString text = index.data().toString();

   //colour flags for predictions
   if ((m_ttp->getTabWidget()->currentIndex()==13 && index.column()==8)
       || (m_ttp->getTabWidget()->currentIndex()==14 && index.column()==5)
       || (m_ttp->getTabWidget()->currentIndex()==15 && index.column()==5)){
     
     int flagValue = index.data().toInt();
     if      (flagValue ==3)  painter->fillRect(option.rect, Qt::darkRed);
     else if (flagValue ==2)  painter->fillRect(option.rect, Qt::red);
     else if (flagValue ==1)  painter->fillRect(option.rect, QColor(255, 105,   0)); // orange
     else if (flagValue ==0)  painter->fillRect(option.rect, Qt::green);
     else if (flagValue ==-1) painter->fillRect(option.rect, Qt::lightGray);
     else if (flagValue ==-2) painter->fillRect(option.rect, Qt::darkGray);


     //numbers for expert, text for shifter.
     if (flagValue==3)       text= "3: ERROR";
     else if (flagValue==2)  text= "2: Error";
     else if (flagValue==1)  text= "1: Warning";
     else if (flagValue==0)  text= "0: OK";
     else if (flagValue==-1) text="-1: No Prediction";
     else if (flagValue==-2) text="-2: No Rate";
   }
   
   //painter->setFont(font);
   painter->drawText(option.rect, flags, text);
   painter->restore();

}

// IRH 2011-03-14
QSize TRP_Delegate::sizeHint ( const QStyleOptionViewItem & option, 
      const QModelIndex & index ) const {
   Q_UNUSED(option);
   Q_UNUSED(index);
   return QSize(200, 52);
}
