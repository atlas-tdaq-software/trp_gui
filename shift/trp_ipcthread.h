/** @file trp_ipcthread.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_IPCThread: thread class for monitoring partition
 */

#ifndef TRP_IPCTHREAD_H
#define TRP_IPCTHREAD_H

#include <deque>
#include <string>
#include <ipc/partition.h>

#include <QThread>

class TRP_ISThread;

class TRP_IPCThread : public QThread {
  Q_OBJECT

public:
  TRP_IPCThread(const IPCPartition *p, 
                std::string s,
                const std::deque<TRP_ISThread*>& t,
                QObject *parent = 0);

private:
  TRP_IPCThread(const TRP_IPCThread &);
  TRP_IPCThread& operator = (const TRP_IPCThread &);

public:
  void quit();
  void partitionDisappeared();

signals:
  void serverUp();
  void serverDown();
  void threadUp(unsigned int i);
  void threadDown(unsigned int i);

protected:
  void run();

private:
  const IPCPartition *partition;
  std::string server;
  const std::deque <TRP_ISThread *> threads;

  bool partition_exists;
  bool server_exists;
  std::deque <bool> thread_exists;

  bool stop;
};


#endif  // define TRP_IPCTHREAD_H
