#ifndef TRP_UNIQUE_TIMEPOINTS_H
#define TRP_UNIQUE_TIMEPOINTS_H

/* Create a deque of shared pointers of timepoints with
   duplicates removed from a vector of time points */
#include <deque>
#include <TRP/TimePoint_IS.h>

std::deque <std::shared_ptr<TimePoint_IS>>
trp_unique_timepoints(std::vector <TimePoint_IS>& tps);

#endif
