#ifndef IS_SERVER_H
#define IS_SERVER_H

#include <ipc/partition.h>
#include <is/exceptions.h>

namespace is
{
    namespace server
    {
	bool exist( const IPCPartition & partition, const std::string & name );
        
    	//void clone( const IPCPartition & partition, const std::string name, const std::string & clone_name, unsigned int timeout = 0 ) 
        //		throw ( daq::is::ServerNotFound, daq::is::ServerAlreadyExist, daq::is::InvalidServerName );
    }
}

#endif
