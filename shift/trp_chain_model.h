/** @file: trp_model.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_ChainModel: Model for TimePoint_IS object
 */

#ifndef TRP_CHAINMODEL_H
#define TRP_CHAINMODEL_H

#include <QAbstractItemModel>
#include <deque>

#include "trp_isthread.h"
#include <TRP/TimePoint_IS.h>
#include <TRP/Utils.h>
#include "trp_base_model.h"
#include "trp_config.h"
#include "trp_typeAliases.h"
#include <is/inforeceiver.h>
#include <is/callbackinfo.h>

// class TRP_ISThread;

class TRP_ChainModel : public TRP_BaseModel {
   Q_OBJECT

   public:
      TRP_ChainModel(const TRP_TPDataContainer& t, 
		     const TRP_Config *c,  
		     QObject *parent = 0);

   private:
      TRP_ChainModel(const TRP_ChainModel &);
      TRP_ChainModel& operator = (const TRP_ChainModel &);

   public:
      int virtual 
        rowCount(const QModelIndex &parent = QModelIndex()) const;
      int virtual 
        columnCount(const QModelIndex &parent = QModelIndex()) const;

      QVariant virtual data (const QModelIndex &index, int role) const;
      QVariant virtual headerData(int section, 
				  Qt::Orientation orientation, 
				  int role = Qt::DisplayRole) const;

      QString  virtual xTitle(const QModelIndex &index ) const { 
	return getRowName(index); }

      QString  virtual yTitle (const QModelIndex &index ) const {
	return headerData(index.column(), Qt::Horizontal).toString();}

      void     virtual update();
      void     virtual updateSMK();
      void     virtual callback_updateSMK(ISCallbackInfo*isc);
      QString  virtual getSTRName(uint i) const ;
      QString  virtual getHLTName(uint i) const ;
      QString  virtual getEFName(uint i) const ;
      QString  virtual getL2Name(uint i) const ;
      QString  virtual getL1Name(uint i) const ;
      QString  virtual getRowName(const QModelIndex &idx) const;
      QString  virtual getColName(const QModelIndex &idx) const;
      QString  virtual getColName(int col) const;
      int      virtual getThreadIndex(int col) const;
      QVariant virtual getDataPoint(const QModelIndex & idx) const;
      bool     virtual isTitleColumn(int col) const;
      bool     virtual isChainTable() const { return true; }


   private:
      TRP_TPDataContainer m_tpDataContainer;
      std::deque<QString>STR;
      std::deque<QString>HLT;
      std::deque<QString>EF;
      std::deque<QString>L2;
      std::deque<QString>L1;
      int STRIndex{-1};
      int HLTIndex{-1};
      int EFIndex{-1};
      int L2Index{-1};
      int L1Index{-1};
      std::string partitionName;
      std::string serverName;
      std::string aliasName;
      unsigned int smk_old{0};

      bool isRun1;
      bool isRun2;

};

#endif  // define TRP_CHAINMODEL_H

