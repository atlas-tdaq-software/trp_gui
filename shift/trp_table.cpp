#include <QtGui>
#include <QtWidgets>
#include "trp_model.h"
#include "trp_table.h"
#include "trp_chain_model.h"
#include <sstream>

TRP_Table::TRP_Table(const TRP_TPDataContainer& tpContainer, 
		     const TRP_Config *c, 
		     QWidget *parent)
   : QTableView(parent), m_filterText(""), m_hidedisabled(false) {

  m_tablemodel = new TRP_ChainModel(tpContainer, c, this);
  m_tpData= tpContainer[0];

  unsigned int width = 40;
  init(width);
}


TRP_Table::TRP_Table(const TRP_TPDataPtr& tpData, QWidget *parent)
// sue 10.8.2012
  : QTableView(parent), m_filterText(""), m_hidedisabled(false) {
//: QTableView(parent), m_filterText("") {

  m_tpData = tpData;
  m_tablemodel = new TRP_Model(m_tpData, this);
  
  unsigned int width = 20;
  init(width);
}


void TRP_Table::init(unsigned int width){

  m_proxymodel = new QSortFilterProxyModel(this);
  m_proxymodel->setSourceModel(m_tablemodel);
  m_proxymodel->setDynamicSortFilter(true);
  
  setModel(m_proxymodel);
  
  setEditTriggers(QAbstractItemView::NoEditTriggers);
  setSelectionBehavior(QAbstractItemView::SelectItems);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  setMouseTracking(false);
  
  setWordWrap(false);
  setSortingEnabled(true);
  
  horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
  // Not using vertical headers since they cannot be sorted
  verticalHeader()->setVisible(false);
  
  m_minVerticalHeaderWidth = fontMetrics().boundingRect("a").width()*width;
  
  // IRH 2011-03-14
  m_tabledelegate = new TRP_Delegate(this);
  setItemDelegate(m_tabledelegate);

  connect(m_tablemodel,
          SIGNAL(layoutAboutToBeChanged()),
          this,
          SLOT(updateItems()));

  connect(m_tablemodel,
          SIGNAL(layoutAboutToBeChanged()),
          m_proxymodel,
          SLOT(submit()));

  // set colour of selected items to light blue
  setStyleSheet("selection-background-color: rgba(202,225, 255, 100);");
  
  connect(this, 
          SIGNAL(doubleClicked(const QModelIndex &)), 
          this, 
          SLOT(selectXaxisTrend(const QModelIndex &)));
}

TRP_BaseModel * TRP_Table::getModel() const {
   return m_tablemodel;
}

QSortFilterProxyModel * TRP_Table::getProxyModel() const noexcept{
   return m_proxymodel;
}

TRP_Config::Trend  TRP_Table::getXaxisTrend() const noexcept{
   return m_xtrend;
}


void TRP_Table::applyFilter(const QString &text) {

   // QSortFilterProxyModel does not provide substring matching.
   // Implement our own matching.

   // Emacs-style: by default case-insensitive match, if there
   // is one capital character switch to case-sensitive matching.

   m_filterText = text;  
   m_tableregexp.setPattern(text);
   m_tableregexp.setCaseSensitivity(Qt::CaseInsensitive);

   for (int i=0; i<text.size(); ++i) {
      if (text[i].isUpper()) {
         m_tableregexp.setCaseSensitivity(Qt::CaseSensitive);
         break;
      }
   }

   //This is much quicker filtering than Qt method- but filtered plotting not work.
   //bool m_hidedisabled(false); //since functionality removed
   for (int i = 0; i < m_tablemodel->rowCount(); ++i) { 
     bool foundMatch=false;
     for (int n = 0; n < m_tablemodel->columnCount(); ++n) {
       if(!m_tablemodel->isTitleColumn(n) ) continue;
       QString header = m_tablemodel->xTitle(m_tablemodel->index(i,n));
       if (header.contains(m_tableregexp)){
	 foundMatch=true;
	 //break;
       }
     }//loop over n
     if (foundMatch) {
       //if (m_hidedisabled && m_tablemodel->data(m_tablemodel->index(i,4),0).isValid()) {
       if (m_hidedisabled) {
         int pscol=-1;
         if (m_tablemodel->isChainTable()) {
           pscol=2;
         } else {
           pscol=4;
         }
	 double ps = m_tablemodel->data(m_tablemodel->index(i,pscol),0).toDouble();
	 if (ps >= 0.) {
	   showRow(m_proxymodel->mapFromSource(m_tablemodel->index(i, pscol)).row());
	 } else {
	   hideRow(m_proxymodel->mapFromSource(m_tablemodel->index(i, pscol)).row());
	 }
       } else {
	 showRow(m_proxymodel->mapFromSource(m_tablemodel->index(i, 0)).row());
       }
     } else { //if not contains regexp//loop over i
       hideRow(m_proxymodel->mapFromSource(m_tablemodel->index(i, 0)).row());
     }
   } //loop over i    


   //provides filtered plotting
   //Default filter on first column only
   if (m_tablemodel->columnCount()<7) {m_proxymodel->setFilterRegExp(QRegExp(m_tableregexp));}
   //sorts on all columns for chains table.
   else {m_proxymodel->setFilterKeyColumn(-1);}
}


void TRP_Table::applyFilter() {
  applyFilter(m_filterText);
}

// hide disabled
void TRP_Table::applyHide(const bool hidedisabled) {
   m_hidedisabled = hidedisabled;
}


// IRH 2011-03-14
void TRP_Table::updateItems() {
   m_tablemodel->update();
}

void TRP_Table::selectXaxisTrend(const QModelIndex & index) {

  //int row = index.row();  
  //int column = index.column();  
  QModelIndex source_index = m_proxymodel->mapToSource(index);

  m_xtrend.n_thread = -1;// need to sort this
  m_xtrend.x_name = m_tablemodel->xTitle(source_index);
  m_xtrend.y_name = m_tablemodel->yTitle(source_index);

}

void TRP_Table::showEvent(QShowEvent* event)
{
   QTableView::showEvent(event);
   setColumnWidths();
}

void TRP_Table::resizeEvent(QResizeEvent* event)
{
   QTableView::resizeEvent(event);
   // Resize columns if widths changed
   if (event->oldSize().width() != event->size().width()) {
      setColumnWidths();
   }
}

void TRP_Table::setColumnWidths()
{
   /* This is basically replicating resizeMode ::Stretch but with
      the first column being wide enough to display the entire string.
    */

   // Calculate vertical header width
   int hSum(0);
   int nTitles(0);
   bool resize(true);
   double scaleFactor = 1.0;
   while(resize){
      hSum=0;
      nTitles=0;
      for (int n = 0; n < m_tablemodel->columnCount(); ++n) {
         if(!m_tablemodel->isTitleColumn(n) ) continue;
         int w(0);
         int hWidth(m_minVerticalHeaderWidth);
         for (int i = 0; i < m_tablemodel->rowCount(); ++i) { 
            QString header = m_tablemodel->xTitle(m_tablemodel->index(i,n));
            w = fontMetrics().boundingRect(header).width();
            if (w > hWidth) hWidth = w;

         }

         // Round up to nearest 10 to avoid tiny width changes between tabs
         if (m_tablemodel->isChainTable() && n==0) hWidth/=3;
         hWidth = int(ceil(scaleFactor*hWidth/10.0))*10;
         setColumnWidth(n, hWidth);  
         hSum+=hWidth;
         nTitles++;     
      }
      if (m_tablemodel->isChainTable()) { 
        if(hSum < 0.6*viewport()->width() )resize=false;
      } else {
        if(hSum < 0.5*viewport()->width() )resize=false; //titles shouldn't take up all the space 
      }
      scaleFactor*=0.9;  //reduce the size some
   }

   // Stretch the remaining columns
   if (m_tablemodel->columnCount() > 1) {
      int w = (viewport()->width() - hSum) / (m_tablemodel->columnCount()-nTitles);
      for (int i = 0; i < m_tablemodel->columnCount(); ++i) {


         if(m_tablemodel->isTitleColumn(i) ) continue;
         setColumnWidth(i, w);
      }
   }



   //need to make a fn call in table model that returns all columns which are labels then loop over columns

}
TRP_Config::Trend TRP_Table::getXtrend() const noexcept{
  return m_xtrend; 
}

void TRP_Table::clearXtrend(){
  m_xtrend.x_name="";
  m_xtrend.y_name="";
}

QRegExp TRP_Table::getFilterRegexp() const noexcept {return m_tableregexp;}

TRP_TPDataPtr TRP_Table::getThread() const noexcept {return m_tpData;}

std::string TRP_Table::display() const {
  std::stringstream ss;
  ss << "TRP_Table \n"
     << " tpData " << getThread() << '\n'
     << " regexp " << getFilterRegexp().pattern().toStdString() <<'\n'
     << " trend " << getXtrend()
     << " proxy model " << getProxyModel() <<'\n'
     << " model " << getModel() << '\n';

  return ss.str();  
}

std::ostream& operator <<(std::ostream& ostr, const TRP_Table& t){
  ostr << t.display();
  return ostr;
}

std::ostream& operator <<(std::ostream& ostr, const TRP_Table* t){
  ostr << (*t);
  return ostr;
}

