/** @file: trp_help.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Help: Help browser for TRP GUI
 */

#ifndef TRP_HELP_H
#define TRP_HELP_H

#include <QWidget>

class QTextBrowser;

class TRP_Help : public QWidget {
  Q_OBJECT

public:
  TRP_Help(QWidget *parent = 0);

private:
  TRP_Help(const TRP_Help &);
  TRP_Help& operator = (const TRP_Help &);

public:
  void home();

private:
  QTextBrowser *browser;
};

#endif  // define TRP_HELP_H

