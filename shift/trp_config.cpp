#include <stdlib.h>
#include <QtCore>
#include "trp_config.h"
#include <QCoreApplication>

TRP_Config::TRP_Config(QString conf_file, QString part_name, unsigned int hist_limit) {
  setDefaults();

  config_file_path = conf_file;
  
  //Find absolute path to default config file
  QDir *directory = new QDir(QCoreApplication::applicationDirPath());
  //default_config_path = directory->absoluteFilePath("../../../trp_gui/share/trp_gui_conf.xml");
  //default_config_path = directory->absoluteFilePath("../../../online/trp_gui/share/trp_gui_conf.xml");
  QString tdaq_version = QString(getenv("TDAQ_VERSION"));  
  default_config_path    = directory->absoluteFilePath("/afs/cern.ch/atlas/project/tdaq/prod/tdaq/"+tdaq_version+"/installed/share/data/trp_gui/trp_gui_conf.xml");
  default_config_path2   = directory->absoluteFilePath("/afs/cern.ch/atlas/project/tdaq/inst/tdaq/"+tdaq_version+"/installed/share/data/trp_gui/trp_gui_conf.xml");
  // Comment out as this directory is not part of runtime
  //default_config_path    = directory->absoluteFilePath("/afs/cern.ch/atlas/project/tdaq/inst/tdaq/"+tdaq_version+"/trp_gui/share/trp_gui_conf.xml");
  default_config_path_p1 = directory->absoluteFilePath("/atlas/moncfg/"+tdaq_version+"/trigger/trp/trp_gui_conf.xml");

  config_file = new QFile;

  if (openConfigFile() || openDefaultConfigFile()) {
    readConfigFile();
  }

  if (!part_name.isEmpty()) {  
    partition_name = part_name;
  }
  
  if (!hist_limit==0) {
     history_size = hist_limit;  
  }
  
}

TRP_Config::TRP_Config(std::vector<QString> threadlist) {
  setDefaults();

  Thread newthread;
  
  for (unsigned int i=0; i<threadlist.size(); i++) {
    newthread.name = threadlist.at(i);
	newthread.alias = threadlist.at(i);
	newthread.unit  = threadlist.at(i);
	threads.push_back(newthread);
  }
  
}

bool TRP_Config::openConfigFile() {
  if (config_file_path.isEmpty()) {
    warning_text = "No configuration file specified.\n";
    warning_text += "Default settings will be used! \n\n";
    warning_text += "Press OK to continue.";
    return false;
  } 

  if (!QFile::exists(config_file_path)) {
    warning_text  = "Configuration file\n";
    warning_text += QString("(%1)\n").arg(config_file_path);
    warning_text += "does not exist.\n";
    warning_text += "Default settings will be used.";
    return false;
  }

  config_file->setFileName(config_file_path);

  if (!config_file->open(QIODevice::ReadOnly)) {
    warning_text  = "Failed to open configuration file\n";
    warning_text += QString("(%1).\n").arg(config_file_path);
    warning_text += "Default settings will be used.";
    return false;
  }
  return true;
}

bool TRP_Config::openDefaultConfigFile() {
  if (default_config_path.isEmpty()) { // prod
    // check inst
    default_config_path = default_config_path2;
    if (default_config_path.isEmpty()) {
      // check at P1
      default_config_path = default_config_path_p1;
      if (default_config_path.isEmpty()) {
        warning_text += "\n\n";
        warning_text += "Error. Default configuration file not found!";
        return false;
      }
    } 
  }

  if (!QFile::exists(default_config_path)) {
    // check at P1
    default_config_path = default_config_path_p1;
    if (!QFile::exists(default_config_path)) {
      warning_text += "\n\n";
      warning_text += "Default configuration file\n";
      warning_text += QString("(%1)\n").arg(default_config_path);
      warning_text += "not found! Program not able to run.";
      return false;
    }
  }

  config_file->setFileName(default_config_path);

  if (!config_file->open(QIODevice::ReadOnly)) {
    warning_text  = "Failed to open default configuration file\n";
    warning_text += QString("(%1).\n").arg(default_config_path);
    warning_text += "Program not able to run.";
    return false;
  }
  return true;
}

void TRP_Config::readConfigFile() {
  setDevice(config_file);
  while (!atEnd()) {
    readNext();
    if (isStartElement()) {
      if (name() == "general") readGeneral();
      else if (name() == "threads") readThreads();
      else if (name() == "plotpage") readPlotPage();
      else if (name() == "corrplotpage") readCorrPlotPage();
    }
  }
  if (hasError()) {
    warning_text  = "Error in configuration file\n";;
    warning_text += QString("(%1)\n").arg(config_file->fileName());
    warning_text += QString("on line %1:\n").arg(lineNumber());
    warning_text += errorString();
  }
  config_file->close();
}


void TRP_Config::readThreads() {
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "threads") break;
    if (isStartElement()) {
      if (name() == "thread") {
        TRP_Config::Thread thread;
        QXmlStreamAttributes a_list = attributes();
        thread.name = readElementText();
        if (a_list.value("alias").isEmpty()) {
          thread.alias = thread.name;

        } else {
          thread.alias = a_list.value("alias").toString();
        }

        if (a_list.value("unit").isEmpty()) {
          thread.unit = "Hz";
        } else {
          thread.unit = a_list.value("unit").toString();
        }

        threads.push_back(thread);
      }
    }
  }
}

void TRP_Config::readGeneral() {
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "general") break;
    if (isStartElement()) {
      if (name() == "triggerdb") {
        triggerdb_name = readElementText();
      } else if (name() == "partition") {
        partition_name = readElementText();
      } else if (name() == "server") {
        server_name = readElementText();
      } else if (name() == "history") {
        history_size = readElementText().toUInt();
      }
    }
  }
}

void TRP_Config::readPlotPage() {
  QString title_text = "";
  std::deque<TRP_Config::Plot> plot_list;
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "plotpage") break;
    if (isStartElement()) {
      if (name() == "title") {
        title_text = readElementText();
      } else if (name() == "plot") {
        TRP_Config::Plot plot = readPlot();
        plot_list.push_back(plot);
      }
    }
  }
  TRP_Config::PlotPage plot_page;
  plot_page.title = title_text;
  plot_page.plots = plot_list; 
  plot_pages.push_back(plot_page);
}

void TRP_Config::readCorrPlotPage() {
  QString title_text = "";
  std::deque<TRP_Config::Plot> plot_list;
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "corrplotpage") break;
    if (isStartElement()) {
      if (name() == "title") {
        title_text = readElementText();
      } else if (name() == "corrplot") {
	TRP_Config::Plot corrplot = readCorrPlot();
	plot_list.push_back(corrplot);
      }
    }
  }

  TRP_Config::PlotPage plot_page;
  plot_page.title = title_text;
  plot_page.plots = plot_list; 
  plot_pages.push_back(plot_page);
}

TRP_Config::Plot TRP_Config::readCorrPlot() {
  QString title_text = "";
  std::deque<TRP_Config::Trend> trend_list;
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "corrplot") break;
    if (isStartElement()) {
      if (name() == "title") {
        title_text = readElementText();
      } else if (name() == "trend") {
        TRP_Config::Trend trend = readTrend();
        trend_list.push_back(trend);
      }
    }
  }

  TRP_Config::Plot plot;
  plot.title = title_text;
  plot.trends = trend_list;
  return plot;
}

TRP_Config::Plot TRP_Config::readPlot() {
  QString title_text = "";
  std::deque<TRP_Config::Trend> trend_list;
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "plot") break;
    if (isStartElement()) {
      if (name() == "title") {
        title_text = readElementText();
      } else if (name() == "trend") {
        TRP_Config::Trend trend = readTrend();
        trend_list.push_back(trend);
      }
    }
  }
  TRP_Config::Plot plot;
  plot.title = title_text;
  plot.trends = trend_list;
  return plot;
}

TRP_Config::Trend TRP_Config::readTrend() {
  unsigned int n(0);
  QString x, y;
  while (!atEnd()) {
    readNext();
    if (isEndElement() && name() == "trend") break;
    if (isStartElement()) {
      if (name() == "thread") {
        n = readElementText().toUInt();
        if (n > threads.size() - 1) {
          warning_text  = QString("Error on line %1:\n")
                          .arg(lineNumber());
          warning_text += QString("Plotting from thread %1, ")
                          .arg(n);
          warning_text += QString("though only %1 threads were specified.\n")
                          .arg(threads.size());
          warning_text += "(Thread numbering starts at 0.)\n";
        }
      } else if (name() == "x") {
        x = readElementText();
      } else if (name() == "y") {
        y = readElementText();
      }
    }
  }
  TRP_Config::Trend trend;
  trend.n_thread = n;
  trend.x_name = x;
  trend.y_name = y;
  return trend;
}

void TRP_Config::setDefaults() {
  partition_name = QString(getenv("TDAQ_PARTITION"));
  if (partition_name.isEmpty()) {
    partition_name = "ATLAS";
  }
  triggerdb_name = "TRIGGERDB";
  server_name = "ISS_TRP";
  history_size = 1000;
}

QString TRP_Config::warnings() const {
  return warning_text;
}

QString TRP_Config::triggerDBName() const {
  return triggerdb_name;
}

QString TRP_Config::partitionName() const {
  // partition_name priority:
  // 1. command line
  // 2. configuration file
  // 3. environment variable $TDAQ_PARTITION
  // 4. "ATLAS"
  return partition_name;
}

QString TRP_Config::serverName() const {
  return server_name;
}

unsigned int TRP_Config::historySize() const {
  return history_size;
}

std::ostream& operator << (std::ostream& ostr, const TRP_Config::Trend & tr){
  ostr << "TRP_CONFIG::Trend " 
       << tr.x_name.toStdString() << " " 
       << tr.y_name.toStdString() << '\n';
  return ostr;
}

