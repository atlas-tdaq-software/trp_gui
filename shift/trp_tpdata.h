/** @file trp_isthread.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_TPData: thread class for subscribing to IS
 */

#ifndef TRP_TPDATA_H
#define TRP_TPDATA_H

#include <TRP/TimePoint_IS.h>

#include <string>
#include <algorithm>
#include <chrono>
#include <deque>
#include <memory>
#include <QMutex>
#include <QObject>
#include <vector>
#include <set>
#include <QPointF>


class TRP_TPData : public QObject {
  Q_OBJECT

public:
  TRP_TPData(unsigned int nhistory,
             const QString& name,
             const QString& alias,
             const QString& unit,
             bool history_enabled,
             bool debugOn,
             QObject* parent);

  ~TRP_TPData();

  QString getName() const noexcept;
  void getPoints(const QString& name_x, 
                 const QString& name_y,
                 std::deque<std::shared_ptr<QPointF>>& points) const noexcept;

  void getLatestValue(const QString& name_x,
                      const QString &name_y,
                      float& value) const noexcept;

  std::shared_ptr<TimePoint_IS> latest() const noexcept;

  std::deque<std::shared_ptr<TimePoint_IS>> getTimePoints() const noexcept;
  
  unsigned int size() const noexcept;

  bool isHistoryEnabled() const noexcept;
  void setHistoryEnabled(bool state);
  bool isHistoryDiff() const noexcept;

  unsigned int getHistoryPoint() const noexcept;
  unsigned int getHistoryStep() const noexcept;

  void getHistoryValue(const QString& name_x,
                       const QString& name_y, 
                       float &value) const;

  unsigned int getNhistory() const noexcept;
  QString getAlias() const noexcept;
  QString getUnit() const noexcept;

  std::shared_ptr<TimePoint_IS> history(int i) const noexcept;
  
  void updateHistoryPoint(unsigned int point);
  void updateHistoryStep(int step);
  void setTimePoints(const std::deque <std::shared_ptr<TimePoint_IS>>&);
  void addTimePoint(const std::shared_ptr<TimePoint_IS>&);

  void modifyTimePoint (TimePoint_IS*);

  // summary for debugging  std::string getSummary() const;
  std::string toString() const;
  std::string toStringShort() const;

  //need to remove these contructors - required by QObject...
  TRP_TPData(const TRP_TPData&) = delete;
  TRP_TPData& operator = (const TRP_TPData&) = delete;

private:
  QString m_name;
  QString m_alias;
  QString m_unit; // like Hz...
  unsigned int m_nhistory;
  std::deque <std::shared_ptr<TimePoint_IS>> m_time_points;
  mutable QMutex m_mutex;
  bool m_historyEnabled{true};
  unsigned int m_historyPoint{999};
  int m_historyStep{0};
  bool m_historyDiff{false};
  bool m_historyLock{false};
  
  bool m_updated{false};
  std::chrono::system_clock::time_point m_lastUpdate;
  bool m_debugOn;

signals:
  void gotData();   // fired from TRP_ISThread
  void updateSpinboxLimits(int); // fired from TRP_ISThread
  void historyEnabled();
  void gotNewHistoryPoint();
  void gotNewHistoryStep();
  void gotNewHistoryLock();
  void gotNewHistoryDiff();
  void gotHistoryPoint(QString,unsigned int);
  void updateHistoryTimeLabel(int); // fired from TRP_ISThread


public slots:
  void updateHistoryDiff(int);
  void lockHistoryPoint();

private slots:
  void updateHistoryLock(int);
};

std::ostream& operator << (std::ostream&, const std::shared_ptr<TRP_TPData>&);
std::ostream& operator << (std::ostream&, const TRP_TPData*);

#endif
