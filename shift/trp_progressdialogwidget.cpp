#include "trp_progressdialogwidget.h"
#include <string>
// #include <iostream>

TRP_ProgressDialogWidget::TRP_ProgressDialogWidget(int max,
                                                   const std::string& title,
                                                   QObject* parent):
  QThread(parent), m_val(0), m_widget(new QProgressDialog(title.c_str(), 0, 0, max, nullptr)){
  m_widget->setWindowModality(Qt::WindowModal);
  m_widget->setMinimumDuration(0);
  m_widget->setValue(m_val);
  m_widget->update();
}

  
void TRP_ProgressDialogWidget::update(){
  m_widget->update();
}

TRP_ProgressDialogWidget::~TRP_ProgressDialogWidget(){delete m_widget;}

void TRP_ProgressDialogWidget::bump(){
  m_val += 1;
  m_widget->setValue(m_val);
  m_widget->update();
}
