#include <QtGui>

#include "trp_model.h"

TRP_Model::TRP_Model(TRP_TPDataPtr t, QObject *parent): 
  TRP_BaseModel(parent), m_tpData(t) {

  connect(m_tpData,
          SIGNAL(gotData()),
          this,
          SIGNAL(layoutAboutToBeChanged()));

  // history
  connect(m_tpData,
          SIGNAL(gotNewHistoryPoint()),
          this,
          SIGNAL(layoutAboutToBeChanged()));

  connect(m_tpData,
          SIGNAL(gotNewHistoryStep()),
          this,
          SIGNAL(layoutAboutToBeChanged()));

  connect(m_tpData,
          SIGNAL(gotNewHistoryLock()),
          this,
          SIGNAL(layoutAboutToBeChanged()));

  connect(m_tpData,
          SIGNAL(gotNewHistoryDiff()),
          this,
          SIGNAL(layoutAboutToBeChanged()));

  connect(m_tpData,
          SIGNAL(historyEnabled()),
          this,
          SIGNAL(layoutAboutToBeChanged()));
}


int TRP_Model::rowCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  if (m_tpData->size() == 0) return 0;
  return m_tpData->latest()->XLabels.size();
}


int TRP_Model::columnCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  if (m_tpData->size() == 0) return 0;
  return m_tpData->latest()->YLabels.size()+1;
}

QVariant TRP_Model::data(const QModelIndex &index, int role) const {

  if (!index.isValid() || m_tpData->size()==0) return QVariant();

  if (role == Qt::DisplayRole) {
    // Vertical header column
    if (index.column()==0) return xTitle(index);
    float value;
    if (!m_tpData->isHistoryEnabled()) {
      m_tpData->getLatestValue(xTitle(index), yTitle(index), value);
    } else {
      m_tpData->getHistoryValue(xTitle(index), yTitle(index), value);
    }
    return value;
  }
  else if (role == Qt::TextAlignmentRole) {
    // center vertically, and left-align vertical labels
    return QVariant(Qt::AlignVCenter | ((index.column()!=0) ? Qt::AlignRight : Qt::AlignLeft));
  }
  else if (role == Qt::BackgroundColorRole) {
    return (index.column()!=0) ? QVariant() : qApp->palette().color(QPalette::Button);
  }
  else return QVariant();
}


QVariant TRP_Model::headerData(int section, Qt::Orientation orientation,
                    int role) const {
  
  if (role != Qt::DisplayRole || m_tpData->size() == 0) return QVariant();

  if (orientation == Qt::Horizontal) {
    if (section==0) return QString("Name");
    QString hlabel = QString::fromStdString(m_tpData->latest()->YLabels[section-1]);
    return hlabel;
  }
  else return QVariant();
}

void TRP_Model::update()
{
  emit layoutChanged();
}

QString  TRP_Model::xTitle(const QModelIndex &index ) const {
  return QString::fromStdString(m_tpData->latest()->XLabels[index.row()]);
}


QString  TRP_Model::yTitle(const QModelIndex &index ) const {
  return headerData(index.column(), Qt::Horizontal).toString();
}
