#include <iostream>
#include <ipc/core.h>
#include <QtGui>
#include <QtWidgets>
#include <QApplication>
#include "trp_config.h"
#include "trp_mainwindow.h"
#include <TSystem.h>
#include <csignal>

#include <iostream>
void usage();

int main(int argc, char *argv[]) {

  // reset all the ROOT signal handlers
  // kMAXSIGNALS is set in a ROOT enum, ESignals is a root enum, 
  for (int sig = 0; sig < kMAXSIGNALS; sig++){
    auto es = static_cast<ESignals>(sig); // cast int to ESignals (enum)
    gSystem->ResetSignal(es);  // set default handler for signal sig
  }

  

  QString conf_file = "";
  QString part_name = "";
  unsigned int hist_limit= 0;
  unsigned int run_number = 0;
  QString root_file = QString::null;
  bool debugOn {false};
  for (int i = 0; i < argc; i++) {
    QString arg = argv[i];
    if (arg == "-h" || arg == "--help") {
      usage();
      exit(0);
    } else if (arg == "-H") {
      hist_limit = atoi(argv[i+1]);
    } else if (arg == "-c") {
      conf_file = argv[i+1];
    } else if (arg == "-p") {
      part_name = argv[i+1];
    } else if (arg == "-r") {
	  run_number = atoi(argv[i+1]);
	} else if (arg == "-f") {
      root_file = QDir::current().absolutePath()+"/"+QString::fromLatin1(argv[i+1]);
	} else if (arg == "-d") {
      debugOn = true;
    }
  }


  QApplication trp_application(argc, argv);
  IPCCore::init(argc, argv);

  TRP_Config *trp_config = new TRP_Config(conf_file, part_name, hist_limit);
  TRP_MainWindow trp_mainwindow(trp_config, run_number, root_file, debugOn);

  trp_mainwindow.show();
  return trp_application.exec();
}

void usage() {
  std::cout 
  << "trp [-c conf_file] [-p part_name] \n"
  << "Description:\n"
  << "  Launches the graphical user interface (GUI)\n"
  << "  of the Trigger Rate Presenter (TRP).\n"
  << "Options:\n"
  << " -c conf_file   Specify the configuration file to use.\n"
  << " -p part_name   Specify the partition. This option overrides the\n"
  << "                partition specified in the configuration file.\n"
  << " -f filename    Load TRP rates from ROOT file.\n"
  << " -r run_number  Load TRP rates via run number.\n";
}

