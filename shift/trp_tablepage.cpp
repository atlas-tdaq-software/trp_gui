#include <iostream>

#include <QtGui>
#include <QtWidgets>
#include "trp_isthread.h"
#include "trp_table.h"
#include "trp_base_model.h"
#include "trp_tablepage.h"


// set up a table (Tables, History Tables) for each the information from each TRP_ISThread

TRP_TablePage::TRP_TablePage(const TRP_TPDataContainer& tpContainer, 
                             const TRP_Config *c, 
                             QWidget *parent)
   : QWidget(parent),
     m_tpContainer(tpContainer), 
     m_filterText(""), 
     m_tabHideCheckbox(0) {
  setTableID(0); //Set default value

  /* This class is a QWidget Instances will be added to the the main
     QTabWidget in TRP_MainPage with titles "Tables" and "HistoryTables"

     This constructor sets up its own QTabWidget to allow a complex display
     to be presented in the main window twhen one of the Tables tabs is clicked.
     Clicking on the main window "Tables" tab will display this QTabWidget.

     The QTabwidget will be set up according to the state of the 
     boolean "history_enabled" flag of the data objects (TRP_TPData).

     The position in the window that the widget will be diplayed is determined
     by the QVBoxLayout object wich is instantiated toward the end of this 
     constructor. This places the tab_widget at the bottom of the page.
   */

  //  std::cout << "TRP_TablePage start constuctor\n";
  //  if (tpContainer.empty()){
  //    std::cout << "time point container empty\n";
  //  } else {
  //    std::cout << "first time point\n";
  //    std::cout << tpContainer[0]->toString() <<'\n';
  //  }
    
  m_tabwidget = new QTabWidget(this);
  int i{0};
  for (auto tp : tpContainer){
    TRP_Table *table = new TRP_Table(tp, this);
    m_tables.push_back(table);
    m_tabwidget->addTab(table, tp->getAlias());
    
    if (m_tabwidget->tabText(i) == "L1") {
      m_tabHideCheckbox.push_back(i);
    }
    ++i;
  }

  /* set up some connections
     for history enabled containers (this flag is set in TRP_MainWindow)
     These communicate with widgets that are displayed only of the
     history enabled flag is set in the  TPR_TPData objects
   */

  if (tpContainer[0]->isHistoryEnabled()) {
    connect(tpContainer[0],
            SIGNAL(updateSpinboxLimits(int)),
            this,
            SLOT(updateSpinboxLimits(int)));
    
    for (auto tp : tpContainer) {
      connect(tp,
              SIGNAL(updateHistoryTimeLabel(int)),
              this,
              SLOT(updateHistoryTimeLabelIfCurrent(int)));
    }
  }
    

  /* 
     for non-history enabled containers add a Chains tab
     to the tab_widget under constructon.
     This tab is associated with a TRP_Table with attribute
     table model havng a concrete type TRP_ChainModel 
     (ie sees all the TRP_TPData objects).
   */
  if(!tpContainer[0]->isHistoryEnabled()){
    TRP_Table *table = new TRP_Table(tpContainer, c, this);
    m_tables.push_back(table);
    m_tabwidget->addTab(table, "Chains");
    if (m_tabwidget->tabText(tpContainer.size()) == "Chains") {
      m_tabHideCheckbox.push_back(tpContainer.size());
    }
  } else {
    // If history _is_ enabled (i.e. if reading from ROOT file) look for the 'L1_Rate' tab, and add it to this list of tables to be affected by 'hide' instructions (i.e. 'L1 Enabled').
    for (unsigned idx = 0; idx < tpContainer.size(); idx++) {
      if (m_tabwidget->tabText(idx) == "L1_Rate") {
	m_tabHideCheckbox.push_back(idx);
      }
    }
  }

  
  // start work on a new visual layer for the tab_widget:
  // set up the buttons to make plots. This will raise signals
  // connected to TRP_MainWindow::makeplot()
  
  QPushButton *plot_button = new QPushButton("&Plot", this);
  connect (plot_button, SIGNAL(clicked()),
           this, SLOT(makePlot()));
  
  QPushButton *plotCorrelation_button = new QPushButton("Plot Correlation", this);
  connect (plotCorrelation_button, SIGNAL(clicked()),
           this, SLOT(makeCorrelationPlot()));
  
  QPushButton *clear_button = new QPushButton("&Clear", this);
  connect (clear_button, SIGNAL(clicked()),
           this, SLOT(clear()));
  
  // history
  source_model = m_tables[0]->getModel();
  h_enabled = m_tpContainer[0]->isHistoryEnabled();

  m_point = 999;
  m_step  = 0;
  if (h_enabled) {
    
    datetime_spinbox = new QDateTimeEdit(m_tabwidget);
    datetime_spinbox->setWrapping(true);
    datetime_spinbox->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
    datetime_spinbox->setAlignment(Qt::AlignRight);
	
    connect (datetime_spinbox, SIGNAL(dateTimeChanged(QDateTime)),
             this, SLOT(updateHistoryPoints(QDateTime)));
    
    lumiblock_spinbox = new QSpinBox(m_tabwidget);
    lumiblock_spinbox->setWrapping(true);
    lumiblock_spinbox->setAlignment(Qt::AlignRight);
	
    connect (lumiblock_spinbox, SIGNAL(valueChanged(int)),
             this, SLOT(getHistoryPointsFromLumiblock(int)));
    
    connect(parent, SIGNAL(updateHistoryStepDialog(int)),
            this, SLOT(updateHistoryStepDialog(int)));

    // Time labels
    for (unsigned int i = 0; i < tpContainer.size(); ++i) {
      m_history_time_labels[i] = new QLabel("", this);
      //          history_time_labels[i]->hide();

      connect(this, 
              SIGNAL(gotTime(QString)),
              m_history_time_labels[i], 
              SLOT( setText(QString) ));

      connect(m_tabwidget,
              SIGNAL(currentChanged(int)),
              this, 
              SLOT(updateHistoryTimeLabel(int)));

      for (auto tp : tpContainer) {
        connect(tp,
                SIGNAL(gotHistoryPoint(QString,unsigned int)),
                this,
                SLOT(updateHistoryTimeLabel(QString,unsigned int)));
      }
      
    }
    
  }

  connect (m_tabwidget,
           SIGNAL(currentChanged(int)),
           this, 
           SLOT(isTabClicked(int)));
  
  // hide disabled
  // Hide checkbox:
  if (m_tabHideCheckbox.size()>0) {
    connect (m_tabwidget,
             SIGNAL(currentChanged(int)),
             this, 
             SLOT(isTabClicked(int)));
    hide_checkbox = new QCheckBox("&L1 Enabled", this);
    hide_checkbox->setChecked(false);
    connect (hide_checkbox, SIGNAL(stateChanged(int)),
             this, SLOT(toggleHide(int)));
  }
  
  top_layout = new QHBoxLayout;
  top_layout->addWidget(plot_button);
  top_layout->addWidget(plotCorrelation_button);
  top_layout->addWidget(clear_button);
  if (!tpContainer[0]->isHistoryEnabled()) {
    // Add stretch on the correct side of the 'L1 Enabled' button, in order to ensure tidy layout.
    top_layout->addStretch();
  }
	  
  if (m_tabHideCheckbox.size()>0) top_layout->addWidget(hide_checkbox);
  
  if (tpContainer[0]->isHistoryEnabled()) {
    // Add stretch on the correct side of the 'L1 Enabled' button, in order to ensure tidy layout.
    top_layout->addStretch();
  }

  // history
  if (h_enabled) {
    QLabel *time_label = new QLabel("Time:");
    QLabel *lb_label = new QLabel("LB:");
    top_layout->addWidget(time_label);
    top_layout->addSpacing(130);
    top_layout->addWidget(lb_label);
    top_layout->addSpacing(32);
  }
  
  // Filters: 
  
  QLabel *filter_label = new QLabel("&Filter:", this);
  m_filterline = new QLineEdit(this);
  filter_label->setBuddy(m_filterline);
  connect (m_filterline, SIGNAL(textChanged(QString)),
           this, SLOT(filterChanged(QString)));
  
  m_filtercheckbox = new QCheckBox("&Enable filter", this);
  m_filtercheckbox->setChecked(true);
  connect (m_filtercheckbox, SIGNAL(stateChanged(int)),
           this, SLOT(toggleFilter(int)));
  
  middle_layout = new QHBoxLayout;
  middle_layout->addWidget(filter_label);
  middle_layout->addWidget(m_filterline);
  middle_layout->addWidget(m_filtercheckbox);
  if (h_enabled) {
    middle_layout->addStretch();
    middle_layout->addWidget(datetime_spinbox);
    middle_layout->addWidget(lumiblock_spinbox);
	
    middle_layout2 = new QHBoxLayout;
    middle_layout2->addStretch();
    for (unsigned int i = 0; i < tpContainer.size(); ++i) {
      middle_layout2->addWidget(m_history_time_labels[i]);
    }
	
  }
  
  
  // Main Layout
  
  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->addLayout(top_layout);
  main_layout->addLayout(middle_layout);
  if (h_enabled) main_layout->addLayout(middle_layout2);
  main_layout->addWidget(m_tabwidget);
  
  setLayout(main_layout);
  
  if (h_enabled) updateSpinboxLimits(0);  
}

void TRP_TablePage::applyFilter(const QString& text) {
   m_filterText = text;
   for (auto table : m_tables) {
     table->applyFilter(text);
   }
}

void TRP_TablePage::filterChanged(const QString& text) {
   // Enable filter if text was changed
   if (not m_filtercheckbox->isChecked()) {
      m_filtercheckbox->blockSignals(true);
      m_filtercheckbox->setChecked(true);
      m_filtercheckbox->blockSignals(false);
   }
   applyFilter(text);
}

void TRP_TablePage::toggleFilter(int state) {

  if (state==Qt::Checked)
    {
      applyFilter(m_filterline->text());
    } else {
    applyFilter("");}
}


void TRP_TablePage::clear() {
  for (auto table : m_tables) {table->selectionModel()->clearSelection();}
}

void TRP_TablePage::makePlot() {
  
  std::deque<TRP_Config::Trend*> trend_list;
  int i{0};
  for (auto table : m_tables) {
    TRP_BaseModel *source = table->getModel();
    QSortFilterProxyModel *proxy_model = table->getProxyModel();
    QList<QModelIndex> list = table->selectionModel()->selectedIndexes();
    
    for (auto proxy_index : list) {      
      QModelIndex source_index = proxy_model->mapToSource(proxy_index);
      TRP_Config::Trend *trend = new TRP_Config::Trend;
      int thread = table->getModel()->getThreadIndex(source_index.column()); 
      
      // if a normal table, use i, if a chain table use the given thread
      trend->n_thread = thread>=0 ? thread : i; 
      trend->x_name = source->xTitle(source_index);
      trend->y_name = source->yTitle(source_index);
      trend_list.push_back(trend);
    }
    ++i;
  }

  if (trend_list.size() == 0) {
    QMessageBox message(QMessageBox::Information,
                        QString("Note"),
                        QString("Could not make plot."),
                        QMessageBox::Ok,
                        this);
    message.setInformativeText("No table cells selected.");
    message.exec();
  } else {
    emit plotRequest(trend_list, m_tableID);
  }
}


void TRP_TablePage::setXtrend(TRP_Config::Trend & trendx){

  m_xxtrend.n_thread = trendx.n_thread;
  m_xxtrend.x_name = trendx.x_name;
  m_xxtrend.y_name = trendx.y_name;

}

void TRP_TablePage::makeCorrelationPlot() {
  
  //  int thisTab=m_tabwidget->currentIndex();

  TRP_Config::Trend *trendx = new TRP_Config::Trend;
  TRP_Config::Trend x_trend;

  //if x_trend previously selected.
  x_trend=getXtrend();
  if (x_trend.x_name !=""){
      trendx->n_thread = x_trend.n_thread;
      trendx->x_name = x_trend.x_name;
      trendx->y_name = x_trend.y_name;    
  }
 
  //if not changed tabs BUT x_trend reselected
  if (m_tabsVisited.size()==0){
    x_trend= m_tables[m_lastTabVisited]->getXtrend();
    if (x_trend.x_name !=""){
      trendx->n_thread = m_lastTabVisited;
      trendx->x_name = x_trend.x_name;
      trendx->y_name = x_trend.y_name;    
    }
  }

 //if changed tabs, check all tabsVisited, to find last selection.
  for (unsigned int j = 0; j < m_tabsVisited.size(); ++j) {
    unsigned int i = m_tabsVisited[j];
    x_trend= m_tables[i]->getXtrend();
    
    if (x_trend.x_name =="")continue;
    trendx->n_thread = i;
    trendx->x_name = x_trend.x_name;
    trendx->y_name = x_trend.y_name;    
    
    //clear
    m_tables[i]->clearXtrend();
  }
  m_tabsVisited.clear();
  //need to keep last selection  
  setXtrend(*trendx);

  std::deque<TRP_Config::Trend*> trend_list;
  std::deque<TRP_Config::Trend*> ytrend_list;


  int i{0};
  for (auto table : m_tables) {
    TRP_BaseModel *source = table->getModel();
    QSortFilterProxyModel *proxy_model = table->getProxyModel();
    
    QList<QModelIndex> list = table->selectionModel()->selectedIndexes();

    for (auto proxy_index : list){
      QModelIndex source_index = proxy_model->mapToSource(proxy_index);
      
      TRP_Config::Trend *trend = new TRP_Config::Trend;
      int thread = table->getModel()->getThreadIndex(source_index.column()); 
      trend->n_thread = thread>=0 ? thread : i; //if a normal table, use i, if a chain table use the given thread
      trend->x_name = source->xTitle(source_index);
      trend->y_name = source->yTitle(source_index);
      //trend_list.push_back(trend);
      
      if (trendx->x_name == trend->x_name && trendx->y_name == trend->y_name) continue;
      ytrend_list.push_back(trend);
    }
    ++i;
  }
  if (trendx->x_name !="" && ytrend_list.size()>0){
    trend_list.push_back(trendx);   
    for (unsigned int j = 0; j < ytrend_list.size(); ++j) {
      trend_list.push_back(ytrend_list[j]);  
    }
  }
  
  if (trendx->x_name =="" && ytrend_list.size()>0 ) {
    QMessageBox message(QMessageBox::Warning,
			QString("Warning"),
			QString("Could not make correlation plot."),
			QMessageBox::Ok,
			this);
    message.setInformativeText("Double click on cell to select x-axis trend.");
    message.exec();
  }
  else if (trendx->x_name !="" && ytrend_list.size()<1 ){
    QMessageBox message(QMessageBox::Warning,
			QString("Warning"),
			QString("Could not make correlation plot."),
			QMessageBox::Ok,
			this);
    message.setInformativeText("Need to select y-axis trends.  Use Ctrl key if selecting trends from same tab.");
    message.exec();
  }
  else if (trendx->x_name =="" && ytrend_list.size() <1 ) {
    QMessageBox message(QMessageBox::Warning,
			QString("Warning"),
			QString("Could not make correlation plot."),
			QMessageBox::Ok,
			this);
    message.setInformativeText("Double click on cell to select x-axis trend.  Select cells as normal for y-axis trends");
    message.exec();
  } 
  else{
    emit plotCorrelationRequest(trend_list, m_tableID);
  }
}


// hide disabled
void TRP_TablePage::toggleHide(int state) {
   if (state==Qt::Checked) { applyHide(true); }
   else { applyHide(false); }
}

void TRP_TablePage::applyHide(bool hidedisabled) {
  for (auto tabidx : m_tabHideCheckbox) {
    m_tables[tabidx]->applyHide(hidedisabled);
     m_tables[tabidx]->applyFilter(m_filterText);
  }
}

// history
void TRP_TablePage::updateHistoryPointDialog(unsigned int point) {

   OWLTime print_time = m_tpContainer[0]->history(point)->TimeStamp;
   QString print_value;
   const char *axis_value_format(0);
   axis_value_format = "%.2d:%.2d:%.2d";
   print_value.sprintf(axis_value_format,
         print_time.hour(), print_time.min(), print_time.sec());
   m_point = point;
//   updateHistoryStepDialogRange(m_point);
//   history_point_spinbox->setValue(point);

   for (unsigned int i = 0; i < m_tpContainer.size(); ++i) {
      m_tpContainer[i]->updateHistoryPoint(point);
      if (m_tpContainer[i]->getAlias() == 
          m_tabwidget->tabText(m_tabwidget->currentIndex())) {

        updateHistoryTimeLabel(i, point);
      }
   }

}

void TRP_TablePage::updateHistoryPoints(QDateTime datetime) {

   if (m_tpContainer.size() == 0 or m_tpContainer.at(0)->getTimePoints().size() == 0) return;

   std::vector<unsigned int> point_vec = findNearestHistoryPoints(datetime);
   
   for (unsigned int i = 0; i < m_tpContainer.size(); ++i) {
//      history_point_spinbox->setValue(point_vec.at(i));
	  
	  //Update lumiblock spinbox without calling a new timepoint search
	  if (i == 0) {
	    lumiblock_spinbox->blockSignals(true);
	    int lumiblock = m_tpContainer.at(i)->getTimePoints().at(point_vec.at(i))->LumiBlock;
	    lumiblock_spinbox->setValue(lumiblock);
	    lumiblock_spinbox->blockSignals(false);
	  }
      m_tpContainer.at(i)->updateHistoryPoint(point_vec.at(i));
      if (m_tpContainer.at(i)->getAlias() == m_tabwidget->tabText(m_tabwidget->currentIndex())) {
//        updateHistoryStepDialogRange(m_point);
		updateHistoryTimeLabel(i, point_vec.at(i));
      }
   }
   
}

// Find the nearest history point to the argument given. Do this for every thread individually.
std::vector<unsigned int> TRP_TablePage::findNearestHistoryPoints(QDateTime datetime) {
  std::vector<unsigned int> nearest_points;
  int min_secs;
  unsigned int diff_secs;
  unsigned int nearest_point;
  // std::deque<TimePoint_IS*> time_points;
  std::deque<std::shared_ptr<TimePoint_IS>> time_points;
  
  for (unsigned int i_th=0; i_th<m_tpContainer.size(); ++i_th) {
    min_secs = -1;
	bool minimum_passed = false;
	time_points = m_tpContainer.at(i_th)->getTimePoints();
    for (unsigned int i_p=0; i_p<time_points.size(); ++i_p) {
      diff_secs = secDiff(datetime,time_points.at(i_p)->TimeStamp);
      if (min_secs >= 0) minimum_passed = (diff_secs > (unsigned int)abs(min_secs));
      if (diff_secs < (unsigned int)abs(min_secs) or min_secs == -1) {
		nearest_point = i_p;
		min_secs = diff_secs;
	  }
	  if (minimum_passed) break;
    }
	nearest_points.push_back(nearest_point);
  }
  
  return nearest_points;
}

unsigned int TRP_TablePage::secDiff(QDateTime qdatetime, OWLTime owltime) {
  int diff = 0;
  
  diff += ( qdatetime.date().year()   - owltime.year()      ) * 31536000; //* 60 * 60 * 24 * 365
  diff += ( qdatetime.date().month()  - (owltime.month())   ) * 2592000;  //* 60 * 60 * 24 * 30
  diff += ( qdatetime.date().day()    - owltime.day()       ) * 86400;    //* 60 * 60 * 24;
  diff += ( qdatetime.time().hour()   - owltime.hour()      ) * 3600;     //* 60 * 60;
  diff += ( qdatetime.time().minute() - owltime.min()       ) * 60;
  diff += ( qdatetime.time().second() - owltime.sec()       );
  
  unsigned int normdiff = abs(diff);
  return normdiff;
}

void TRP_TablePage::getHistoryPointsFromLumiblock(int lumiblock) {
  // std::deque<TimePoint_IS*> time_points;
  std::deque<std::shared_ptr<TimePoint_IS>> time_points;

  for (unsigned int i_th = 0; i_th < m_tpContainer.size(); ++i_th) {
    time_points = m_tpContainer.at(i_th)->getTimePoints();
	
    for (unsigned int i_p = 0; i_p < time_points.size(); ++i_p) {
      if (time_points.at(i_p)->LumiBlock == lumiblock) {
	    if (i_th == 0) {
		  OWLTime timestamp = time_points.at(i_p)->TimeStamp;
		  QDate date(timestamp.year(),timestamp.month(),timestamp.day());
		  QTime time(timestamp.hour(),timestamp.min(),timestamp.sec());
		  QDateTime datetime(date,time);
		  
		  //Update datetime spinbox without triggering another timepoint search
		  datetime_spinbox->blockSignals(true);
		  datetime_spinbox->setDateTime(datetime);
	      datetime_spinbox->blockSignals(false);
		}
		m_tpContainer.at(i_th)->updateHistoryPoint(i_p);
		
		if (m_tpContainer.at(i_th)->getAlias() == m_tabwidget->tabText(m_tabwidget->currentIndex())) {
		  updateHistoryTimeLabel(i_th,i_p);
        }
		
		break;
	  
	  }
    }
	
  }
}

void TRP_TablePage::updateHistoryTimeLabel(unsigned int current, unsigned int point) {
   for (unsigned int i = 0; i < m_tpContainer.size(); ++i) {
      if (i!=current) m_history_time_labels[i]->hide();
   }
   m_history_time_labels[current]->show();
   if (point >= m_tpContainer[current]->size()) {
      emit gotTime("hh:mm:ss");
      return;
   }
   OWLTime print_time = m_tpContainer[current]->history(point)->TimeStamp;
   unsigned short print_lumiblock = m_tpContainer[current]->history(point)->LumiBlock;
   QString print_value;
   const char *axis_value_format(0); 
   axis_value_format = "Current: %d-%02u-%02u %.2d:%.2d:%.2d LB:%hu";
   print_value.sprintf(axis_value_format, 
         print_time.year(), print_time.month(), 
         print_time.day(),
         print_time.hour(), print_time.min(), print_time.sec(),
		 print_lumiblock);
   emit gotTime(print_value);

}
void TRP_TablePage::updateHistoryTimeLabel(int tabIndex) {
   if (tabIndex > -1) {
      unsigned int current = static_cast<unsigned int>(tabIndex);
      unsigned int point = mm_point[current];
      updateHistoryTimeLabel(current, point);
   }
}

void TRP_TablePage::updateHistoryTimeLabel(QString alias, unsigned int point) {
   unsigned int current = m_tabwidget->currentIndex();
   for (unsigned int i = 0; i < m_tpContainer.size(); ++i) {  
      if (m_tpContainer[i]->getAlias() == alias) {
         mm_point[i] = point;
      }
   }
   if (m_tpContainer[current]->getAlias() == alias) {
      updateHistoryTimeLabel(current, mm_point[current]);
   }
}

void TRP_TablePage::updateHistoryTimeLabelIfCurrent(int thread_number) {
   if (thread_number == m_tabwidget->currentIndex()) {
      unsigned int current = static_cast<unsigned int>(thread_number);
      unsigned int point = mm_point[current];
      updateHistoryTimeLabel(current, point);
   }
}

void TRP_TablePage::updateHistoryPointDialog(int point) {
   unsigned int ui_point = static_cast<unsigned int>(point);
   updateHistoryPointDialog(ui_point);
}


void TRP_TablePage::updateHistoryStepDialog(int step) {
//   if (step > 0) history_step_spinbox->setPrefix("+");
//   if (step < 0) history_step_spinbox->setPrefix("");
//   history_step_spinbox->setValue(step);
   for (unsigned int i = 0; i < m_tpContainer.size(); ++i) {
      m_tpContainer[i]->updateHistoryStep(step);
   }
}

//void TRP_TablePage::updateHistoryStepDialogRange(int point) {
//   history_step_spinbox->setMinimum(0-point);
//   history_step_spinbox->setMaximum(999-point);
//}

void TRP_TablePage::updateSpinboxLimits(int thread_number) {
 
  // std::deque<TimePoint_IS*> time_points = m_tpContainer.at(thread_number)->getTimePoints();
  std::deque<std::shared_ptr<TimePoint_IS>> time_points = 
    m_tpContainer.at(thread_number)->getTimePoints();

  if (time_points.size() == 0) return;

  OWLTime oldest = time_points.at(0)->TimeStamp;
  OWLTime newest = time_points.at(time_points.size()-1)->TimeStamp;
  
  QDate oldest_date(oldest.year(),oldest.month(),oldest.day());
  QDate newest_date(newest.year(),newest.month(),newest.day());
  QTime oldest_time(oldest.hour(),oldest.min(),oldest.sec());
  QTime newest_time(newest.hour(),newest.min(),newest.sec());
  QDateTime oldest_datetime(oldest_date,oldest_time);
  QDateTime newest_datetime(newest_date,newest_time);
  
  datetime_spinbox->blockSignals(true);
  datetime_spinbox->setDateTimeRange(oldest_datetime,newest_datetime);
  datetime_spinbox->blockSignals(false);
  
  int min_lumiblock = -1;
  int max_lumiblock = -1;
  for (unsigned int i=0; i<time_points.size(); ++i) {
    if (min_lumiblock == -1 or time_points.at(i)->LumiBlock < min_lumiblock) {
	  min_lumiblock = time_points.at(i)->LumiBlock;
	}
	if (max_lumiblock == -1 or time_points.at(i)->LumiBlock > max_lumiblock) {
	  max_lumiblock = time_points.at(i)->LumiBlock;
	}
  }
  
  lumiblock_spinbox->blockSignals(true);
  lumiblock_spinbox->setRange(min_lumiblock,max_lumiblock);
  lumiblock_spinbox->blockSignals(false);

}

void TRP_TablePage::isTabClicked(int currentTab) {
  m_tabsVisited.push_back(currentTab);
  m_lastTabVisited=currentTab;
}

QTabWidget* TRP_TablePage::getTabWidget() const noexcept {return m_tabwidget;}

TRP_Config::Trend TRP_TablePage::getXtrend() const noexcept {return m_xxtrend;}


std::deque <TRP_Table *> TRP_TablePage::getTables() const noexcept {return m_tables;}

std::vector<int> TRP_TablePage::getTabHideCheckbox() const noexcept {
  return m_tabHideCheckbox;}

QLineEdit* TRP_TablePage::getLineEdit() const noexcept {return m_historyline;}

TRP_TPDataContainer TRP_TablePage::getThreads() const noexcept{ return m_tpContainer; }

void TRP_TablePage::setTableID(int ID) {m_tableID = ID;}

