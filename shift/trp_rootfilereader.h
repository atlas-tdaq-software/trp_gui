#ifndef TRP_ROOTFILEREADER_H
#define TRP_ROOTFILEREADER_H

/* Class to read root files for the TRP.
The resulting time point data resides in m_tpdata, which can
be extracted and displayed by trpgui.
*/

// STL include(s)
#include <string>
#include <vector>
#include <set>
#include <regex>

// Qt include(s)
#include <QObject>
#include <QMutex>
#include <QString>
#include <QMessageBox>

// ROOT include(s)
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TLeaf.h>

// Package include(s)
#include <TRP/TimePoint_IS.h>
#include "./trp_typeAliases.h"
#include "trp_progressdialogwidget.h"


class QPointF;

class TRP_RootFileReader : public QObject{
  Q_OBJECT

public:
			   
  TRP_RootFileReader(TRP_TPDataPtr&,
                     const QString& branchName,
                     TFile* rootFile, 
                     QObject* parent = 0);
			   
  virtual ~TRP_RootFileReader();

  // no copy constructor, assign operator for QObjects
  TRP_RootFileReader(const TRP_RootFileReader &) = delete;
  TRP_RootFileReader& operator = (const TRP_RootFileReader &) = delete;
  
  // extract the time point data for displaying
  TRP_TPDataPtr getTimePointData() const noexcept;

  //debug
  std::string getSumary() const noexcept;

  void readRootFile(std::vector <TimePoint_IS> &tp_vec);
  void run(); // read root file, fill m_tpdata

private:
  QMutex m_mutex;
  TRP_TPDataPtr&  m_tpdata; // time point store and accessors. no ownership
  std::string m_branchName;  // root file branch read by this reader
  TFile* m_file;  // no ownership
  bool m_doSummary {true};  // debug flag

  // a place for threads to exists until they complete quiting
  std::vector<TRP_ProgressDialogWidget*> m_dialogProgressWidgets;
  


signals:
  void gotData();
  void updateProgressDialog();
  void bumpProgressDialog();

private slots:
  void updateHistoryDiff(int);
  void lockHistoryPoint();
};

#endif  // define TRP_ISTHREAD_H
