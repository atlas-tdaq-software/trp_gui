/** @file: trp_model.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_BaseModel: Model for TimePoint_IS object
 */

#ifndef TRP_BASEMODEL_H
#define TRP_BASEMODEL_H

#include <QAbstractItemModel>
// IRH 2011-03-14 added new class
#include <QAbstractItemDelegate>
// IRH get hold of the parent(s)
#include "trp_table.h"
#include "trp_tablepage.h"
#include "trp_typeAliases.h"
#include <TRP/TimePoint_IS.h>
#include <ostream>

class TRP_BaseModel : public QAbstractItemModel {
  Q_OBJECT

public:
  TRP_BaseModel(QObject *parent = 0);

private:
  TRP_BaseModel(const TRP_BaseModel &);
  TRP_BaseModel& operator = (const TRP_BaseModel &);

public:
  QModelIndex  parent (const QModelIndex &index) const override;

  QModelIndex  index (int row, int column, const QModelIndex &parent = QModelIndex()) const override;

  QString      virtual xTitle     (const QModelIndex &index) const=0;
  QString      virtual yTitle     (const QModelIndex &index) const=0;
  void         virtual update     ()=0;
  int          virtual getThreadIndex(int col=-1) const=0;
  bool         virtual isTitleColumn(int col) const=0;
  bool         virtual isChainTable() const=0;
  std::string display() const;
 // history
public:
  unsigned int getHistoryPoint() { return history_point; }
signals:
 void updateHistoryTimeLabel(unsigned int) const;
 //
 void historyPointChanged(unsigned int);
 void historySizeChanged(unsigned int);


private slots:
  void emitUpdateHistoryTimeLabel(unsigned int);

private:
  unsigned int history_point;
};

// IRH 2011-03-14 added new class
class TRP_Delegate : public QAbstractItemDelegate {
  Q_OBJECT

public:
  TRP_Delegate(QObject *parent = 0);

private:
  TRP_Delegate(const TRP_Delegate &);
  TRP_Delegate& operator = (const TRP_Delegate &);

public:
  void paint ( QPainter * painter, const QStyleOptionViewItem & option, 
               const QModelIndex & index ) const;
  QSize sizeHint ( const QStyleOptionViewItem & option, 
                   const QModelIndex & index ) const;
private:
  TRP_Table*     m_tt;
  TRP_TablePage* m_ttp;
  TRP_TPDataPtr  m_thread;
};

std::ostream& operator << (std::ostream&, const TRP_BaseModel&);
std::ostream& operator << (std::ostream&, const TRP_BaseModel*);
#endif  // define TRP_BASEMODEL_H



