#include "trp_unique_timepoints.h"
#include <TRP/TimePoint_IS.h>
#include <algorithm>

// helper class to identify duplicates
struct SameTime{
  bool operator() (const std::shared_ptr<TimePoint_IS>& l, 
                   const std::shared_ptr<TimePoint_IS>& r) const {
    return l->TimeStamp == r->TimeStamp;}
};

// helper class for sorting time points
struct TimePointSorter{
  bool operator()(const std::shared_ptr<TimePoint_IS>& l, 
                  const std::shared_ptr<TimePoint_IS>& r) const {
    return l->TimeStamp < r->TimeStamp;
  }
};

// helper class for shared ptr construction
struct ToPtr{
  std::shared_ptr<TimePoint_IS> operator()(const TimePoint_IS& tp){
    return std::make_shared<TimePoint_IS>(tp);
  }
};

std::deque <std::shared_ptr<TimePoint_IS>>
trp_unique_timepoints(std::vector <TimePoint_IS>& tps){
  std::deque <std::shared_ptr<TimePoint_IS>> time_points;

  // convert incomming time points to pointers
  std::transform(tps.begin(),
                 tps.end(),
                 std::back_inserter(time_points),
                 ToPtr());

  // remove duplicates using unique. This requures a sorted container
  // so sort if necessay.
  if (not is_sorted(time_points.begin(), 
                    time_points.end(), 
                    TimePointSorter()))
           {
             std::sort(time_points.begin(), 
                       time_points.end(), 
                       TimePointSorter());
           }
  auto p = std::unique(time_points.begin(),
                       time_points.end(), 
                       SameTime());
  
  
  
  // remove residual capacity
  time_points.resize(p - time_points.begin());
  time_points.shrink_to_fit();
  return time_points;
}
