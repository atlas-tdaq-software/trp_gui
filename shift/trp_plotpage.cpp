#include <QGridLayout>
#include "trp_plot.h"
#include "trp_plotpage.h"

TRP_PlotPage::TRP_PlotPage(QWidget *parent)
  : QWidget(parent) {
  grid = new QGridLayout;
  setLayout(grid);
}

TRP_PlotPage::~TRP_PlotPage() {
  std::deque<TRP_Plot*>::const_iterator it;
  for (it = plots.begin(); it != plots.end(); ++it) {
    delete *it;
  }
  plots.clear();
}

void TRP_PlotPage::addPlot(TRP_Plot *new_plot) {
  plots.push_back(new_plot);
}

void TRP_PlotPage::formatPage() {
  unsigned int n_columns;
  
  if (plots.size() <= 3) {
    n_columns = 1;
  } else if (plots.size() == 4) {
    n_columns = 2;
  } else if (plots.size() <= 6 || plots.size() == 9) {
    n_columns = 3;
  } else {
    n_columns = 4;
  }

  unsigned int row(0), column(0);
  for (unsigned int i = 0; i < plots.size(); i++) {

    grid->addWidget(plots[i], row, column);
    ++column;
    if (column == n_columns) {
      column = 0;
      ++row;
    }
  }
}
