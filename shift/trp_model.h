/** @file: trp_model.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Model: Model for TimePoint_IS object
 */

#ifndef TRP_MODEL_H
#define TRP_MODEL_H

#include <QAbstractItemModel>
#include <TRP/TimePoint_IS.h>
#include "trp_typeAliases.h"
#include "trp_base_model.h"


// IRH  note new class declared and define here,
// possibly move to a separate file later
class TRP_Delegate; 

class TRP_Model : public TRP_BaseModel {
  Q_OBJECT

public:
  TRP_Model(TRP_TPDataPtr t, QObject *parent = 0);

  TRP_Model(const TRP_Model &) = delete;
  TRP_Model& operator = (const TRP_Model &) = delete;

  int rowCount    (const QModelIndex &parent = QModelIndex()) const override;
  int columnCount (const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

  QString xTitle(const QModelIndex &index ) const override;
  
  QString yTitle(const QModelIndex &index ) const override;
  void update() override;
  int getThreadIndex(int ) const override {return -1;}
  bool isTitleColumn(int col) const override {return col==0;}
  bool isChainTable() const override { return false; }

 private:
  TRP_TPDataPtr m_tpData;
};

#endif
