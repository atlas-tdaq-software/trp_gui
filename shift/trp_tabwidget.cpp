#include "trp_tabwidget.h"

TRP_TabWidget::TRP_TabWidget(QWidget* parent) {

  this->setParent(parent);
  connect(this,SIGNAL(tabCloseRequested(int)),
	      this,SLOT(closeTab(int)));

}

void TRP_TabWidget::closeTab(int index) {
  int tabID = this->tabWhatsThis(index).toInt();
  this->removeTab(index);
  emit deleteTabData(tabID);
}

QTabBar* TRP_TabWidget::tabBar() {
  return QTabWidget::tabBar();
}
