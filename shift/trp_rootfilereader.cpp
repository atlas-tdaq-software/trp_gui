#include <QPointF>
#include <owl/time.h>
#include <stdlib.h>

#include <iostream>
#include <sstream>

#include "trp_rootfilereader.h"
#include "trp_unique_timepoints.h"
#include <TRP/TimePoint_IS.h>

#include "trp_typeAliases.h"
#include "trp_unique_timepoints.h"

#include <QtGui>

#include <QLabel>
#include <QStatusBar>
#include <QProgressBar>

#include <TSystem.h>
#include <csignal>
#include <map>
#include <exception>


// ROOT signals are now reset to default value in main()
/*
namespace {
  volatile std::sig_atomic_t gSignalStatus;
}

void signalHandler(int signal){
  std::cout << "Signal " << signal << " caught\n";
  if(signal == SIGFPE) {
    std::cout << "Signal " << signal << " unrecoverable, exiting\n";
    exit(1);
  }
  //Allow ctrl-c to exit program
  if(signal == SIGINT) {
    std::cout << "Signal " << signal << " exiting\n";
    exit(1);
  }
  gSignalStatus = signal;
}
class SignalRouter{
  // Replace all signal handlers by  SignalHandler on instantiation, 
  //   reset to original handlers on destruction

public:
  SignalRouter(){
    // copy the old handlers to a map, and set the new handler.
    m_map[SIGFPE] = signal(SIGFPE, signalHandler);
    m_map[SIGABRT] = signal(SIGABRT, signalHandler);
    m_map[SIGILL] = signal(SIGILL, signalHandler);
    m_map[SIGINT] = signal(SIGINT, signalHandler);
    m_map[SIGSEGV] = signal(SIGSEGV, signalHandler);
    m_map[SIGTERM] = signal(SIGTERM, signalHandler);
  }
  
  ~SignalRouter(){
    // reset the the signal handlers to their original values.
    signal(SIGFPE, m_map[SIGFPE]);
    signal(SIGABRT, m_map[SIGABRT]);
    signal(SIGILL, m_map[SIGILL]);
    signal(SIGINT, m_map[SIGINT]);
    signal(SIGSEGV, m_map[SIGSEGV]);
    signal(SIGTERM, m_map[SIGTERM]);
  }
  
private:
  std::map<int, void(*) (int)> m_map;  //int to function pointer lookup
};

*/

TRP_RootFileReader::TRP_RootFileReader(TRP_TPDataPtr& tpdata,
                                       const QString& branchName,
                                       TFile *rootFile, 
                                       QObject *parent):
  QObject(parent),
  m_tpdata(tpdata),
  m_branchName(branchName.toStdString()),
  m_file(rootFile){
  
  connect (this,
           SIGNAL(gotData()),
           m_tpdata,
           SIGNAL(gotData()));
  
  connect (this,
           SIGNAL(gotData()),
           this,
           SLOT(lockHistoryPoint()));
  
  // unset all the ROOT signal handlers
  for (int sig = 0; sig < kMAXSIGNALS; sig++){
    auto es = static_cast<ESignals>(sig); // cast int to ESignals (enum)
    gSystem->ResetSignal(es);  // set default handler for signal sig
  }
  
}


TRP_RootFileReader::~TRP_RootFileReader() {
  // the reader does not own its pointers - so nothing to clean up.
}

void TRP_RootFileReader::run() {

  // If re-subscribing, make sure points are not duplicated.
  std::vector <TimePoint_IS> tps;

  // SignalRouter sr;  // convert signals to exceptions

  std::string errmsg{"TRP_RootFileReader " + m_branchName + " "};

  m_mutex.lock();
  try {
    /* signal handling test
       int a = 1/0;
       int b = a + 1;
       std::cout<< b << '\n'; */
    
    /* signal handling test
       std::raise(SIGINT); signal test */
    
    readRootFile(tps);
  } catch(std::exception& e){
    std::cout << errmsg << e.what() << '\n';
    m_mutex.unlock();
    return;
  } catch(...) {
    std::cout << errmsg << "Unknown exception \n";
    m_mutex.unlock();
    return;
  }
  
  /*
  if (::gSignalStatus != 0){
    std::cout << "TRP_RootFileReader " << m_branchName <<" received signal "
              << ::gSignalStatus << " while reading ROOT file, giving up.\n";
    ::gSignalStatus = 0;
    m_mutex.unlock();
    return;
  }
  */

  m_mutex.unlock();

  m_mutex.lock();
  auto utps = trp_unique_timepoints(tps);
  m_tpdata->setTimePoints(utps);
  m_mutex.unlock();
  emit gotData();
  
  //debug
  /*
  std::cout<< m_tpdata->getSummary() << '\n';
  if (m_doSummary){
    std::cout<< m_tpdata << '\n';
  }  
  */
}


void TRP_RootFileReader::updateHistoryDiff(int state) {
  m_tpdata->updateHistoryDiff(state);
}


void TRP_RootFileReader::lockHistoryPoint() {m_tpdata->lockHistoryPoint();}


void TRP_RootFileReader::readRootFile(std::vector <TimePoint_IS> &tp_vec) {
  TTree *tree = (TTree*)m_file->Get(m_branchName.c_str());

  auto n_history = tree->GetEntries();
  tp_vec.resize(n_history);
  
  //Remove -1 in TimePoint_IS.Data
  for (unsigned int i=0; i<tp_vec.size(); ++i) {
    tp_vec.at(i).Data.clear();
  }
  
  std::set<std::string> XLabel_set, YLabel_set;
  std::string XLabel, YLabel;
  std::vector<std::string> XLabels, YLabels;
  std::string branchname;
  
  TObjArray *objarray = (TObjArray*)tree->GetListOfBranches();
  TIter iter(objarray);
  
  int nbranches = objarray->GetEntries();
  
  // set up a progress bar
  std::stringstream ss;
  ss<< "Processing "<< m_branchName;
  TRP_ProgressDialogWidget*  progress = new TRP_ProgressDialogWidget(nbranches, 
                                                                     ss.str().c_str(), 
                                                                     this);
  // ensure the thread can live until it quits
  m_dialogProgressWidgets.push_back(progress);
  progress->start();

  connect(this, SIGNAL(updateProgressDialog()), progress, SLOT(update()));
  connect(this, SIGNAL(bumpProgressDialog()), progress, SLOT(bump()));

  emit updateProgressDialog();
  // progress bar is set up

  TBranch *branch{nullptr};
  TLeaf *leaf{nullptr};

  // Count occurences of YLabels, to warn if there may be problems related to
  // the architecture of the data structure in TRP/TimePoint_IS
  std::map<std::string, unsigned> YLabelCounter;

  // List of branch name regex patterns to ignore.
  std::vector<std::string> ignore_patterns = {"SMK", "L1PSK", "HLTPSK", "BGK",
					      "Atlas.*", "AthenaP1.*"};

  // Loop over all branches in .root file
  while ((branch = (TBranch*)iter.Next())) {
    emit bumpProgressDialog();  // bump the progress bar
    if (branch->GetNleaves() != 1) { continue; }
	
    branchname = branch->GetName();

    // @TEMP: Manually change branch names to avoid conflict with TimePointIS storage method
    std::string origbranchname = branchname;
    if (branchname == "ATLAS_PREFERRED_Inst_Mu") {
      branchname = "ATLAS_PREFERRED_Inst_Mu_Val";
    } else if (branchname == "ATLAS_PREFERRED_Inst_Mu_err") {
      branchname = "ATLAS_PREFERRED_Inst_Mu_Err";
    }

    // Check whether to ignore branch.
    bool ignore = false;
    for (const auto& pattern : ignore_patterns) {
      ignore |= std::regex_match(branchname, std::regex(pattern));
      if (ignore) { break; }
    }
    if (ignore) { continue; }

    // Store labels only for items going into the 'Data' container.
    if (!(branchname == "TimeStamp" || 
          branchname == "LumiBlock" || 
          branchname == "Run")) {
	  
      size_t position = branchname.rfind("_");
      XLabel = branchname.substr(0,position);
      YLabel = branchname.substr(position+1);
      
      if (!XLabel_set.count(XLabel)) {
	XLabels.push_back(XLabel);
	XLabel_set.insert(XLabel);
      }
      
      if (!YLabel_set.count(YLabel)) {
	YLabels.push_back(YLabel);
	YLabel_set.insert(YLabel);
      }
    }

    // Count the instances of column 'YLabel'. Using the fact that
    // std::map::operator[] inserts default value (0) when accessing a key not
    // present in map.
    YLabelCounter[YLabel]++;
  
    // Get value of each branch for every history point
    leaf = (TLeaf*)branch->GetLeaf(origbranchname.c_str()); // @TEMP

    // Loop time points.
    for (unsigned int ihist = 0; ihist < n_history; ihist++) {
      branch->GetEvent(n_history-1-ihist);
      
      if (branchname == "TimeStamp") {
        tp_vec[ihist].TimeStamp = leaf->GetValue();
      } else if (branchname == "LumiBlock") {
        tp_vec[ihist].LumiBlock = leaf->GetValue();
      } else if (branchname == "Run") {
        tp_vec[ihist].RunNumber = leaf->GetValue();
      } else {
        tp_vec[ihist].Data.push_back(leaf->GetValue());
      }
    }
  }


  progress->quit();
  // std::cout << "TRP_RootFileReader looped over "<< nbranch << " branches\n";
  
  // Insert labels for all history points
  for (unsigned int ihist = 0; ihist < n_history; ihist++) {
    tp_vec[ihist].XLabels = XLabels;
    tp_vec[ihist].YLabels = YLabels;
  }

  // Check to make sure that the same number of items have been added for each
  // YLabel (i.e. column). If not, issue warning to user.
  std::set<unsigned> ItemCountSet;
  for (const auto& key : YLabels) {
    ItemCountSet.insert(YLabelCounter.at(key));
  }

  if (ItemCountSet.size() > 1) {
    // @TODO: Make "logger"-type class, with warning message capabilities, from
    //        which the different trp_gui classes can inherit? Otherwise, make
    //        common utility function?
    std::string text = "Inconsistent item count encountered.";
    std::string informativeText = "This may (will) lead to look-up errors for the TRP/TimePoint_IS class, and plots may (will) not display the correct values.";

    std::string detailedText = "<TRP_RootFileReader::readRootFile>\n";
    detailedText += "The number of items for each column do not match:\n";
    for (const auto& key : YLabels) {
      detailedText += "  '" + key + "': " + std::to_string(YLabelCounter.at(key)) + "\n";
    }

    QMessageBox warningBox;
    warningBox.setIcon(QMessageBox::Warning);
    warningBox.setWindowTitle("Warning");
    warningBox.setText           (QString::fromStdString(text));
    warningBox.setInformativeText(QString::fromStdString(informativeText));
    warningBox.setDetailedText   (QString::fromStdString(detailedText));
    warningBox.exec();
  }

}

TRP_TPDataPtr TRP_RootFileReader::getTimePointData() const noexcept{
  return m_tpdata;
}
