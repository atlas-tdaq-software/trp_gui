#include <QtGui>
#include <QtWidgets>
#include "trp_help.h"

TRP_Help::TRP_Help(QWidget *parent) : QWidget(parent) {
  setWindowFlags(Qt::Window);
  setWindowTitle("TRP GUI Help");

  browser = new QTextBrowser;
  
  QPushButton *close_button    = new QPushButton("&Close");
  QPushButton *home_button     = new QPushButton("&Home");
  QPushButton *back_button     = new QPushButton("<");
  QPushButton *forward_button  = new QPushButton(">");

  connect(close_button,   SIGNAL(clicked()), this, SLOT(close()));
  connect(home_button,    SIGNAL(clicked()), browser, SLOT(home()));
  connect(back_button,    SIGNAL(clicked()), browser, SLOT(backward()));
  connect(forward_button, SIGNAL(clicked()), browser, SLOT(forward()));

  QShortcut *back_shortcut    = new QShortcut(QKeySequence::Back, this);
  QShortcut *forward_shortcut = new QShortcut(QKeySequence::Forward, this);

  connect(back_shortcut,    SIGNAL(activated()), browser, SLOT(backward()));
  connect(forward_shortcut, SIGNAL(activated()), browser, SLOT(forward()));

  QHBoxLayout *top_layout = new QHBoxLayout;
  top_layout->addWidget(home_button);
  top_layout->addWidget(back_button);
  top_layout->addWidget(forward_button);
  top_layout->addStretch();
  top_layout->addWidget(close_button);

  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->addLayout(top_layout);
  main_layout->addWidget(browser);
  setLayout(main_layout);

  browser->setSource(QUrl::fromLocalFile(":/trp_help.html"));

}

void TRP_Help::home() {
  browser->home();
}
