#include "is_server.h"
#include <is/is.hh>

bool
is::server::exist( const IPCPartition & partition, const std::string & name )
{
    try {
    	return partition.isObjectValid<is::repository>( name );
    }
    catch ( ... ) { }
    
    return false;
}

/*
void 
is::server::clone( const IPCPartition & partition, const std::string name, const std::string & clone_name, unsigned int timeout )
		throw ( daq::is::ServerNotFound, daq::is::ServerAlreadyExist, daq::is::InvalidServerName )
{
    if ( exist( partition, clone_name ) )
    {
    	throw daq::is::ServerAlreadyExist( ERS_HERE, clone_name, partition.name() );
    }
    
    try {
    	is::repository_var rep = partition.lookup<is::repository>( name );
        
        is::replica_var process = is::replica::_narrow( rep );
        
        process -> replicate( clone_name.c_str() );
    }
    catch( daq::ipc::Exception & ex ) {
    	throw daq::is::ServerNotFound( ERS_HERE, name, partition.name(), ex );
    }
    catch( is::InvalidName & ex ) {
    	throw daq::is::InvalidServerName( ERS_HERE, name, partition.name() );
    }
    catch( CORBA::SystemException & ex ) {
    	throw daq::is::ServerNotFound( ERS_HERE, name, partition.name(), daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    
    unsigned int max_iterations = timeout * 10; // number of 100 ms slices
    unsigned int it = 0;
    try {
	while (      !partition.isObjectValid<is::repository>( clone_name ) 	// server is not yet up
        	&& ( !timeout || ++it < max_iterations ) )			// timeout is not set or is not yet expired
	{
	    usleep( 100000 );
	}
    }
    catch( daq::ipc::Exception & ex ) {
	throw daq::is::ServerNotFound( ERS_HERE, name, partition.name(), ex );
    }
}
*/
