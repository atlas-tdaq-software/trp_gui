/** @file trp_mainwindow.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_MainWindow: main window for TRP GUI
 */

#ifndef TRP_MAINWINDOW_H
#define TRP_MAINWINDOW_H

#include <set>
#include <map>
#include <QMainWindow>
#include <QPointer>
#include<QtGui>
#include <QtWidgets>
#include "trp_config.h"
#include "trp_help.h"
#include "trp_ipcthread.h"
#include "trp_isthread.h"
#include "trp_typeAliases.h"
#include "trp_plot.h"
#include "trp_plotpage.h"
#include "trp_tablepage.h"
#include "trp_tabwidget.h"
#include "trp_trend.h"
#include "trp_progressdialogwidget.h"
#include <memory>
//#include "TriggerMenuHelpers.h"

class TFile;
class ISInfoReceiver;

struct  tabDataStruct {
  TRP_TablePage* table_page;
  QSignalMapper* thread_mapper;
  TRP_Config* config;
  TRP_TPDataContainer threads;  
};

class TRP_CheckBox : public QCheckBox {
  Q_OBJECT
public:
  TRP_CheckBox(const QString & text, QWidget * parent = 0)
  : QCheckBox(text, parent){CheckAll = true;};
  ~TRP_CheckBox(){};

private:
  bool CheckAll;

public slots:
  void changeCheckedState() {
    if (CheckAll) {
	  QCheckBox::setCheckState(Qt::Checked);
	  CheckAll = false;
	}
    else if (!CheckAll) {
      QCheckBox::setCheckState(Qt::Unchecked);
      CheckAll = true;
    }
  }
};

class QTabWidget;
class QMessageBox;
class QSignalMapper;
class TRP_Plot;
class TRP_PlotPage;
class TRP_Help;

// history
class TRP_TablePage;

struct BranchNamesFromCheckBoxResult{
  std::vector<QString> thread_list;
  int nL1{0};
  int nL2{0};
  int nEF{0};
};

class TRP_MainWindow : public QMainWindow {
  Q_OBJECT

public:
  TRP_MainWindow(TRP_Config *c, 
                 int run_number = 0, 
                 QString root_file = QString::null,
                 bool debugOn = false,
                 QWidget *parent = 0);
  ~TRP_MainWindow();

  TRP_Plot::plotType selectedPlotType;  

  TRP_MainWindow(const TRP_MainWindow &) = delete;
  TRP_MainWindow& operator = (const TRP_MainWindow &) = delete;


  
private slots:
  void isHistoryTabClicked(int);
//  void emitUpdateHistoryPointDialog(unsigned int);

  void emitUpdateHistoryStepDialog(int);
  void setPlotDiffStep(int step);
  void setPlotDiffStep1() {setPlotDiffStep(-2);}
  void setPlotDiffStep2() {setPlotDiffStep(-1);}
  void setPlotDiffStep3() {setPlotDiffStep(0);}
  void setPlotDiffStep4() {setPlotDiffStep(1);}
  void setPlotDiffStep5() {setPlotDiffStep(2);}
  void updatePlotDiffStepInfo();


private:
  TRP_Config *m_config;
  TRP_TabWidget* m_tab_widget;
  std::deque<TRP_PlotPage *> m_plot_pages;
  TRP_TPDataContainer m_timePointDataCollection;
  std::deque<TRP_ISThread*> m_threads;
  std::map<int, std::map<unsigned int, std::set<QPointer<TRP_Plot>>>> m_plots;
  QString m_tableID_counter;
  std::map<int, tabDataStruct> m_tabDataMap;
  unsigned int m_plot_text_size;
  int m_ISThread_tableID;
  std::set<QString> m_tabs_without_history;
  QAction *m_setPlotTextSize1Action, *m_setPlotTextSize2Action, 
          *m_setPlotTextSize3Action, *m_setPlotTextSize4Action, 
          *m_setPlotTextSize5Action, *m_setPlotTextSize6Action;
  IPCPartition *m_trp_partition;
  std::string m_partitionName;
  ISInfoReceiver *m_trp_receiver; 
  //confadapter_imp::TriggerMenuHelpers *trigger_menu;
  QSignalMapper* m_thread_mapper;
  TRP_Help* m_help_browser;
  TRP_IPCThread *m_ipc_thread;
  const QString m_gui_version;
  QString m_history_tab_text;
  // unsigned int m_history_size; //For reading root file
  TRP_TablePage* m_history_table_page;
  int  m_plot_diff_step;
  QAction *m_setPlotDiffStep1Action, *m_setPlotDiffStep2Action, 
          *m_setPlotDiffStep3Action, *m_setPlotDiffStep4Action, 
          *m_setPlotDiffStep5Action;
  bool m_exit_deletePlots;
  bool m_debugOn;
  // a place for threads to exists until they complete quiting
  std::vector<TRP_ProgressDialogWidget*> m_dialogProgressWidgets;

  void runFromIS();

  // --- methods used for handling ROOT files (See also private slots)
  void runFromRootFile(int run_number, const QString& root_file);
  TFile* openFileMdaDialog(int runnumber);
  TFile* openFileLocallyDialog(QString filepath);
  void processRootFile(TFile*);
  TRP_TablePage* makeRootFileTablePage(TFile*,
                                       const TRP_Config*,
                                       QSignalMapper*,
                                       TRP_TPDataContainer&);
  void runRootFileReaders(const TRP_Config*, 
                          TRP_TPDataContainer&,
                          TFile*,
                           QSignalMapper*);

  TRP_TPData* runOneRootFileReader(unsigned int, 
                                   int, 
                                   const TRP_Config::Thread&,
                                   TFile*,
                                    QSignalMapper*) ;

  // --- end methods used for handling ROOT files (See also private slots)
  
  void createPlotPages();
  void createRootPlotsPage(QString, BranchNamesFromCheckBoxResult, int);

  void createActionsAndMenus();
  BranchNamesFromCheckBoxResult branchNamesFromCheckBox(TFile* pfile);

  void testfunction();

  QString getUnitNameFromConfigFile (const TRP_Config::Thread&) const;
  QString getAliasNameFromConfigFile (const TRP_Config::Thread&) const;
  TRP_TabWidget* getTabWidget()  { return m_tab_widget; }
  
private slots:
  void grabScreenShot();

  // slots for handling roort files start
  void getRunNumberFromUserAndProcess();
  void getRootFilePathFromUserAndProcess();
  // slots for handling roort files end
  
  void setPlotTextSize(unsigned int size);
  void setPlotTextSize1() {setPlotTextSize(1);}
  void setPlotTextSize2() {setPlotTextSize(2);}
  void setPlotTextSize3() {setPlotTextSize(3);}
  void setPlotTextSize4() {setPlotTextSize(4);}
  void setPlotTextSize5() {setPlotTextSize(5);}
  void setPlotTextSize6() {setPlotTextSize(6);}
  void updatePlotTextSizeInfo();

  void warning(QString warning_text, QString message);
  void configWarning(QString message);
  void databaseWarning(QString message);

  void serverOn();
  void serverOff();
  void threadOn(unsigned int);
  void threadOff(unsigned int);

  void about();
  void helpContents();

  TRP_Plot* makePlot(std::deque<TRP_Config::Trend*> trend_list, 
                      int tableID=-1, QWidget *parent=0);

  TRP_Plot* makeCorrelationPlot(std::deque<TRP_Config::Trend*> trend_list, 
                      int tableID=-1, QWidget *parent=0);
  void clonePlot();

  void updatePlots(int i_th = -1, int ID = -1);
  void updatePlot(TRP_Plot *plot);
  void updatePlotPointers();
  
  void deletePlots(int ID = -1);

  void deleteTabData(int tab_index);

signals:
  void updateHistoryPointDialog(unsigned int);
  void updateHistoryStepDialog(int);
  void updateProgressDialog();
  void bumpProgressDialog();

};

#endif  // define TRP_MAINWINDOW_H
