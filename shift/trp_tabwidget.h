/** @file trp_tabwidget.h
 *  @author Martin Spangenberg (Martin.Spangenberg@cern.ch)
 *  @brief TRP_TabWidget: Extension of QTabWidget making tabs closable and tabBar() accessible
 */

#ifndef TRP_TABWIDGET_H
#define TRP_TABWIDGET_H

#include <QTabWidget>

class TRP_TabWidget: public QTabWidget {
  Q_OBJECT
  
public:

  TRP_TabWidget(QWidget* parent);
  ~TRP_TabWidget(){};

  QTabBar* tabBar();
  
public slots:

  void closeTab(int index);

signals:
  void deleteTabData(int);

};

#endif