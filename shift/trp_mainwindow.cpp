#include <iostream>
#include <stdlib.h>

#include <owl/time.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <TFile.h>
#include <is/inforeceiver.h>

#include "trp_mainwindow.h"
#include "trp_typeAliases.h"
#include "trp_rootfilereader.h"
#include <QProgressDialog>
#include <sstream>
#include <memory>

#include "mda/read_back/FileRead.h"
#include <chrono>
#include <thread>
#include <sstream>
#include <algorithm>
#include <utility>
#include <cassert>
TRP_MainWindow::TRP_MainWindow(TRP_Config *config, 
                               int run_number, 
                               QString root_file, 
                               bool debugOn, 
                               QWidget *parent):
  QMainWindow(parent), 
  m_config(config),
  m_gui_version(TRPVERSION), 
  m_exit_deletePlots(false), 
  m_debugOn(debugOn) {

  if (!m_config->warnings().isEmpty()) {configWarning(m_config->warnings());}

  setWindowTitle("Trigger Rate Presenter");

  runFromIS();

  if (run_number > 0 or root_file != QString::null){
    runFromRootFile(run_number, root_file);
  }
  
  // tmp workaround to fix L1 table showing 0s
  m_tab_widget->setCurrentIndex(1);
  sleep(1);
  m_tab_widget->setCurrentIndex(0);
}

TRP_MainWindow::~TRP_MainWindow() {
  m_ipc_thread->quit();
  for (unsigned int i = 0; i < m_threads.size(); i++) {
    m_threads[i]->quit();
  }
  serverOff();
  m_ipc_thread->wait();
  delete m_trp_partition;
  ///delete trigger_menu;
}

void TRP_MainWindow::runFromIS(){
  // start ****
  m_ISThread_tableID = -1;
  m_tableID_counter.setNum(0);

  m_tab_widget = new TRP_TabWidget(this);
  setCentralWidget(m_tab_widget);
  m_tab_widget->setTabsClosable(true);

  // For deleting .root tab data
  connect(m_tab_widget, SIGNAL(deleteTabData(int)),
          this, SLOT(deletePlots(int)));
  
  connect(m_tab_widget, SIGNAL(deleteTabData(int)),
          this, SLOT(deleteTabData(int)));

  m_plot_text_size = 2;
  // history (initialisation value needed for createActionsAndMenus())
  m_plot_diff_step = 0;
  createActionsAndMenus();

  m_trp_partition = new IPCPartition(m_config->partitionName().toStdString());
  m_partitionName = m_config->partitionName().toStdString();
  m_trp_receiver = 0;

  m_thread_mapper = new QSignalMapper(this);
  
  // instantiate the threads that will listen to IS
  // !!! history_enabled must be set to false to get the
  // table page to set up correctly !!! ???
  bool history_enabled = false;
  for (unsigned int i = 0; i < m_config->threads.size(); i++) {
    TRP_Config::Thread c_th = (m_config->threads)[i];
    
    TRP_TPDataPtr tpdata =  new TRP_TPData(m_config->historySize(),
                                           c_th.name,
                                           c_th.alias,
                                           c_th.unit,
                                           history_enabled,
                                           m_debugOn,
                                           nullptr);

    TRP_ISThread *thread = new TRP_ISThread(c_th.name, c_th.alias, c_th.unit,
                                            m_config->serverName(),
                                            m_config->historySize(), 
                                            m_partitionName,
                                            tpdata,
                                            this);
    thread->setThreadNumber(i);
    m_thread_mapper->setMapping(thread, i);
    connect(thread,
            SIGNAL(gotData()),
            m_thread_mapper,
            SLOT(map()));

    m_timePointDataCollection.push_back(thread->getTimePointData());
    m_threads.push_back(thread);
  }

  connect(m_thread_mapper,
          SIGNAL(mapped(const int &)),
          this,
          SLOT(updatePlots(const int &)));

  m_ipc_thread = new TRP_IPCThread(m_trp_partition,
                                   m_config->serverName().toStdString(),
                                   m_threads,
                                   this);

  connect (m_ipc_thread, 
           SIGNAL(serverDown()),
           this, 
           SLOT(serverOff()));

  connect (m_ipc_thread,
           SIGNAL(serverUp()),
           this,
           SLOT(serverOn()));

  connect (m_ipc_thread,
           SIGNAL(threadDown(unsigned int)),
           this,
           SLOT(threadOff(unsigned int)));

  connect (m_ipc_thread,
           SIGNAL(threadUp(unsigned int)),
           this,
           SLOT(threadOn(unsigned int)));

  /* m_ipc_thread determines the state of IS, and signals
     this class to control the TRP_ISThreads 
     via the threadOn and threadOff methods.
   */
  m_ipc_thread->start();

  // std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  TRP_TablePage *table_page = new TRP_TablePage(m_timePointDataCollection,
                                                m_config,
                                                this);
  table_page->setTableID(m_ISThread_tableID);
  m_tabs_without_history.insert("Tables");

  connect(table_page,
          SIGNAL(plotRequest(std::deque<TRP_Config::Trend*>,int)),
          this,
          SLOT(makePlot(std::deque<TRP_Config::Trend*>,int)));
  
  connect(table_page,
          SIGNAL(plotCorrelationRequest(std::deque<TRP_Config::Trend*>,int)),
          this,
          SLOT(makeCorrelationPlot(std::deque<TRP_Config::Trend*>,int)));

  // history
  m_history_tab_text = "History Tables";
  if (!m_timePointDataCollection[0]->isHistoryEnabled()) {
    // sets flag and emits signal
    m_timePointDataCollection[0]->setHistoryEnabled(true);
  }

  m_history_table_page = new TRP_TablePage(m_timePointDataCollection, m_config, this);
  m_history_table_page->setTableID(m_ISThread_tableID);

  connect (m_history_table_page,
           SIGNAL(plotRequest(std::deque<TRP_Config::Trend*>,int)),
           this, 
           SLOT(makePlot(std::deque<TRP_Config::Trend*>,int)));

  connect (m_history_table_page,
           SIGNAL(plotCorrelationRequest(std::deque<TRP_Config::Trend*>,int)),
           this,
           SLOT(makeCorrelationPlot(std::deque<TRP_Config::Trend*>,int)));

  //Build default struct containing threads and config for the IS Server

  tabDataStruct tds;
  tds.threads.assign(m_timePointDataCollection.begin(),
                     m_timePointDataCollection.end());
  tds.config = m_config;
  tds.table_page = m_history_table_page;
  tds.thread_mapper = m_thread_mapper;

  m_tabDataMap.insert(std::pair<int,tabDataStruct>(m_ISThread_tableID, tds)); 
  
  createPlotPages();
  updatePlotTextSizeInfo();
  updatePlotDiffStepInfo();

  int tabindex_tablepage = m_tab_widget->addTab(table_page, "Tables");
  m_tab_widget->setTabWhatsThis(tabindex_tablepage, m_tableID_counter);
  m_tab_widget->tabBar()->tabButton(tabindex_tablepage,
                                  QTabBar::RightSide)->resize(0,0);

  m_tabDataMap.insert(std::pair<int,tabDataStruct>(m_tableID_counter.toInt(),
                                                 tds));
  
  m_tableID_counter.setNum(m_tableID_counter.toInt()+1);
  
  int tabindex_history = m_tab_widget->addTab(m_history_table_page,
                                              m_history_tab_text);

  m_tab_widget->setTabWhatsThis(tabindex_history, m_tableID_counter);
  m_tab_widget->tabBar()->tabButton(tabindex_history, 
                                  QTabBar::RightSide)->resize(0,0);

  m_tabDataMap.insert(std::pair<int,tabDataStruct>(m_tableID_counter.toInt(),
                                                 tds));

  m_tableID_counter.setNum(m_tableID_counter.toInt()+1);

  connect (m_tab_widget, 
           SIGNAL(currentChanged(int)),
           this, 
           SLOT(isHistoryTabClicked(int)));

  m_help_browser = new TRP_Help(this);
  
}

void TRP_MainWindow::getRunNumberFromUserAndProcess() {
  /* obtain a root file run number via a dialog box */

  auto runnum =  QInputDialog::getInt(this, tr("Open ROOT file via MDA"),
                                      tr("Please specify run number:"), 0, 0);

  if (!(runnum > 0)){
    std::stringstream ss;
    ss << runnum << " is not > 0";
    warning("Error opening root file", ss.str().c_str());
  }
  runFromRootFile(runnum, QString::null);
}

void TRP_MainWindow::getRootFilePathFromUserAndProcess() {
  /* obtain a root file path via a dialog box */

  auto filepath = QFileDialog::getOpenFileName(this, 
                                               tr("Open ROOT file locally"),
                                               QDir::current().absolutePath(), 
                                               tr(".root files (*.root)"));

  if (filepath == QString::null){
    std::stringstream ss;
    ss << filepath.toStdString() << " is null";
    warning("Error opening root file", ss.str().c_str());
  }
  runFromRootFile(0, filepath);
}


void TRP_MainWindow::runFromRootFile(int run_number, const QString& root_file){

  TFile* file = nullptr;
  if (run_number > 0) {
    // use an mda reader to open the root file using a run number only.
    file = openFileMdaDialog(run_number);
  } else if (root_file != QString::null) {
    // open a root file in the current directory
    file = openFileLocallyDialog(root_file);
  } else {
    warning("Open Root file error",
            "Not enough information given to open ROOT file");
    return;
  }

  if (file == nullptr){
    std::stringstream ss;
    
    if (run_number > 0){
      ss <<"Could not to open ROOT file for run " << run_number;
      warning("Open Root file error", ss.str().c_str());
      return;
    }
    
    if (root_file != QString::null){
      ss <<"Could not to open ROOT file " << root_file.toStdString();
      warning("Open Root file error", ss.str().c_str());
      return;
    }
  }
  
  processRootFile(file);
}

void TRP_MainWindow::warning(QString warning_text, QString message) {
  QMessageBox warning_message(QMessageBox::Warning, 
                              QString("Warning"), 
                              warning_text,
                              QMessageBox::Ok, 
                              this);
  warning_message.setInformativeText(message);
  warning_message.exec();
}

void TRP_MainWindow::configWarning(QString message) {
  warning("Configuration issue.", message);
}

void TRP_MainWindow::databaseWarning(QString message) {
  warning("Error reading trigger menu from database.", message);
}

void TRP_MainWindow::serverOn() {
  if (!m_trp_partition->isValid()) {
    m_ipc_thread->partitionDisappeared();
    return;
  }

  for (unsigned int i = 0; i < m_threads.size(); i++) {
    m_threads[i]->setDictionary(m_partitionName);
  }

  if (m_trp_receiver == 0) {
    m_trp_receiver = new ISInfoReceiver(*m_trp_partition);
    for (unsigned int i = 0; i < m_threads.size(); i++) {
      m_threads[i]->setReceiver(m_trp_receiver);
    }
  }
}

void TRP_MainWindow::serverOff() {
  for (unsigned int i = 0; i < m_threads.size(); i++) {
    m_threads[i]->wait();
  }

  if (m_trp_receiver != 0) {
    delete m_trp_receiver;
    m_trp_receiver = 0;
    for (unsigned int i = 0; i < m_threads.size(); i++) {
      m_threads[i]->setReceiver(m_trp_receiver);
    }
  }
}

void TRP_MainWindow::threadOn(unsigned int i) {
  m_threads[i]->start();
}

void TRP_MainWindow::threadOff(unsigned int i) {
  m_threads[i]->quit();
}

void TRP_MainWindow::createPlotPages() {

  for (unsigned int ipp = 0; ipp < m_config->plot_pages.size(); ipp++) {
    TRP_Config::PlotPage c_pp = (m_config->plot_pages)[ipp];
    TRP_PlotPage *plot_page = new TRP_PlotPage(this);

    for (unsigned int ip = 0; ip < c_pp.plots.size(); ip++) {
      TRP_Config::Plot c_p = c_pp.plots[ip];
	  
      std::deque<TRP_Config::Trend*> trend_list;
      for (unsigned int it = 0; it < c_p.trends.size(); it++) {
        TRP_Config::Trend c_t = c_p.trends[it];
        TRP_Config::Trend *trend = new TRP_Config::Trend(c_t);
        trend_list.push_back(trend);
      }

      // TRP_Plot *plot = makePlot(trend_list, -1, plot_page);
      TRP_Plot *plot;
      QString correlations = "Correlations";
      if (c_pp.title == correlations){
        plot = makeCorrelationPlot(trend_list, -1, plot_page);
      }
      else{
        plot = makePlot(trend_list, -1, plot_page);
      }
      plot->setTitle(c_p.title);
      plot_page->addPlot(plot);
    }
	
    plot_page->formatPage();
    m_plot_pages.push_back(plot_page);
    int tabindex = m_tab_widget->addTab(plot_page, c_pp.title);

    m_tab_widget->tabBar()->tabButton(tabindex, QTabBar::RightSide)->resize(0,0);
	m_tabs_without_history.insert(c_pp.title);
    
  }
}

void TRP_MainWindow::createRootPlotsPage  (QString filename, BranchNamesFromCheckBoxResult result, int tableID) {
  /**
   * Create a ROOT plot page, mimicking the first plot page for live data,
   * "Overall rates". 
   *
   * @NOTE: This method does essentially the same thing as the inner loop in
   * TRP_MainWindow::createPlotPages, so we could probably avoid some code
   * duplication there.
   */
  
  // Define variable(s)
  const bool debug = false;

  // Get configuration for first plot page, "Overall rates".
  TRP_Config::PlotPage c_pp = (m_config->plot_pages)[0];
  
  // Create new plot page for historic run; add tab to main widget.
  TRP_PlotPage *rootFile_plot_page = new TRP_PlotPage(this);
  int tab_index = m_tab_widget->addTab(rootFile_plot_page,"Run "+filename+" Plots");
  m_tab_widget->setTabWhatsThis(tab_index, m_tableID_counter);

  // Loop all plots specified in chosen config.
  for (unsigned ip = 0; ip < c_pp.plots.size(); ip++) {
    TRP_Config::Plot c_p = c_pp.plots[ip];
    
    // Get all trends for current plot
    std::deque<TRP_Config::Trend*> trend_list;
    for (unsigned it = 0; it < c_p.trends.size(); it++) {
      TRP_Config::Trend c_t = c_p.trends[it];
      TRP_Config::Trend *trend = new TRP_Config::Trend(c_t);
      
      // Map live-date theads onto ROOT threads
      // --------------------------------------
      // The thread storing the data related to e.g. HLT in live data need not
      // be the same as in ROOT files. In order to use the same plot page
      // config, we need to update the thread index to match ordering for the
      // ROOT file.
      //
      // Alternative:
      //   assert m_tabDataMap.size() > 0
      //   assert m_tabDataMap[0].threads.size() > trend->n_thread
      const unsigned idx_thread_live = trend->n_thread;
      if (m_tabDataMap.size() == 0) {
	if (debug) {
	  std::cout << "[WARN]  m_tabDataMap has 0 entries." << std::endl;
	}
	continue;
      }
      if (m_tabDataMap[0].threads.size() <= idx_thread_live) {
	if (debug) {
	  std::cout << "[WARN]  m_tabDataMap[0].threads has " << m_tabDataMap[0].threads.size() << "; trying to access index " << idx_thread_live << "." << std::endl;
	}
	continue;
      }
      
      // Get name of thread with requested live-data index.
      QString thread_name = m_tabDataMap[0].threads[idx_thread_live]->getName();
      
      // Check if a similarly named thread has been loaded from ROOT file.
      int idx_thread_root = -1;
      for (unsigned idx = 0; idx < m_tabDataMap[tableID].threads.size(); idx++) {
	if (thread_name == m_tabDataMap[tableID].threads[idx]->getName()) {
	  idx_thread_root = idx;
	  break;
	}
      }
      
      if (idx_thread_root == -1) {
	if (debug) {
	  std::cout << "[WARN]  No ROOT thread named " << thread_name.toStdString() << " was found." << std::endl;
	}
	continue;
      }
      
      // Assign ROOT file thread index.
      trend->n_thread = idx_thread_root;
      
      // Check for label matches
      // -----------------------
      // In cases where the trend is labelled as e.g. "str_Main_physics" in live
      // data and as "HLT_str_Main_physics" in ROOT, we want to rename the
      // trend's `x_name` field, in order to use the same plot page config as
      // for live data.
      const QString qlabel_live = trend->x_name;
      
      // List of pairs of partial matches and similarity measures.
      std::vector< std::pair<QString, int> > qlabels_partial;  
      
      QString qlabel;
      bool exact = false;
      for (const auto& label : m_tabDataMap[tableID].threads[idx_thread_root]->latest()->XLabels) {
	
	// Assign label variable.
	qlabel = QString::fromStdString(label);
	
	// Exact match.
	if (qlabel == qlabel_live) {
	  exact = true;
	  break;
	}
	
	// Partial match (bi-directional check).
	if (qlabel.contains(qlabel_live) || qlabel_live.contains(qlabel)) {
	  qlabels_partial.push_back(std::make_pair(qlabel, abs(qlabel_live.size() - qlabel.size())));
	}
      }
      
      // Update label if necessary.
      if (!exact) {
	if (qlabels_partial.size() == 0) {
	  if (debug) {
	    std::cout << "[WARN]  No matches found for '" << qlabel.toStdString() << "'." << std::endl;
	  }
	  continue;
	}
	
	// Select closest partial match (smallest similarity measure).
	auto iter_min = std::min_element(qlabels_partial.begin(), qlabels_partial.end(), [] (const auto& a, const auto& b) { return a.second < b.second; });
	
	if (debug) {
	  if (qlabels_partial.size() > 1) {
	    std::cout << "[WARN]  Found " << qlabels_partial.size() << " partial matches for '" << qlabel_live.toStdString() << "':" << std::endl;
	    for (const auto& qlabel_partial: qlabels_partial) {
	      std::cout << "[WARN]  - " << qlabel_partial.first.toStdString() << " (difference: " << qlabel_partial.second << ")"<< std::endl;
	    }
	  }
	  std::cout << "[WARN]  Using " << (*iter_min).first.toStdString() << std::endl;
	}
	
	// Assign updated name.
	trend->x_name = (*iter_min).first;
      }

      // Append to list of trends to be plotted.
      trend_list.push_back(trend);
    }
    
    // Create plot from specified trends.
    TRP_Plot *plot;
    QString correlations = "Correlations";
    if (c_pp.title == correlations){
      plot = makeCorrelationPlot(trend_list, tableID, rootFile_plot_page);
    }
    else{
      plot = makePlot(trend_list, tableID, rootFile_plot_page);
    }
    
    // Set ROOT to be data source.
    plot->setDataSource(1);
    
    // Set title; add plot to plot page.
    plot->setTitle(c_p.title);
    rootFile_plot_page->addPlot(plot);
  }
  
  // Format plot page.
  rootFile_plot_page->formatPage();

  return;
}



void TRP_MainWindow::clonePlot() {
  QPointer<TRP_Plot> original_plot = (TRP_Plot*)sender();
  if (!original_plot) return;

  std::deque<TRP_Config::Trend*> trend_list;
  std::deque<TRP_Trend*>::const_iterator i_t;
  for (i_t = original_plot->begin(); i_t != original_plot->end(); ++i_t) {
    TRP_Config::Trend *trend = new TRP_Config::Trend();
    trend->x_name = (*i_t)->getNameX();
    trend->y_name = (*i_t)->getNameY();
    unsigned int n = 0;
    TRP_TPDataPtr t = (*i_t)->getThread();
    for (unsigned int i = 0; i < m_timePointDataCollection.size(); i++) {
      //      if (t == threads[i]) {
      if (t->getName() == m_timePointDataCollection[i]->getName()) {
        n = i;
        break;
      }
    }
    trend->n_thread = n;
    trend_list.push_back(trend);
  }
  TRP_Plot *new_plot = makePlot(trend_list);
  new_plot->setTitle(original_plot->getTitle());
}


TRP_Plot* TRP_MainWindow::makePlot(std::deque<TRP_Config::Trend*> trend_list,
				   int tableID, QWidget *parent) {
                               
  selectedPlotType=TRP_Plot::standard;
  TRP_Plot *plot = new TRP_Plot(parent, selectedPlotType);
  
   if (m_plot_text_size != 1) plot->setTextSize(m_plot_text_size);
   connect(plot,
           SIGNAL(textSizeChanged()),
           this,
           SLOT(updatePlotTextSizeInfo()));

   connect(plot,
           SIGNAL(destroyed()),
           this,
           SLOT(updatePlotPointers()));

   connect (plot,
            SIGNAL(cloneRequest()),
            this,
            SLOT(clonePlot()));

   // history
   if (m_plot_diff_step != 1) plot->setDiffStep(m_plot_diff_step);

   connect(plot,
           SIGNAL(diffStepChanged()),
           this,
           SLOT(updatePlotDiffStepInfo()));

   connect(plot,
           SIGNAL(pointDoubleClicked(unsigned int)),
           m_tabDataMap[tableID].table_page, 
           SLOT(updateHistoryPointDialog(unsigned int)));
		 
//   connect(plot, SIGNAL(pointDoubleClicked(unsigned int)),
//           this, SLOT(emitUpdateHistoryPointDialog(unsigned int)));

   QPointer<TRP_Plot> pointer_to_plot = plot;
   
   for (unsigned int i = 0; i < trend_list.size(); i++) {
     auto plot_thread = m_tabDataMap[tableID].threads.at(trend_list[i]->n_thread);
     
	 TRP_Trend *trend = new TRP_Trend(trend_list[i]->x_name, 
                                      trend_list[i]->y_name,
                                      plot_thread);
      plot->addTrend(trend);
      m_plots[tableID][trend_list[i]->n_thread].insert(pointer_to_plot);
   }
   updatePlot(plot);

   if (plot->parentWidget() == 0) {
      plot->setParent(this);
      plot->setWindowFlags(Qt::Window);
      plot->setGeometry(x(), y(), plot->getMinWidth(), plot->getMinHeight());
      plot->show();
   }

   std::deque<TRP_Config::Trend*>::iterator it;
   for (it = trend_list.begin(); it != trend_list.end(); ++it) {
      delete *it;
   }
   trend_list.clear();
   
   return plot;
}

TRP_Plot* TRP_MainWindow::makeCorrelationPlot(std::deque<TRP_Config::Trend*> trend_list,
					      int tableID, QWidget *parent) {

   selectedPlotType=TRP_Plot::correlation;
   TRP_Plot *plot = new TRP_Plot(parent, selectedPlotType);

   if (m_plot_text_size != 1) plot->setTextSize(m_plot_text_size);
   connect(plot,
           SIGNAL(textSizeChanged()),
           this,
           SLOT(updatePlotTextSizeInfo()));

   connect(plot,
           SIGNAL(destroyed()),
           this,
           SLOT(updatePlotPointers()));

   connect(plot, 
           SIGNAL(cloneRequest()),
           this, 
           SLOT(clonePlot()));

   // history
   if (m_plot_diff_step != 1) plot->setDiffStep(m_plot_diff_step);
   connect(plot, 
           SIGNAL(diffStepChanged()),
           this, 
           SLOT(updatePlotDiffStepInfo()));

   connect(plot, 
           SIGNAL(pointDoubleClicked(unsigned int)),
           m_tabDataMap[tableID].table_page, 
           SLOT(updateHistoryPointDialog(unsigned int)));
		 
//   connect(plot, SIGNAL(pointDoubleClicked(unsigned int)),
//           this, SLOT(emitUpdateHistoryPointDialog(unsigned int)));

   QPointer<TRP_Plot> pointer_to_plot = plot;
   
   for (unsigned int i = 0; i < trend_list.size(); i++) {
     auto plot_thread = m_tabDataMap[tableID].threads.at(trend_list[i]->n_thread);
     
     TRP_Trend *trend = new TRP_Trend(trend_list[i]->x_name, 
                                      trend_list[i]->y_name,
                                      plot_thread);

      plot->addTrend(trend);
      m_plots[tableID][trend_list[i]->n_thread].insert(pointer_to_plot);
   }
   updatePlot(plot);

   if (plot->parentWidget() == 0) {
      plot->setParent(this);
      plot->setWindowFlags(Qt::Window);
      plot->setGeometry(x(), y(), plot->getMinWidth(), plot->getMinHeight());
      plot->show();
   }

   std::deque<TRP_Config::Trend*>::iterator it;
   for (it = trend_list.begin(); it != trend_list.end(); ++it) {
      delete *it;
   }
   trend_list.clear();
   
   return plot;
}

void TRP_MainWindow::updatePlots(int i_th, int ID) {
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  if (i_th == -1) {
    std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
    std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
	for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
      for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
        for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
          if (*i_p) updatePlot(*i_p);
        }
      }
	}
  } else {
    for (i_p = m_plots[ID][i_th].begin(); i_p != m_plots[ID][i_th].end(); ++i_p) {
      if (*i_p) updatePlot(*i_p);
    }
  }
}

void TRP_MainWindow::updatePlot(TRP_Plot *plot) {
  std::deque<TRP_Trend*>::const_iterator i_t;
  for (i_t = plot->begin(); i_t != plot->end(); ++i_t) {
    (*i_t)->updatePoints();
  }
  plot->update();
}

void TRP_MainWindow::updatePlotPointers() {
  std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
    for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
      for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
        if (*i_p == 0) {
          i_m->second.erase(i_p);
        }
      }
    }
  }
}

void TRP_MainWindow::deletePlots(int ID) {

 updatePlotPointers();
 std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
 std::set< QPointer<TRP_Plot> >::iterator i_p;
 int c1=0;
 for (i_m = m_plots[ID].begin(); i_m != m_plots[ID].end(); ++i_m) {
   for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
     c1++;
     if ( i_p->data() && c1==1) {
       if (i_p->data()->getDataSource() == 1) {
         m_exit_deletePlots=true;
         c1=0;
         continue;
       }
       i_p->data()->~TRP_Plot();
       deletePlots(ID);
     }
     if (i_m->second.size() == 0 || m_exit_deletePlots == true ) return;
   }
 }
 //m_plots[ID].clear();
 return;
/*
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  for (i_m = m_plots[ID].begin(); i_m != m_plots[ID].end(); ++i_m) {
    std::set< QPointer<TRP_Plot> >::iterator i_p;
    for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
      if (i_p->data()->getDataSource() == 0) {
        i_p->data()->~TRP_Plot();
      }
    }
  }
*/
}

void TRP_MainWindow::createActionsAndMenus() {
  /*** Create Actions ***/

  // Screen-shot

  QAction *screenShotAction = new QAction("Grab Screenshot", this);
  screenShotAction->setShortcut(QKeySequence("CTRL+G"));

  connect(screenShotAction, 
          SIGNAL(triggered()),
          this, 
          SLOT(grabScreenShot()));
  // Quit

  QAction *quitAction = new QAction("Quit", this);
  quitAction->setShortcut(QKeySequence("CTRL+Q"));
  connect(quitAction,
          SIGNAL(triggered()),
          qApp,
          SLOT(quit()));

  // Open data from .root file
		  
  QAction *rootFileByRunNumberAction = new QAction("Load TRP rates via run number...", this);
  rootFileByRunNumberAction->setShortcut(QKeySequence("CTRL+R"));

  connect(rootFileByRunNumberAction, 
          SIGNAL(triggered()),
          this, 
          SLOT(getRunNumberFromUserAndProcess()));

  QAction *rootFileByPathAction = 
    new QAction("Load TRP rates from ROOT file...",this);

  rootFileByPathAction->setShortcut(QKeySequence("CTRL+F"));

  connect(rootFileByPathAction,
          SIGNAL(triggered()),
          this,
          SLOT(getRootFilePathFromUserAndProcess()));

  // Set Text Size

  m_setPlotTextSize1Action = new QAction("1", this);
  m_setPlotTextSize1Action->setCheckable(true);

  connect(m_setPlotTextSize1Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize1())); 

  m_setPlotTextSize2Action = new QAction("2", this);
  m_setPlotTextSize2Action->setCheckable(true);

  connect(m_setPlotTextSize2Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize2()));

  m_setPlotTextSize3Action = new QAction("3", this);
  m_setPlotTextSize3Action->setCheckable(true);

  connect(m_setPlotTextSize3Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize3()));

  m_setPlotTextSize4Action = new QAction("4", this);
  m_setPlotTextSize4Action->setCheckable(true);

  connect(m_setPlotTextSize4Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize4()));

  m_setPlotTextSize5Action = new QAction("5", this);
  m_setPlotTextSize5Action->setCheckable(true);

  connect(m_setPlotTextSize5Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize5()));

  m_setPlotTextSize6Action = new QAction("6", this);
  m_setPlotTextSize6Action->setCheckable(true);

  connect(m_setPlotTextSize6Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotTextSize6()));

  QActionGroup *setSizeGroup = new QActionGroup(this);
  setSizeGroup->addAction(m_setPlotTextSize1Action);
  setSizeGroup->addAction(m_setPlotTextSize2Action);
  setSizeGroup->addAction(m_setPlotTextSize3Action);
  setSizeGroup->addAction(m_setPlotTextSize4Action);
  setSizeGroup->addAction(m_setPlotTextSize5Action);
  setSizeGroup->addAction(m_setPlotTextSize6Action);

  // history

  m_setPlotDiffStep1Action = new QAction("-2", this);
  m_setPlotDiffStep1Action->setCheckable(true);
  connect(m_setPlotDiffStep1Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotDiffStep1())); 

  m_setPlotDiffStep2Action = new QAction("-1", this);
  m_setPlotDiffStep2Action->setCheckable(true);

  connect(m_setPlotDiffStep2Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotDiffStep2()));

  m_setPlotDiffStep3Action = new QAction("0", this);
  m_setPlotDiffStep3Action->setCheckable(true);

  connect(m_setPlotDiffStep3Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotDiffStep3()));

  m_setPlotDiffStep4Action = new QAction("1", this);
  m_setPlotDiffStep4Action->setCheckable(true);

  connect(m_setPlotDiffStep4Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotDiffStep4()));

  m_setPlotDiffStep5Action = new QAction("2", this);
  m_setPlotDiffStep5Action->setCheckable(true);

  connect(m_setPlotDiffStep5Action,
          SIGNAL(triggered()), 
          this,
          SLOT(setPlotDiffStep5()));

  QActionGroup *setStepGroup = new QActionGroup(this);
  setStepGroup->addAction(m_setPlotDiffStep1Action);
  setStepGroup->addAction(m_setPlotDiffStep2Action);
  setStepGroup->addAction(m_setPlotDiffStep3Action);
  setStepGroup->addAction(m_setPlotDiffStep4Action);
  setStepGroup->addAction(m_setPlotDiffStep5Action);

  // About

  QAction *aboutAction = new QAction("About TRP GUI", this);
  connect(aboutAction,
          SIGNAL(triggered()),
          this,
          SLOT(about()));

  // Help Contents

  QAction *helpContentsAction = new QAction("Contents", this);

  connect(helpContentsAction,
          SIGNAL(triggered()),
          this,
          SLOT(helpContents()));

  /*** Create Menus ***/

  QMenu *fileMenu = new QMenu(this);
  fileMenu->setTitle("File");
  fileMenu->addAction(rootFileByRunNumberAction);
  fileMenu->addAction(rootFileByPathAction);
  fileMenu->addAction(screenShotAction);
  fileMenu->addAction(quitAction);

  QMenu *plotsMenu = new QMenu(this);
  plotsMenu->setTitle("Plots");

  QMenu *sizeMenu = new QMenu(this);
  sizeMenu->setTitle("Set Text Size");
  sizeMenu->addAction(m_setPlotTextSize1Action);
  sizeMenu->addAction(m_setPlotTextSize2Action);
  sizeMenu->addAction(m_setPlotTextSize3Action);
  sizeMenu->addAction(m_setPlotTextSize4Action);
  sizeMenu->addAction(m_setPlotTextSize5Action);
  sizeMenu->addAction(m_setPlotTextSize6Action);
  // history

  QMenu *stepMenu = new QMenu(this);
  stepMenu->setTitle("Set History Diff Step");
  stepMenu->addAction(m_setPlotDiffStep1Action);
  stepMenu->addAction(m_setPlotDiffStep2Action);
  stepMenu->addAction(m_setPlotDiffStep3Action);
  stepMenu->addAction(m_setPlotDiffStep4Action);
  stepMenu->addAction(m_setPlotDiffStep5Action);


  QMenu *helpMenu = new QMenu(this);
  helpMenu->setTitle("Help");
  helpMenu->addAction(helpContentsAction);
  helpMenu->addSeparator();
  helpMenu->addAction(aboutAction);

  /*** Place Menus ***/

  plotsMenu->addMenu(sizeMenu);
  // history
//  plotsMenu->addMenu(stepMenu);

  menuBar()->addMenu(fileMenu);
  menuBar()->addMenu(plotsMenu);
  menuBar()->addMenu(helpMenu);

}

void TRP_MainWindow::grabScreenShot() {
  QString image_name = QFileDialog::getSaveFileName(this, "Save Image As ...",
    "trp_screenshot.png",
    "Image Files (*.bmp, *.jpg, *.jpeg, *.png, *.ppm, *.tiff, *.xbm, *.xpm)");
  if (image_name.isEmpty()) return;
  QPixmap pixmap = QPixmap::grabWidget(this);
  QImage image = pixmap.toImage();
  bool saved = image.save(image_name);
  if (!saved) {
    QStringList list = image_name.split("/");
    list.removeLast();
    QString dir_name = list.join("/");
    QString message = "The account you are using ";
    message += QString("(%1) may not have permission ").arg(getenv("USER"));
    message += QString("to save to the directory %1.").arg(dir_name);
    warning("Image could not be saved.", message);
  }
}

void TRP_MainWindow::setPlotTextSize(unsigned int size) {
  std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
    for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
      for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
        if (*i_p) (*i_p)->setTextSize(size);
      }
    }
  }
}

void TRP_MainWindow::updatePlotTextSizeInfo() {
  int actual_plot_text_size = -1;
  std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
    for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
      for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
        if (*i_p) {
          int i_text_size = (*i_p)->getTextSize();
          if (i_text_size != actual_plot_text_size) {
            if (actual_plot_text_size == -1) {
              actual_plot_text_size = i_text_size;
            } else {
              actual_plot_text_size = 0;
              break;
            }
          }
        }
      }
    }
  }
  if (actual_plot_text_size > 0) {
    m_plot_text_size = actual_plot_text_size;
  }
  if (actual_plot_text_size == 1) {
    if (m_setPlotTextSize1Action->isChecked() == false) {
      m_setPlotTextSize1Action->setChecked(true);
    }
  } else if (actual_plot_text_size == 2) {
    if (m_setPlotTextSize2Action->isChecked() == false) {
      m_setPlotTextSize2Action->setChecked(true);
    }
  } else if (actual_plot_text_size == 3) {
    if (m_setPlotTextSize3Action->isChecked() == false) {
      m_setPlotTextSize3Action->setChecked(true);
    }
  } else if (actual_plot_text_size == 4) {
    if (m_setPlotTextSize4Action->isChecked() == false) {
      m_setPlotTextSize4Action->setChecked(true);
    }
  } else if (actual_plot_text_size == 5) {
    if (m_setPlotTextSize5Action->isChecked() == false) {
      m_setPlotTextSize5Action->setChecked(true);
    }
  } else if (actual_plot_text_size == 6) {
    if (m_setPlotTextSize6Action->isChecked() == false) {
      m_setPlotTextSize6Action->setChecked(true);
    }
  } else {
    m_setPlotTextSize1Action->setChecked(false);
    m_setPlotTextSize2Action->setChecked(false);
    m_setPlotTextSize3Action->setChecked(false);
    m_setPlotTextSize4Action->setChecked(false);
    m_setPlotTextSize5Action->setChecked(false);
    m_setPlotTextSize6Action->setChecked(false);
  }
}

void TRP_MainWindow::about() {
  QString about_text = "<center>";
  about_text += "<b>";
  about_text += "Trigger Rate Presenter (TRP)";
  about_text += "<br>";
  about_text += "Graphical User Interface (GUI)";
  about_text += "<br>";
  about_text += QString("Version %1").arg(m_gui_version);
  about_text += "</b>";
  about_text += "<br><br>";
  about_text += "For information on other parts of TRP, see:";
  about_text += "<br>";
  about_text += "<a href=\"https://twiki.cern.ch/twiki/bin/view/Atlas/TRPInfo\">https://twiki.cern.ch/twiki/bin/view/Atlas/TRPInfo</a>";
  about_text += "</center>";

  QMessageBox::about(this, "About TRP GUI", about_text);
}

void TRP_MainWindow::helpContents() {
  m_help_browser->resize(500, 400);
  m_help_browser->home();
  m_help_browser->raise();
  m_help_browser->showNormal();
}

// history
void TRP_MainWindow::isHistoryTabClicked(int index) {
  if (!m_tabs_without_history.count(m_tab_widget->tabText(index))) {
    for (unsigned int i = 0; i < m_tabDataMap[index].threads.size(); i++) {
      if (!m_tabDataMap[index].threads[i]->isHistoryEnabled()) {
        m_tabDataMap[index].threads[i]->setHistoryEnabled(true);  
      }
    }
  } else {
    for (unsigned int i = 0; i < m_tabDataMap[index].threads.size(); i++) {
      if (m_tabDataMap[index].threads[i]->isHistoryEnabled()) {
        m_tabDataMap[index].threads[i]->setHistoryEnabled(false);  
      }
    } 
  } 
}

/*
void TRP_MainWindow::emitUpdateHistoryPointDialog(unsigned int point) {
  emit updateHistoryPointDialog(point);
}
*/


void TRP_MainWindow::emitUpdateHistoryStepDialog(int step) {
  emit updateHistoryStepDialog(step);
}

void TRP_MainWindow::setPlotDiffStep(int step) {
  std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
    for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
      for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
        if (*i_p) (*i_p)->setDiffStep(step);
      }
    }
  }
}

void TRP_MainWindow::updatePlotDiffStepInfo() {
  int actual_plot_diff_step = -10000;
  std::map<int, std::map<unsigned int, std::set< QPointer<TRP_Plot> > > >::iterator i_t;
  std::map<unsigned int, std::set< QPointer<TRP_Plot> > >::iterator i_m;
  std::set< QPointer<TRP_Plot> >::iterator i_p;
  for (i_t = m_plots.begin(); i_t != m_plots.end(); ++i_t) {
    for (i_m = i_t->second.begin(); i_m != i_t->second.end(); ++i_m) {
      for (i_p = i_m->second.begin(); i_p != i_m->second.end(); ++i_p) {
        if (*i_p) {
          int i_diff_step = (*i_p)->getDiffStep();
          if (i_diff_step != actual_plot_diff_step) {
            if (actual_plot_diff_step == -10000) {
              actual_plot_diff_step = i_diff_step;
              updateHistoryStepDialog(actual_plot_diff_step);
            } else {
              actual_plot_diff_step = -10001;
              break;
            }
          }
        }
      }
    }
  }
  if (actual_plot_diff_step > -3 && actual_plot_diff_step < 3) {
    m_plot_diff_step = actual_plot_diff_step;
    updateHistoryStepDialog(m_plot_diff_step);
  }
  if (actual_plot_diff_step == -2) {
    if (m_setPlotDiffStep1Action->isChecked() == false) {
      m_setPlotDiffStep1Action->setChecked(true);
    }
  } else if (actual_plot_diff_step == -1) {
    if (m_setPlotDiffStep2Action->isChecked() == false) {
      m_setPlotDiffStep2Action->setChecked(true);
    }
  } else if (actual_plot_diff_step == 0) {
    if (m_setPlotDiffStep3Action->isChecked() == false) {
      m_setPlotDiffStep3Action->setChecked(true);
    }
  } else if (actual_plot_diff_step == 1) {
    if (m_setPlotDiffStep4Action->isChecked() == false) {
      m_setPlotDiffStep4Action->setChecked(true);
    }
  } else if (actual_plot_diff_step == 2) {
    if (m_setPlotDiffStep5Action->isChecked() == false) {
      m_setPlotDiffStep5Action->setChecked(true);
    }
  } else {
    m_setPlotDiffStep1Action->setChecked(false);
    m_setPlotDiffStep2Action->setChecked(false);
    m_setPlotDiffStep3Action->setChecked(false);
    m_setPlotDiffStep4Action->setChecked(false);
    m_setPlotDiffStep5Action->setChecked(false);
  }
}

TFile* TRP_MainWindow::openFileMdaDialog(int runnumber) {
  /* Reurn  a pointer to root file given a run number.  */

  if (!(runnumber > 0)) {
    warning("File not found!", "Failing to open file for run number 0");
    return nullptr;}

  daq::mda::FileRead reader;
  TFile *file = reader.openFile("TriggerRates_ATLAS_"+QString::number(runnumber).toStdString()+".root","");

  if (file and not file->IsZombie()) {
    return file;
  } else {
    std::stringstream ss;
    ss << "No ROOT file with the specified run number was found " << runnumber;
    warning("File not found!", ss.str().c_str());
  }
  return nullptr;
  
}

TFile* TRP_MainWindow::openFileLocallyDialog(QString filepath) {
  /* Reurn  a pointer to root file given a file path.  */

  if (filepath == QString::null) {
    filepath = QFileDialog::getOpenFileName(this, 
                                            tr("Open ROOT file locally"),
                                            QDir::current().absolutePath(), 
                                            tr(".root files (*.root)"));
  }
  
  if (filepath == QString::null) {
    warning("Error!","No ROOT file name supplied");
    return nullptr;
  }

  std::stringstream ss;
  std::string str_filepath = filepath.toStdString();

  QFileInfo info(filepath);
  if (!info.exists()){
    ss << "The ROOT file " << str_filepath << " does not exist";
    warning("Error!",QString(ss.str().c_str()));
    return nullptr;
  }

  TFile *file = nullptr;
  try{
    file = new TFile(filepath.toStdString().c_str());
  } catch(...) {
    ss << "An error occurred while creating the ROOT file " << str_filepath;
    warning("Error!", QString(ss.str().c_str()));
    return nullptr;
  }

  if (!file or file->IsZombie()) {
    ss << "An error occurred while creating the ROOT file " << str_filepath;
    warning("Error!", QString(ss.str().c_str()));
    return nullptr;
  }
    
  return file;
}

TRP_TablePage* TRP_MainWindow::makeRootFileTablePage(TFile* pfile,
                                                     const TRP_Config* config,
                                                     QSignalMapper* thread_mapper,
                                                     TRP_TPDataContainer& tpDataContainer){
  /* Create the root file table page. TablePage objects contain TabWidgets. 
     Here the contained QTabWidget contains the tables for the ROOT files.
     
     Inputs:
     pfile  - the root file being processed
     toContainer - time point container to be filled by reading the ROOT file
     bNfcB - branch name to check box table - association between ROOT file branch name
             and check by the user. This index is used elsewhere
   */
 

  // Read the root files and fill the tpData container with time points
  runRootFileReaders(config, 
                     tpDataContainer, 
                     pfile, 
                     thread_mapper);
  
  for (auto t : tpDataContainer) {t->setHistoryEnabled(true);}

  return new TRP_TablePage(tpDataContainer, config, this);
}

BranchNamesFromCheckBoxResult TRP_MainWindow::branchNamesFromCheckBox(TFile* pfile) {
  /* user informs which root file  branches of root file to precess via a CheckBox */

  QDialog threadDialog(this);
  //throughout the TRP code, the term "thread" is used, 
  // rather than "branch", but the latter seems more clear for the user
  threadDialog.setWindowTitle(tr("Branch list"));
  
  QGridLayout gridlayout(&threadDialog);
  QBoxLayout layout_checkboxes(QBoxLayout::TopToBottom);
  QBoxLayout layout_checkall(QBoxLayout::LeftToRight);
  QBoxLayout layout_buttons(QBoxLayout::LeftToRight);
  gridlayout.addLayout(&layout_checkboxes,0,0);
  gridlayout.addLayout(&layout_checkall,1,0);
  gridlayout.addLayout(&layout_buttons,2,0);

  QLabel description("Choose which branches to load:");
  layout_checkboxes.addWidget(&description);
  
  QVector<TRP_CheckBox*> CheckBoxVec;
  
  TTree* tree;
  QString tree_name;
  QStringList name_list;
  //Read run number and thread names from TFile
  TIter iter(pfile->GetListOfKeys());
  while ((tree = (TTree*)iter.Next())) {  // compiler suggests the extra parens
  	tree_name = tree->GetName();
	if (!name_list.contains(tree_name)) {
      name_list << tree_name;
	}
  }
  
  //Sort names alphabetically, but place L1_Rate, L2_Rate and EF_Rate at the top
  name_list.sort();
  int index = name_list.indexOf("L1_Rate");
  if (index >= 0) name_list.move(index,0);
  index = name_list.indexOf("Lu_Rate_Luminosity");
  if (index >= 0) name_list.move(index,1);
  
  for (auto name : name_list) {CheckBoxVec.append(new TRP_CheckBox(name));}
  
  if (CheckBoxVec.empty()){
    warning("Empty Root File ", "No branches found in root file");
    return BranchNamesFromCheckBoxResult();
  }

  //Create dialog to choose which threads to load
  for (auto box : CheckBoxVec) {
    QString boxtext = box->text();
    if (boxtext == "L1_Rate" || boxtext == "HLT_Rate" || boxtext == "Lu_Rate_Luminosity"
        || boxtext == "L2_Rate" || boxtext == "EF_Rate" 
        || boxtext == "Test_HLT_Rate"
        ) {
      box->setCheckState(Qt::Checked);
	}
    layout_checkboxes.addWidget(box);
  }
  
  QPushButton button_Checkall(tr("Check/uncheck all"));
  QPushButton button_OK(tr("OK"));
  //QPushButton button_Cancel(tr("Cancel")); //has been removed because acts just as button_OK, needs to be fixed
  for (int i=0; i<CheckBoxVec.size(); i++) {
    connect(&button_Checkall,
            SIGNAL(clicked()),
            CheckBoxVec.at(i),
            SLOT(changeCheckedState()));
  }
  
  connect(&button_OK,
          SIGNAL(clicked()),
          &threadDialog,
          SLOT(accept()));

  layout_checkall.addWidget(&button_Checkall);
  layout_buttons.addWidget(&button_OK);
  
  button_OK.setFocus();
  
  //wait for user input to dialog box
  //int accepted = threadDialog.exec();
  threadDialog.exec();  
  
  //While no boxes are checked (i.e. no branches selected), will output
  //an error message, prompting the user to check some boxes.
  //Also initiliazes nL1, nL2 and nEF which will be used to create plots in createRootPlotsPage
  /* FIXME set values in the map which are required by code in prcessRootFile.
   This referes to obsolete branches, so should be fixed. */
  int numCheck{0};
  BranchNamesFromCheckBoxResult result;

  do{
    for (auto box : CheckBoxVec){
      if (box->checkState() == Qt::Checked) {
        result.thread_list.push_back(box->text());
        if (box->text().toStdString() == "L1_Rate") {
          result.nL1 = numCheck;
        }
        if (box->text().toStdString() == "L2_Rate") {
          result.nL2 = numCheck;
        }
        if (box->text().toStdString() == "EF_Rate") {
          result.nEF = numCheck;
        }
        ++numCheck;
      }
    }
    if(result.thread_list.size()==0){
      QMessageBox message(QMessageBox::Warning,
                          QString("Warning"),
                          QString("No branches chosen."),
                          QMessageBox::Ok,
                          this);
      message.setInformativeText("Please check at least one box.");
      message.exec();
      threadDialog.exec();
    }
  }
  while(result.thread_list.size()==0);
  
  qDeleteAll(CheckBoxVec);
  CheckBoxVec.clear();

  return result;
}

void TRP_MainWindow::processRootFile(TFile* pfile) {
  /* Read root file, display contents */

  //user inputs root file branches to be processed.
  auto checkBoxResult  = branchNamesFromCheckBox(pfile);

  // No branches found in root file
  if (checkBoxResult.thread_list.empty()) {return;}

  TRP_TPDataContainer tpDataContainer;

  TRP_Config* config = new TRP_Config(checkBoxResult.thread_list);
  

  QSignalMapper* thread_mapper = new QSignalMapper(this);

  auto table_page = makeRootFileTablePage(pfile,
                                          config,
                                          thread_mapper,
                                          tpDataContainer);

  QString filename = pfile->GetName();
  filename.remove(0,filename.lastIndexOf("TriggerRates")); 
  filename.remove(QRegExp("[^0-9]"));
  
  int tab_index = m_tab_widget->addTab(table_page,
                                     "Run "+filename+" Tables");

  m_tab_widget->setTabWhatsThis(tab_index, m_tableID_counter);
  table_page->setTableID(m_tableID_counter.toInt());

  connect(table_page, 
          SIGNAL(plotRequest(std::deque<TRP_Config::Trend*>,int)),
          this, 
          SLOT(makePlot(std::deque<TRP_Config::Trend*>,int)));

  connect(table_page, 
          SIGNAL(plotCorrelationRequest(std::deque<TRP_Config::Trend*>, int)),
          this, 
          SLOT(makeCorrelationPlot(std::deque<TRP_Config::Trend*>, int)));

  //Fill struct with tab data. This will be placed in A MAP
  // (an attribute of this class) with key being a page index.
  // The map will be accessed by the trend ojects to obtain the
  // data read by the reader.
  tabDataStruct tabData;
  tabData.table_page = table_page;
  tabData.thread_mapper = thread_mapper;
  for (auto tpdp : tpDataContainer) {
    tabData.threads.push_back(tpdp);
  }
  tabData.config = config;

  m_tabDataMap.insert(std::pair<int,tabDataStruct>(m_tableID_counter.toInt(),
                                                 tabData));
  
  //used to define plots in createRootPlotsPage
  int tableID = m_tableID_counter.toInt(); 

  m_tableID_counter.setNum(m_tableID_counter.toInt()+1);

  // creates a plot page from configuration data hardwired into the method
  // createRootPlotsPage also calls makePlots which access m_tabDataMap
  // to obtain data.
  createRootPlotsPage(filename, checkBoxResult, tableID);
}

void TRP_MainWindow::runRootFileReaders(const TRP_Config* root_config,
                                        TRP_TPDataContainer& tpdataContainer,
                                        TFile* pfile,
                                        QSignalMapper* mapper){

  // Loop over configured "threads", and read a root branch for each.
  
  // root file loading may take some time - show the user where we are.
  //int step = 100/(root_config->threads.size());
  //ReadProgressWidget progress(step);
  //TRP_ProgressDialogWidget progress(step, "Reading root file threads...");

  auto nthreads = root_config->threads.size();

  TRP_ProgressDialogWidget* progress = new TRP_ProgressDialogWidget(nthreads, 
                                                                    "reading root file...", 
                                                                    this);
  // ensure the thread can live until it quits
  m_dialogProgressWidgets.push_back(progress);
  progress->start();

  int i{0};

  connect(this, SIGNAL(updateProgressDialog()), progress, SLOT(update()));
  connect(this, SIGNAL(bumpProgressDialog()), progress, SLOT(bump()));
  emit updateProgressDialog();

  auto historySize = root_config->historySize();
 
  for (auto newthread : root_config->threads) {
    emit updateProgressDialog();
    tpdataContainer.push_back(runOneRootFileReader(historySize,
                                                   i++,
                                                   newthread,
                                                   pfile,
                                                   mapper));
    emit bumpProgressDialog();
  }
  progress->quit();
}

TRP_TPData* 
  TRP_MainWindow::runOneRootFileReader(unsigned int historySize,
                                       int index,
                                       const TRP_Config::Thread& thread,
                                       TFile* pfile,
                                       QSignalMapper* mapper){
  
  auto threadUnit=getUnitNameFromConfigFile(thread);
  auto threadAlias=getAliasNameFromConfigFile(thread);
  
  //make a pointer to a TRP_TPData object. Cannot use a shared pointer
  // due to QT class heirarchy....The caller owns the pointer
  
  bool history_enabled = false;
  TRP_TPDataPtr tpdata = new TRP_TPData(historySize,
                                        thread.name,
                                        threadAlias,
                                        threadUnit,
                                        history_enabled,
                                        m_debugOn,
                                        nullptr);
  
  // add the time points to the root file reader. This will 
  // read the file and fill the points
  TRP_RootFileReader reader(tpdata, thread.name, pfile, this);
  connect(&reader,
          SIGNAL(gotData()),
          mapper,
          SLOT(map()));
  
  mapper->setMapping(&reader, index);
  
  // the reader will now read the root file and fill in the time point data
  reader.run();
  return tpdata;
}
 

void TRP_MainWindow::deleteTabData(int tab_index) {

  if (m_tabDataMap.find(tab_index) != m_tabDataMap.end()) {
  
    delete m_tabDataMap[tab_index].table_page;
    for (unsigned int i=0; i<m_tabDataMap[tab_index].threads.size(); ++i) {
      // IRH Threads needed for ntuple run plots page
      //delete m_tabDataMap[tab_index].threads[i];
    }
    delete m_tabDataMap[tab_index].thread_mapper;
    delete m_tabDataMap[tab_index].config;

    m_tabDataMap.erase(tab_index);

  }
}

QString 
TRP_MainWindow::getUnitNameFromConfigFile(const TRP_Config::Thread &rootthread) const
{

  QString unit_name = "Hz";
  
  for (unsigned int i=0; i<m_config->threads.size(); i++) {
    TRP_Config::Thread c_th = (m_config->threads)[i];
    if (c_th.name==rootthread.name) unit_name=c_th.unit;
  }
  return unit_name;
}

QString 
TRP_MainWindow::getAliasNameFromConfigFile(const TRP_Config::Thread &rootthread) const
{

  QString alias_name = rootthread.name;

  for (unsigned int i=0; i<m_config->threads.size(); i++) {
    TRP_Config::Thread c_th = (m_config->threads)[i];
    if (c_th.name==rootthread.name) alias_name=c_th.alias;
  }

  return alias_name;
}
