#ifndef TRP_PROGRESSDIALOGWIDGET_H
#define TRP_PROGRESSDIALOGWIDGET_H
#include  <QProgressDialog>
#include <QThread>


class TRP_ProgressDialogWidget: public QThread {
  Q_OBJECT
 public:
  TRP_ProgressDialogWidget(int, const std::string&, QObject*);
  ~TRP_ProgressDialogWidget();
  TRP_ProgressDialogWidget& operator=(const TRP_ProgressDialogWidget&) = delete;
  TRP_ProgressDialogWidget (const TRP_ProgressDialogWidget&) = delete;
  
 private:
  int m_val{0};
  QProgressDialog* m_widget;

public slots:
  void bump();
  void update();

};
  
#endif
