#include <QtGui>
#include "trp_chain_model.h"
//#include <TRP/TriggerMenuHelpers.h>
//#include <TRP/TrigConfSmKey.h>
#include "TRP/TriggerMenuHelpers.h"
#include "TRP/TrigConfSmKey.h"


TRP_ChainModel::TRP_ChainModel(const TRP_TPDataContainer& t,
			       const TRP_Config *c, 
			       QObject *parent)
  : TRP_BaseModel(parent), m_tpDataContainer(t) {

  for(auto tp : m_tpDataContainer){
    connect(tp, 
            SIGNAL(gotData()), 
            this, 
            SIGNAL(layoutAboutToBeChanged()));
  }

  for(uint i=0;i<m_tpDataContainer.size();i++){
    if(m_tpDataContainer[i]->getAlias()=="STR") STRIndex=i;
    if(m_tpDataContainer[i]->getAlias()=="HLT") HLTIndex=i;
    if(m_tpDataContainer[i]->getAlias()=="EF") EFIndex=i;
    if(m_tpDataContainer[i]->getAlias()=="L2") L2Index=i;
    if(m_tpDataContainer[i]->getAlias()=="L1") L1Index=i;
  }

  aliasName=c->triggerDBName().toStdString();
  partitionName=c->partitionName().toStdString();
  serverName=c->serverName().toStdString();

  STR.clear();
  HLT.clear();
  EF.clear();
  L2.clear();
  L1.clear();
  IPCPartition* mPartition = new IPCPartition( partitionName );
  ISInfoReceiver* receiver = new ISInfoReceiver(*mPartition);

  // subscription fails silently when running on root files?
  try {
    receiver->subscribe("RunParams",
                        TrigConfSmKey::type(),
                        &TRP_ChainModel::callback_updateSMK,this);
  } catch (...) {}
}


int TRP_ChainModel::rowCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);

  if(HLT.size()==0 &&  m_tpDataContainer[HLTIndex]->size()==0)return 5;
  if(HLT.size()==0) return  m_tpDataContainer[HLTIndex]->latest()->XLabels.size();
  return HLT.size();

  return 0;
}

int TRP_ChainModel::columnCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  return 9;
}

QVariant TRP_ChainModel::data(const QModelIndex &idx, int role) const { 

  if (!idx.isValid() || m_tpDataContainer[0]->size()==0) return QVariant();
  
    if (role == Qt::DisplayRole) {
      return getDataPoint(idx);
    } else if (role == Qt::TextAlignmentRole) {
      // center vertically, and left-align vertical labels
      return QVariant(Qt::AlignVCenter | ((idx.column()!=0 && idx.column()!=4 && idx.column()!=8) ? Qt::AlignRight : Qt::AlignLeft));
    } else if (role == Qt::BackgroundColorRole) {
      return (idx.column()!=0 && idx.column()!=4 && idx.column()!=8) ? QVariant() : qApp->palette().color(QPalette::Button);
    } else return QVariant();

  return QVariant();
}

QVariant TRP_ChainModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role != Qt::DisplayRole || m_tpDataContainer[0]->size() == 0) return QVariant();
  if (orientation == Qt::Horizontal) {
    return getColName(section);
  }
  else return QVariant();
}

void TRP_ChainModel::callback_updateSMK(ISCallbackInfo*isc)
{
   TrigConfSmKey sm_k;
   isc->value(sm_k);
   if(sm_k.SuperMasterKey != smk_old )
      updateSMK();
}

void TRP_ChainModel::updateSMK()
{       
  confadapter_imp::TriggerMenuHelpers menu(partitionName,aliasName);
  unsigned int smk=0;
  unsigned int l1k=0;
  unsigned int hltk=0;
  try{
    trp_utils::GetTriggerKeys(partitionName, smk, hltk, l1k  );
  } catch (std::runtime_error &exception) {
    std::cout << "runtime error on getTriggerKeys " << std::endl;
  } catch (std::exception &exception) {
    std::cout << "exeption on getTriggerKeys " << std::endl;
  }
  if(smk==smk_old) return;
  smk_old=smk;
  try{
    menu.getConfiguration(smk, hltk, l1k);
  } catch (std::runtime_error &exception) {
    std::cout << "runtime error on getConfiguration" << std::endl;
  } catch (std::exception &exception) {
    std::cout << "exeption on getConfiguration" << std::endl;
  }
 
  isRun2 = menu.lhcRun() >= 2;
  isRun1 = menu.lhcRun() == 1;
  std::string run = isRun2 ? "Run 2" : "Run 1";
  std::cout << "This is a " << run << " menu." << std::endl;

    // read the configuration in the new mapping format
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_hlt_l1_TEMP = menu.getMapping("hlt_l1");
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_str_hlt_TEMP = menu.getMapping("str_hlt");
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_str_type_TEMP = menu.getMapping("str_type"); //stream type

    // convert them to the old multimapping format
    confadapter_imp::TriggerMenuHelpers::MultiMapping  m_hlt_l1;
    confadapter_imp::TriggerMenuHelpers::MultiMapping  m_str_hlt;
    confadapter_imp::TriggerMenuHelpers::MultiMapping  m_str_type;

    for (auto x: m_hlt_l1_TEMP)
    {
      for (std::string y: x.second)
      {
        m_hlt_l1.insert(std::pair<std::string,std::string>( x.first ,y ));
      }
    }


    for (auto x: m_str_hlt_TEMP)
    {
      for (std::string y: x.second)
      {
        m_str_hlt.insert(std::pair<std::string,std::string>( x.first ,y ));
      }
    }


    for (auto x: m_str_type_TEMP )
    {
      for (std::string y: x.second)
      {
        m_str_type.insert(std::pair<std::string,std::string>( x.first ,y ));
      }
    }


    confadapter_imp::TriggerMenuHelpers::MultiMapping m_l1_hlt;

    //menu.printMultiMapping(m_hlt_l1 ," " );
    //menu.printMultiMapping(m_str_hlt ," " );
    //menu.printMultiMapping(m_str_type ," " );

    confadapter_imp::TriggerMenuHelpers::MultiMapping::const_iterator it;
    for ( it=m_hlt_l1.begin() ; it != m_hlt_l1.end(); it++ )
      m_l1_hlt.insert(std::pair<std::string,std::string>( (*it).second , (*it).first ));

    confadapter_imp::TriggerMenuHelpers::MultiMapping::const_iterator mMap_it1, mMap_it2, mMap_it3;
    for(mMap_it1=m_l1_hlt.begin();mMap_it1!=m_l1_hlt.end();mMap_it1++) {

      std::string key1=(*mMap_it1).first;  //HLT
      std::string val1=(*mMap_it1).second; //L1 

      std::string key2; //stream
      std::string val2; //HLT
      std::string streamName = "";
      std::string streamType;

      ///find matching HLT in Map_stream_HLT, and assign stream.
      for(mMap_it2=m_str_hlt.begin();mMap_it2!=m_str_hlt.end();mMap_it2++) {

        if (key1 != (*mMap_it2).second)continue;
        key2=(*mMap_it2).first; //stream
        val2=(*mMap_it2).second; //HLT
  
        //get stream type
        for(mMap_it3=m_str_type.begin();mMap_it3!=m_str_type.end();mMap_it3++) {

          if (key2 != (*mMap_it3).first)continue;
          streamType=(*mMap_it3).second;
          streamName=key2+"_"+streamType;   
        }

        int nKey1=m_l1_hlt.count(key1);
     
        if(nKey1 > 1) {
        
          STR.push_back(QString::fromStdString(streamName));
          HLT.push_back(QString::fromStdString(key1));
          L1.push_back(QString::fromStdString("Multi"));

          mMap_it1=m_l1_hlt.upper_bound(key1);
          mMap_it1--; //make up for the ++ that will be applied in a second
          continue;
        } else {
          STR.push_back(QString::fromStdString(streamName));
          if(key1.size()<=80)
            HLT.push_back(QString::fromStdString(key1));
          else
            HLT.push_back(QString::fromStdString(key1.substr(0,80).append("...")));
          if(val1.size()<=80)
            L1.push_back(QString::fromStdString(val1));
          else
            L1.push_back(QString::fromStdString(val1.substr(0,80).append("...")));

          //HLT.push_back(QString::fromStdString(key1));
          //L1.push_back(QString::fromStdString(val1));
        }//number HLT>1
      } //map m_str_hlt

    } //map m_hlt_l1
}


QString TRP_ChainModel::getSTRName(uint i) const 
{
   if(i>= STR.size()) return "";
   return STR[i];
}

QString TRP_ChainModel::getHLTName(uint i) const
{
   if(i>= HLT.size()) return "";
   return HLT[i];
}

QString TRP_ChainModel::getEFName(uint i) const 
{
   if(i>= EF.size()) return "";
   return EF[i];
}

QString TRP_ChainModel::getL2Name(uint i) const 
{
   if(i>= L2.size()) return "";
   return L2[i];
}

QString TRP_ChainModel::getL1Name(uint i) const 
{
   if(i>= L1.size()) return "";
   return L1[i];
}
  
QString TRP_ChainModel::getRowName(const QModelIndex& idx) const
{
  int row = idx.row();
  int col = idx.column();

    if(col < 4)
      return getL1Name(row);
    else if (col < 8)
      return getHLTName(row);
    else if (col== 8)
      return getSTRName(row);

   return "";
}

QString TRP_ChainModel::getColName(const QModelIndex& idx) const
{
   int col = idx.column();
   return getColName(col);
}

QString TRP_ChainModel::getColName(int col) const
{
  QString hlabel="";

    if( m_tpDataContainer[HLTIndex]->size()==0 || m_tpDataContainer[L1Index]->size()==0 ) return hlabel;
    if (col== 0)return QString("L1 Name");
    if (col== 1)hlabel  = QString::fromStdString(m_tpDataContainer[L1Index]->latest()->YLabels[0]);
    if (col== 2)hlabel  = QString::fromStdString(m_tpDataContainer[L1Index]->latest()->YLabels[3]);
    if (col== 3)hlabel  = QString::fromStdString(m_tpDataContainer[L1Index]->latest()->YLabels[2]);
    if (col== 4)return QString("HLT Name");
    if (col== 5)hlabel  = QString::fromStdString(m_tpDataContainer[HLTIndex]->latest()->YLabels[0]);
    if (col== 6)hlabel  = QString::fromStdString(m_tpDataContainer[HLTIndex]->latest()->YLabels[1]);
    if (col== 7)hlabel  = QString::fromStdString(m_tpDataContainer[HLTIndex]->latest()->YLabels[3]);
    if (col== 8)return QString("Stream Name");

   return hlabel;


}
int TRP_ChainModel::getThreadIndex(int col) const
{

    if(col < 4) {
      return L1Index;
    } else if (col < 8) {
      return HLTIndex;
    } else if (col == 8) {
      return STRIndex;
    }

   return 0;

}

QVariant TRP_ChainModel::getDataPoint(const QModelIndex & idx) const
{
  // Vertical header column
  if (idx.column()%4 == 0) return getRowName(idx); //assum will be 1 title and 3 data fields =>mod4
  float value;
  m_tpDataContainer[getThreadIndex(idx.column())]->getLatestValue(getRowName(idx),getColName(idx), value);
  return value;
}

bool TRP_ChainModel::isTitleColumn(int col) const
{
  return (col%4==0); 

}

void TRP_ChainModel::update()
{
  updateSMK();
  emit layoutChanged();
}
