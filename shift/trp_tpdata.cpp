#include "trp_tpdata.h"
#include <sstream>

TRP_TPData::TRP_TPData(unsigned int nhistory,
                       const QString& name,
                       const QString& alias,
                       const QString& unit,
                       bool historyEnabled,
                       bool debugOn,
                       QObject* parent = 0):
  QObject{parent},
  m_name{name},
  m_alias{alias},
  m_unit{unit},
  m_nhistory{nhistory},
  m_historyEnabled{historyEnabled},
  m_debugOn{debugOn}{
}

TRP_TPData::~TRP_TPData(){}

QString TRP_TPData::getName() const noexcept {return m_name;}

void TRP_TPData::setTimePoints(const std::deque <std::shared_ptr<TimePoint_IS>>& tps){ 
  
  m_mutex.lock();
  m_time_points = tps;
  for (const std::shared_ptr<TimePoint_IS>& tp : m_time_points) {
      modifyTimePoint(tp.get());
  }
  m_mutex.unlock();
  m_updated = true;
  m_lastUpdate = std::chrono::system_clock::now();
  /* The table tabs in the gui display reuquire filled in  time points
     to set up the tables (technically: the trp_nase_model derived classes
     associated with the tables require this. Inform them that a tpData
     object is available.
   */
  if (m_debugOn){std::cout<< toStringShort() << '\n';} //debug
  emit gotData();
}

void TRP_TPData::addTimePoint(const std::shared_ptr<TimePoint_IS>& tp){

  
  m_mutex.lock();
  modifyTimePoint(tp.get());
  m_time_points.push_back(tp);
  while (m_time_points.size() > m_nhistory) {m_time_points.pop_front();}
  m_mutex.unlock();

  if (m_debugOn){std::cout<< toStringShort() << '\n';} //debug

  m_updated = true;
  m_lastUpdate = std::chrono::system_clock::now();

  /* The table tabs in the gui display reuquire filled in  time points
     to set up the tables (technically: the trp_nase_model derived classes
     associated with the tables require this. Inform them that a tpData
     object is available.
   */
  emit gotData();
}

void TRP_TPData::modifyTimePoint (TimePoint_IS* tp) {

    // Add TAV x (PS > 0) column
    // -------------------------
    // Define all pairs of (rate, prescal) column to try.
    std::vector< std::pair<std::string, std::string> > name_pairs = {
        std::pair<std::string, std::string>("TAV", "PS"),            // L1
        std::pair<std::string, std::string>("output", "prescaled")  // HLT
    };

    // Loop of pairs of columns.
    for (const std::pair<std::string, std::string>& name_pair : name_pairs) {

        // Define convenience variables.
        const std::string name_rate = name_pair.first;
        const std::string name_ps   = name_pair.second;

        // Checking whether the current `TimePoint_IS` has the requried columns.
        const int y_TAV = std::find(tp->YLabels.begin(), tp->YLabels.end(), name_rate) - tp->YLabels.begin();
        const int y_PS  = std::find(tp->YLabels.begin(), tp->YLabels.end(), name_ps)   - tp->YLabels.begin();
        const std::string new_label = name_rate + " * (" + name_ps + " > 0)";
        if (y_TAV < (int) tp->YLabels.size() &&
            y_PS  < (int) tp->YLabels.size() &&
            std::find(tp->YLabels.begin(), tp->YLabels.end(), new_label) == tp->YLabels.end()) {

            // Pad the TimePoint_IS `Data` member and add new column
            for (int index = tp->NumRow() * tp->NumCol() - 1; index > 0; index -= tp->NumRow()) {
                tp->Data.insert(tp->Data.begin() + index + 1, 0);
            }
            tp->YLabels.push_back(new_label);  // Do this only after padding `Data`.

            // Set values of new column
            const int y = tp->YLabels.size() - 1;  // New, target column is last
            float TAV, PS;
            for (int x = 0; x < (int) tp->XLabels.size(); x++) {
                tp->get(x, y_TAV, TAV);
                tp->get(x, y_PS,  PS);
                tp->set(x, y, PS > 0 ? TAV : 0);
            }
        }
    }
}

void TRP_TPData::getPoints(const QString& m_nameX, 
                           const QString& m_nameY, 
                           std::deque<std::shared_ptr<QPointF>>& points) const noexcept{
  float value;
  bool got_value;
  m_mutex.lock();
  for (auto p_tp : m_time_points){
    got_value = p_tp->get(m_nameX.toStdString(), m_nameY.toStdString(), value);
    if (got_value) {
      if (std::isnan(value) || std::isinf(value)) {
        value = 1e38;
      }
      auto point = std::make_shared<QPointF>(p_tp->TimeStamp.c_time(), value);
      points.push_back(point);
    }
  }
  m_mutex.unlock();
}
 
void TRP_TPData::getLatestValue(const QString &m_nameX, 
                                const QString &m_nameY, 
                                float &value) const noexcept{
  m_mutex.lock();
  if (!m_time_points.back()->get(m_nameX.toStdString(), m_nameY.toStdString(), value)){
    value = 0.;}
  m_mutex.unlock(); 
}

std::shared_ptr<TimePoint_IS> TRP_TPData::latest() const noexcept {
  m_mutex.lock();
  auto tp = m_time_points.back();
  m_mutex.unlock();
  return tp;
}

std::deque<std::shared_ptr<TimePoint_IS>> TRP_TPData::getTimePoints() const noexcept {
  m_mutex.lock();
  std::deque<std::shared_ptr<TimePoint_IS>> ctp =  m_time_points;
  m_mutex.unlock();
  return ctp;
}
  
unsigned int TRP_TPData::size() const noexcept {
  m_mutex.lock();
  std::size_t s =  m_time_points.size();
  m_mutex.unlock();
  return s;
}


bool TRP_TPData::isHistoryEnabled() const noexcept { 
  return m_historyEnabled;
}

void TRP_TPData::setHistoryEnabled(bool state) {
  m_historyEnabled = state;
  emit historyEnabled();
}

bool TRP_TPData::isHistoryDiff() const noexcept{
  return m_historyDiff;
}


void TRP_TPData::lockHistoryPoint(){
  auto sz = size();
  if (sz == m_nhistory) {
    m_historyPoint -= 1;
  }
  emit gotHistoryPoint(m_alias, m_historyPoint);
}


void TRP_TPData::updateHistoryDiff(int state) {
  if (state==Qt::Checked) {
    m_historyDiff = true;
  } else {
    m_historyDiff = false;
  }
  emit gotNewHistoryDiff();
}

void TRP_TPData::updateHistoryLock(int state){
  if (state==Qt::Checked) {
    m_historyLock = true;
  } else {
    m_historyLock = false;
  }
  emit gotNewHistoryLock();
}


unsigned int TRP_TPData::getHistoryPoint() const noexcept {return m_historyPoint;}

unsigned int TRP_TPData::getHistoryStep() const noexcept {return m_historyStep;}

void TRP_TPData::getHistoryValue(const QString &m_nameX, 
                                 const QString &m_nameY, 
                                 float &value) const {
  unsigned int max_points = size();
  unsigned int historyPoint_two = m_historyPoint + m_historyStep;
  float value_two;
  if (m_historyPoint >= max_points || historyPoint_two >= max_points) {
    value = 0.;
  } else {
    bool got_value;
    bool got_value_two = false;

    m_mutex.lock();
    got_value = m_time_points[m_historyPoint]->get(m_nameX.toStdString(), 
                                                   m_nameY.toStdString(),
                                                   value);
    if (historyPoint_two != m_historyPoint) {
      got_value_two = 
        m_time_points[historyPoint_two]->get(m_nameX.toStdString(), 
                                             m_nameY.toStdString(), 
                                             value_two);
    }
    m_mutex.unlock();

    if (got_value == false) {
      value = 0.;
    }
    if (got_value_two == true) {
      if (m_historyDiff) {
        value = (value-value_two)/value;
        if (std::isnan(value) || std::isinf(value)) {
          value = 1e38;
        }
      } else {
        value = value - value_two;
      }
    }
  }
}
 
unsigned int TRP_TPData::getNhistory() const noexcept {return m_nhistory;}

QString TRP_TPData::getAlias() const noexcept {return m_alias;}

QString TRP_TPData::getUnit() const noexcept {return m_unit;}

std::shared_ptr<TimePoint_IS> TRP_TPData::history(int i) const noexcept {
  m_mutex.lock();
  auto tp = m_time_points[i];
  m_mutex.unlock();
  return tp;
}
  
void TRP_TPData::updateHistoryPoint(unsigned int point){
  m_historyPoint = point;
  emit gotNewHistoryPoint();
}


void TRP_TPData::updateHistoryStep(int step){
  m_historyStep = step;
  emit gotNewHistoryStep();
}

std::string timeAsString (const std::chrono::system_clock::time_point& tp)
{
     // convert to system time:
     std::time_t t = std::chrono::system_clock::to_time_t(tp);
     std::string ts = std::ctime(&t);// convert to calendar time
     ts.resize(ts.size()-1);         // skip trailing newline
     return ts;
}

std::string TRP_TPData::toString() const{
  std::stringstream ss;
  auto now = std::chrono::system_clock::now();
  ss << "TRP_TPData: "
     << m_name.toStdString()
     << " "
     << timeAsString(now)
     <<" alias "
     << getAlias().toStdString()
     <<" nhistory "
     << m_nhistory
     << '\n'
     <<" history enabled "
     << std::boolalpha
     << m_historyEnabled
     << " history point "
     << m_historyPoint
     << " history step "
     << m_historyPoint
     << " history diff "
     << m_historyDiff
     << " history lock "
     << m_historyDiff;

  if (!m_updated){
    ss << " no data\n";
    return ss.str();
  }

  ss << "\nupdated: "
     << timeAsString(m_lastUpdate);

  auto s = size();
  if (s > 0){
    auto last = latest();
    ss << " N time points "
       << s
       << " X vals "
       << last->XLabels.size()
       << " Y vals "
       << last->YLabels.size()
       << " Data size "
       << last->Data.size()
       <<" max size "
       << m_nhistory;
  }


  return ss.str();
}

std::string TRP_TPData::toStringShort() const{
  std::stringstream ss;
  auto now = std::chrono::system_clock::now();
  ss << "TRP_TPData: "
     << m_name.toStdString()
     << " "
     << timeAsString(now)
     <<" alias "
     << getAlias().toStdString();

  if (!m_updated){
    ss << " no data\n";
    return ss.str();
  }

  ss << " updated: "
     << timeAsString(m_lastUpdate);

  auto s = size();
  if (s > 0){
    auto last = latest();
    ss << " N time points "
       << s
       << " X vals "
       << last->XLabels.size()
       << " Y vals "
       << last->YLabels.size()
       << " Data size "
       << last->Data.size()
       <<" max size "
       << m_nhistory;
    return ss.str();
  }

  ss << " zero time points\n";
  return ss.str();
}  
    
std::ostream&  operator << (std::ostream& o, const std::shared_ptr<TRP_TPData>& t){
  o << t->toStringShort();
  return o;
}

std::ostream&  operator << (std::ostream& o, const TRP_TPData* t){
  o << t->toStringShort();
  return o;
}
