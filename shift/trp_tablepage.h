/** @file: trp_tablepage.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_TablePage: Page Containing TRP trigger-rate tables
 */

#ifndef TRP_TABLEPAGE_H
#define TRP_TABLEPAGE_H

#include <deque>
#include "trp_typeAliases.h"
#include <QWidget>
#include <QDateTime>
#include <QDateTimeEdit>
#include "trp_config.h"
#include "trp_table.h"
// history
#include<QSpinBox>
#include<QHBoxLayout>

class TimePoint_IS;
class TRP_ISThread;
class TRP_Table;
class QLineEdit;
class QCheckBox;
//IRH 2011-03-14
class QTabWidget;
class QTabWidget;
class QLabel;
class TRP_BaseModel;

class TRP_TablePage : public QWidget {
  Q_OBJECT

public:
  TRP_TablePage(const TRP_TPDataContainer&, 
                const TRP_Config *c, 
                QWidget *parent = 0);

private:
  TRP_TablePage(const TRP_TablePage &);
  TRP_TablePage& operator = (const TRP_TablePage &);

public:
  QTabWidget* getTabWidget() const noexcept;
  std::deque <TRP_Table *> getTables() const noexcept;
  std::vector<int> getTabHideCheckbox() const noexcept;
  QLineEdit* getLineEdit() const noexcept;
  TRP_TPDataContainer getThreads() const noexcept;
  void setTableID(int ID);
  
signals:
  void plotRequest(std::deque<TRP_Config::Trend*>,int);
  void plotCorrelationRequest(std::deque<TRP_Config::Trend*>,int);

private:
  std::deque <TRP_Table *> m_tables;
  TRP_TPDataContainer m_tpContainer;
  QTabWidget* m_tabwidget;
  QLineEdit*  m_filterline;
  QCheckBox*  m_filtercheckbox;

  QBoxLayout* top_layout;
  QHBoxLayout* middle_layout;
  QHBoxLayout* middle_layout2;

  int m_tableID;
  int m_lastTabVisited{0};

  TRP_Config::Trend  m_xxtrend;
  void setXtrend(TRP_Config::Trend &trendx);
  TRP_Config::Trend  getXtrend() const noexcept;


// hide disabled
  void applyHide(bool hidedisabled);
  QCheckBox* hide_checkbox;

  
// history
signals:
  void gotNewHistoryPoint(int);
  void gotTime(QString);

private slots:
  void updateHistoryTimeLabel(int);
  void updateHistoryTimeLabel(QString,unsigned int);
  void updateHistoryTimeLabelIfCurrent(int);
  void updateHistoryPointDialog(int);
  void updateHistoryPointDialog(unsigned int);
  void updateHistoryPoints(QDateTime);
  void updateHistoryStepDialog(int);
  void updateSpinboxLimits(int);
  void getHistoryPointsFromLumiblock(int);
  
private:
//  void updateHistoryStepDialogRange(int);
  void updateHistoryTimeLabel(unsigned int, unsigned int);

  unsigned int secDiff(QDateTime, OWLTime);
  std::vector<unsigned int> findNearestHistoryPoints(QDateTime);
  TRP_BaseModel*   source_model; 
  QLineEdit*   m_historyline;
  QString    m_filterText;
  std::vector<int> m_tabHideCheckbox;
  QSpinBox* history_point_spinbox;
  QSpinBox* lumiblock_spinbox;
  QDateTimeEdit* datetime_spinbox;
  QSpinBox* history_step_spinbox;
  std::deque<QLabel*> m_history_time_labels;
  std::deque<unsigned int> mm_point;
//  std::deque< std::deque<QDateTime> > QDateTimeVec;

  unsigned int  m_point;
  int           m_step;
  bool         h_enabled;
  unsigned int h_point;
  unsigned int h_size;
  bool         h_lock;
  int          h_step;
//  bool         first_minset;
//  bool         first_maxset;

  std::deque<int> m_tabsVisited;

private slots:
  void makePlot(); 
  void makeCorrelationPlot(); 
  void clear();
  void filterChanged(const QString& text);
  void applyFilter(const QString& text);
  void toggleFilter(int state); 
  void toggleHide(int state);
  void isTabClicked(int currentTab);

};

#endif  // define TRP_TABLEPAGE_H

