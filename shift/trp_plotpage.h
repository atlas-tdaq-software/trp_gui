/** @file: trp_plotpage.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_PlotPage: Page Containing TRP plots
 */

#ifndef TRP_PLOTPAGE_H
#define TRP_PLOTPAGE_H

#include <deque>

#include <QWidget>

class QGridLayout;
class TRP_Plot;

class TRP_PlotPage : public QWidget {

  Q_OBJECT

public:
  TRP_PlotPage(QWidget *parent = 0);
  virtual ~TRP_PlotPage();

private:
  TRP_PlotPage(const TRP_PlotPage &);
  TRP_PlotPage& operator = (const TRP_PlotPage &);

public:
  void addPlot(TRP_Plot *new_plot);
  void formatPage();

  std::deque<TRP_Plot*>::const_iterator begin() const {
    return plots.begin();
  }
  std::deque<TRP_Plot*>::const_iterator end() const {
    return plots.end();
  }

private:
  std::deque<TRP_Plot*> plots;
  QGridLayout *grid;

};

#endif  // define TRP_PLOTPAGE_H

