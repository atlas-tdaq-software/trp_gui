#include <QPointF>
#include <owl/time.h>
#include <stdlib.h>

#include <iostream>

#include <is/inforeceiver.h>
#include <is/callbackinfo.h>

#include <is/infoT.h>
#include <is/info.h>

#include "trp_isthread.h"
#include "trp_unique_timepoints.h"
#include <TRP/TimePoint_IS.h>

#include "trp_tpdata.h"

#include <map>
#include <exception>

TRP_ISThread::TRP_ISThread(QString thread_name,
                           QString thread_alias,
                           QString thread_unit,
                           QString server_name,
                           unsigned int history_size,
                           std::string partitionName,
                           TRP_TPDataPtr tpdata,
                           QObject *parent) : 
  QThread(parent), dictionary(IPCPartition(partitionName)), m_tpdata(tpdata) {
  stop = false;
  name = thread_name;
  alias = thread_alias;
  unit = thread_unit;
  server = server_name;
  full_name = server.toStdString() + "." + name.toStdString();
  n_history = history_size;
  receiver = 0;
  //dictionary = 0; // bug #93742 fix 4 Feb 2013
  

  firstcallback = true;
  
  file = NULL;
  
  // history
  history_enabled = false;
  history_point   = 999;
  history_step    = 0;
  history_diff    = false;
  
  
  connect (this, SIGNAL(gotData()),
           this, SLOT(lockHistoryPoint()));
  
  // disable ROOT signal handlers, which can cause the system to hang
}
  
TRP_ISThread::~TRP_ISThread() {
  delete m_tpdata;
}

void TRP_ISThread::quit() {
  stop = true;
  if (receiver) {
    try {
      receiver->unsubscribe(full_name);
    } catch (...) {}
  }
}

void TRP_ISThread::run() {
  
  // If re-subscribing, make sure points are not duplicated.
  std::vector <TimePoint_IS> tps;
  
  try {
    dictionary.getValues(full_name, tps, n_history);
  } catch (...) {
}
  
  m_mutex.lock();
  if (m_doSummary){std::cout<< m_tpdata<< '\n';} //debug
  auto utps = trp_unique_timepoints(tps);
  m_tpdata->setTimePoints(utps);
  m_mutex.unlock();
  emit gotData();
  
  
  try {
    receiver->subscribe(full_name, &TRP_ISThread::is_callback, this);
  } catch (...) {}
	
  while (!stop) {
    sleep(1);
  }
  
}

void TRP_ISThread::is_callback(ISCallbackInfo *isc) {
  if (isc->reason() == ISInfo::Deleted) return;

  /*
  bool got_point = true;
  TimePoint_IS *time_point = new TimePoint_IS;
  try {
    isc->value(*time_point);
  } catch (...) {
    got_point = false;
  }

  if (!got_point) {
    delete time_point;
    return;
  }
  */


  TimePoint_IS time_point;
  try {
    isc->value(time_point);
  } catch (...) {
    return;
  }

  m_tpdata->addTimePoint(std::make_shared<TimePoint_IS>(time_point));

  if (m_doSummary){std::cout<< m_tpdata<< '\n';} //debug

  emit updateSpinboxLimits(thread_number);
  emit updateHistoryTimeLabel(thread_number);
  
  emit gotData();
}

bool TRP_ISThread::exists() {
  bool exists = false;
  try {
    if (dictionary.contains(full_name) /* || !rootFile_name.isEmpty()  */) {
      exists = true;
    }
  } catch (...) {}
  return exists;
}


void TRP_ISThread::updateHistoryDiff(int state) {
  m_tpdata->updateHistoryDiff(state);
}


void TRP_ISThread::lockHistoryPoint() {
  m_tpdata->lockHistoryPoint();
}

TRP_TPDataPtr TRP_ISThread::getTimePointData() const noexcept{
  return m_tpdata;
}
