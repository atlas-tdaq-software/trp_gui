/** @file trp_preview.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Preview: preview of a TRP_Plot (for saving image)
 */

#ifndef TRP_PREVIEW_H
#define TRP_PREVIEW_H

#include <QDialog>
#include "trp_trend.h"
#include "trp_preview.h"
#include "trp_plot.h"

class QFrame;
class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;
class QGroupBox;

class TRP_Preview : public QDialog {
  Q_OBJECT

  friend class TRP_Plot;

public:
  TRP_Preview(QPixmap p, 
              std::deque<TRP_Trend*> trends, 
              QFont legend_font, QFont title_font, 
	      TRP_Plot::plotType selectedPlotType,
              QWidget *parent = 0);

private:
  TRP_Preview(const TRP_Preview &);
  TRP_Preview& operator = (const TRP_Preview &);

public slots:
  void save();

protected:
  void paintEvent(QPaintEvent *event);

private:

  TRP_Plot::plotType plotTypeSelected;

  QPixmap pixmap;

  QFrame *frame;
  QGroupBox *legend_box;

  QHBoxLayout *hbox;
  QVBoxLayout *layout;
  QGridLayout *legend_grid;

  QRect save_rect;
};

#endif  // define TRP_PREVIEW_H

