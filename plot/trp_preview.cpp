#include <iostream>
#include <stdlib.h>

#include <QtGui>
#include <QtWidgets>

#include "trp_preview.h"

TRP_Preview::TRP_Preview(QPixmap p, 
                         std::deque<TRP_Trend*> trends, 
                         QFont legend_font, QFont title_font, 
			 TRP_Plot::plotType selectedPlotType,
                         QWidget *parent) : QDialog(parent) {

  setAttribute(Qt::WA_DeleteOnClose);
  setWindowTitle("Preview");
  pixmap = p;

  plotTypeSelected = selectedPlotType;

  // Frame for pixmap:

  frame = new QFrame;
  frame->setMinimumSize(QSize(pixmap.width(), pixmap.height()));
  frame->setFrameStyle(QFrame::NoFrame);

  // Legend box:

  legend_grid = new QGridLayout;
  legend_grid->setVerticalSpacing(0);

  legend_box = new QGroupBox("Legend");
  legend_box->setFont(title_font);
  legend_box->setContentsMargins(4, 15, 4, 1);
  legend_box->setLayout(legend_grid);

  //if (plotTypeSelected==TRP_Plot::correlation) legend_box->hide();

  std::deque<QHBoxLayout*> legend_layouts;

  int max_width = 0;
  int i_count=0;
  std::deque<TRP_Trend*>::const_iterator i_trend;
  for (i_trend=trends.begin(); i_trend!=trends.end(); ++i_trend) {
    QHBoxLayout *legend_layout = new QHBoxLayout;  
    legend_layout->setSpacing(6);

    QLabel *line = new QLabel("");
    line->setPalette(QPalette((*i_trend)->getColor()));
    line->setAutoFillBackground(true);
    line->setMaximumSize(QSize(10, 2));
    legend_layout->addWidget(line);

    QLabel *label = new QLabel((*i_trend)->getFullName());
    label->setFont(legend_font);
    label->adjustSize();
    legend_layout->addWidget(label);

    ++i_count;
    if (plotTypeSelected==TRP_Plot::standard){
      legend_layouts.push_back(legend_layout);
    }
    else if (plotTypeSelected==TRP_Plot::correlation && i_count>1){
      legend_layouts.push_back(legend_layout);
    }

    if (label->size().width() > max_width) max_width = label->size().width();
  }

  max_width += 25;
  int ncolumns = qRound(float(pixmap.width()/max_width));
  int r(0), c(0);
  for (unsigned int i = 0; i < legend_layouts.size(); i++) {
    legend_grid->addLayout(legend_layouts[i], r, c);
    if (++c >= ncolumns) {
      ++r;
      c = 0;
    }
  }

  if (trends.size() == 0) legend_box->hide();

  // Buttons at bottom:

  hbox = new QHBoxLayout;

  QPushButton *cancel_button = new QPushButton("&Cancel");
  QPushButton *save_button   = new QPushButton("&Save");
  save_button->setDefault(true);

  connect(cancel_button, SIGNAL(clicked()), this, SLOT(close()));
  connect(save_button,   SIGNAL(clicked()), this, SLOT(save()));

  hbox->addStretch();
  hbox->addWidget(cancel_button);
  hbox->addSpacing(5);
  hbox->addWidget(save_button);
  hbox->addStretch();

  // Layout:

  layout = new QVBoxLayout;
  layout->addWidget(frame);
  layout->addWidget(legend_box);
  layout->addSpacing(5);
  layout->addLayout(hbox);
  layout->addSpacing(-5);
  layout->setSizeConstraint(QLayout::SetFixedSize);
  setLayout(layout);

}

void TRP_Preview::paintEvent(QPaintEvent *) {
  QPainter painter(this);

  painter.setPen(Qt::NoPen);
  painter.setBrush(Qt::white);

  int bottom;
  if (legend_box->isHidden()) { 
    bottom = frame->geometry().bottom();
  } else {
    bottom = legend_box->geometry().bottom();
  }

  save_rect = QRect(QPoint(frame->geometry().left() - 2,
                           frame->geometry().top() - 2),
                    QPoint(frame->geometry().right() + 2,
                           bottom + 2));
  painter.drawRect(save_rect);

  painter.drawPixmap(frame->geometry(), pixmap);

}

void TRP_Preview::save() {
  QString image_name = QFileDialog::getSaveFileName(this, "Save Image As ...",
    "image.png",
    "Image Files (*.bmp, *.jpg, *.jpeg, *.png, *.ppm, *.tiff, *.xbm, *.xpm)");
  if (image_name.isEmpty()) {
    close();
  } else {
    QPixmap pixmap = QPixmap::grabWidget(this, save_rect);
    QImage image = pixmap.toImage();
    bool saved = image.save(image_name);
    if (saved) {
      close();
    } else {
      QStringList list = image_name.split("/");
      list.removeLast();
      QString dir_name = list.join("/");
      QString message = "The account you are using "; 
      message += QString("(%1) may not have permission ").arg(getenv("USER"));
      message += QString("to save to the directory %1.").arg(dir_name);
      QMessageBox warning_message(QMessageBox::Warning,
                                  QString("Warning"),
                                  "Image could not be saved.",
                                  QMessageBox::Ok,
                                  this);
      warning_message.setInformativeText(message);
      warning_message.exec();
    }
  }
}
