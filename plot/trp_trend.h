/** @file trp_trend.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Trend: A trend class for trp_plot
 */

#ifndef TRP_TREND_H
#define TRP_TREND_H

#include "../shift/trp_typeAliases.h"
#include <deque>
#include <memory>
#include <ostream>
#include <QPointF>

class QColor;
//class TRP_ISThread;

class TRP_Trend {

public:
  TRP_Trend(QString nx, QString ny, TRP_TPDataPtr t);
  virtual ~TRP_Trend();

private:
  TRP_Trend(const TRP_Trend &);
  TRP_Trend& operator = (const TRP_Trend &);

public:
  QString getFullName() const;
  QString getUnitName() const;
  QString getNameX() const {return name_x;}
  QString getNameY() const {return name_y;}
  TRP_TPDataPtr getThread() const {return thread;}

  void setColor(QColor new_color) {color = new_color;}
  QColor getColor() const {return color;}

  // void deletePoints();
  void updatePoints();
  void setPoints(std::deque<std::shared_ptr<QPointF>> new_points);

  std::deque<std::shared_ptr<QPointF>>::const_iterator begin() const {
    return m_points.cbegin();}

  std::deque<std::shared_ptr<QPointF>>::const_iterator end()  const {
    return m_points.cend();}

private:
  bool reads_from_thread;
  TRP_TPDataPtr thread;

  std::deque<std::shared_ptr<QPointF>> m_points;

  QColor color;
  QString name_x, name_y;

};

std::ostream& operator << (std::ostream&, const TRP_Trend&);
#endif  // define TRP_TREND_H

