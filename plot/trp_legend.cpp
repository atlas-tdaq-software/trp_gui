#include <QtGui>
#include <QtWidgets>

#include "trp_legend.h"

#include <iostream>
TRP_Legend::TRP_Legend(QWidget *parent) : 
  QFrame(parent),
  user_def_pos(false)
{
  setPalette(QPalette(QColor(255,255,255,127)));   // 50% transparent
  setAutoFillBackground(true);

  legend_layout = new QGridLayout;
  legend_layout->setVerticalSpacing(0);
  legend_layout->setContentsMargins(0,0,0,0);
  setLayout(legend_layout);

  connect(parent, SIGNAL(marginsChanged(int,int,int,int)),
          this, SLOT(setMargins(int,int,int,int)));
}

TRP_Legend::~TRP_Legend() {
}

void TRP_Legend::addTrend(TRP_Trend *new_trend)
{
  if (!new_trend) return;

  int row = legend_layout->rowCount();
  QLabel *line = new QLabel("");
  line->setPalette(QPalette(new_trend->getColor()));
  line->setAutoFillBackground(true);
  line->setMinimumWidth(10);
  line->setMaximumSize(QSize(10, 2));
  legend_layout->addWidget(line, row, 0);
  
  QLabel *label = new QLabel(new_trend->getNameX()+", " + new_trend->getNameY());
  legend_layout->addWidget(label, row, 1);
  adjustSize();
}

void TRP_Legend::setMargins(int left, int /*right*/, int top, int /*bottom*/)
{
  if (not user_def_pos) move(left+4, top+2);
}

void TRP_Legend::enterEvent (QEvent*)
{
  setCursor(Qt::OpenHandCursor);
}

void TRP_Legend::leaveEvent (QEvent*)
{
  unsetCursor();
}


void TRP_Legend::mousePressEvent( QMouseEvent *event )
{
  clickPos = event->pos();
}

void TRP_Legend::mouseMoveEvent(QMouseEvent *event)
{
  user_def_pos = true;
  move( mapToParent(event->pos()) - clickPos );
}
