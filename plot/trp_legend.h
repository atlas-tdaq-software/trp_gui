/** @file trp_legend.h
 *  @author Frank Winklmeier
 *  @brief TRP_Legend: Legend for the plots
 */

#ifndef TRP_LEGEND_H
#define TRP_LEGEND_H

#include <deque>
#include <QFrame>
#include "trp_trend.h"

class QGridLayout;

class TRP_Legend : public QFrame {
  Q_OBJECT
  
public:
  TRP_Legend(QWidget *parent = 0);
  virtual ~TRP_Legend();

private: 
  TRP_Legend(const TRP_Legend &);
  TRP_Legend& operator = (const TRP_Legend &);

public:
  void addTrend(TRP_Trend *new_trend);

public slots:
  void setMargins(int left, int right, int top, int bottom);  
  
protected:
  void enterEvent (QEvent * event);
  void leaveEvent (QEvent * event);
  void mousePressEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);

private:
  QGridLayout* legend_layout;
  QPoint       clickPos;
  bool         user_def_pos;
};

#endif  // define TRP_LEGEND_H

