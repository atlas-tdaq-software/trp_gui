#include <iostream>
#include <stdlib.h>
#include <cmath>

#include<QtGui>
#include <QtWidgets>

#include <owl/time.h>
#include "trp_utility.h"
#include "trp_preview.h"
#include "trp_plot.h"
#include "trp_legend.h"

TRP_Plot::TRP_Plot(QWidget *parent, TRP_Plot::plotType selectedPlotType, TRP_Plot::dataSource selectedDataSource) : QWidget(parent) {
//TRP_Plot::TRP_Plot(QWidget *parent) : QWidget(parent) {
  srand(1234);  // for trend colours

  plotTypeSelected = selectedPlotType;
  dataSourceSelected = selectedDataSource;

  setAttribute(Qt::WA_DeleteOnClose);

  setPalette(QPalette(Qt::white));
  setAutoFillBackground(true);
  
  optShowLegend = true;
  optShowGrid = false;
  optLogX = false;
  optLogY = false;

  if (plotTypeSelected==standard ){
    x_min = 0.;
    x_max = 90.;
    y_min = 0.;
    y_max = 20.;

    x_label = "Time";
    y_label = "Rate (Hz)";
    
    optTimeX = true;
    optTimeY = false;

  }
  else if (plotTypeSelected==correlation){
    
   x_min = 0.;
   x_max = 10.;
   y_min = 0.;
   y_max = 10.;
   
   x_label = "";
   y_label = "Rate (Hz)";
   
   optTimeX = false;
   optTimeY = false;

  }
  
  ensureRangeSanity();

  legend_box = new TRP_Legend(this);
  if (!optShowLegend)   legend_box->hide();   

  optTextSize = 1;
  setTextSizes();

  title = "";
  top_margin = raw_top_margin;
  
  info_box = new QLabel(this);
  info_box->setAutoFillBackground(true);
  info_box->setPalette(QPalette(QColor(255,255,224)));
  info_box->setFrameStyle(QFrame::StyledPanel);
  info_box->hide();

  defaultMinX = true;
  defaultMaxX = true;
  defaultMinY = true;
  defaultMaxY = true;
  changingRangeX = false;
  changingRangeY = false;
  selectingRegion = false;
  
  // history
  optDiffStep = -2;

  createActionsAndMenus();
}

TRP_Plot::~TRP_Plot() {

  unsigned int count=0;
  std::deque<TRP_Trend*>::const_iterator it;
  for (it = trends.begin(); it != trends.end(); ++it) {
    count++;
    delete *it;
  }
  trends.clear();
  plotTypeSelected=standard;
}

void TRP_Plot::createActionsAndMenus() {
  /*** Create Actions ***/

  // Save image 

  QAction *saveImageAction = new QAction("&Save Image", this);
  connect(saveImageAction, SIGNAL(triggered()), 
          this, SLOT(saveImage()));

  // Show legend
  showLegendAction = new QAction("Show L&egend", this);
  showLegendAction->setCheckable(true);
  showLegendAction->setChecked(optShowLegend);
  connect(showLegendAction, SIGNAL(toggled(bool)), 
          this, SLOT(showLegend(bool)));
  
  // Show grid

  showGridAction = new QAction("Show &Grid", this);
  showGridAction->setCheckable(true);
  showGridAction->setChecked(optShowGrid);
  connect(showGridAction, SIGNAL(toggled(bool)), 
          this, SLOT(showGrid(bool)));

  // Set Log-Y

  setLogYAction = new QAction("Set &Log Y", this);
  setLogYAction->setCheckable(true);
  setLogYAction->setChecked(optLogY);
  connect(setLogYAction, SIGNAL(toggled(bool)), 
          this, SLOT(setLogY(bool)));

  // Use Default Ranges for Axes

  setDefaultRangeAction = new QAction("&Unzoom", this);
  connect(setDefaultRangeAction, SIGNAL(triggered(bool)), 
          this, SLOT(setDefaultRanges()));

  // Set Text Size

  setTextSize1Action = new QAction("&1", this);
  setTextSize1Action->setCheckable(true);
  connect(setTextSize1Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize1()));

  setTextSize2Action = new QAction("&2", this);
  setTextSize2Action->setCheckable(true);
  connect(setTextSize2Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize2()));

  setTextSize3Action = new QAction("&3", this);
  setTextSize3Action->setCheckable(true);
  connect(setTextSize3Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize3()));

  setTextSize4Action = new QAction("&4", this);
  setTextSize4Action->setCheckable(true);
  connect(setTextSize4Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize4()));

  setTextSize5Action = new QAction("&5", this);
  setTextSize5Action->setCheckable(true);
  connect(setTextSize5Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize5()));

  setTextSize6Action = new QAction("&6", this);
  setTextSize6Action->setCheckable(true);
  connect(setTextSize6Action, SIGNAL(triggered()), 
          this, SLOT(setTextSize6()));

  if (optTextSize == 1) {
    setTextSize1Action->setChecked(true);
  } else if (optTextSize == 2) {
    setTextSize2Action->setChecked(true);
  } else if (optTextSize == 3) {
    setTextSize3Action->setChecked(true);
  } else if (optTextSize == 4) {
    setTextSize4Action->setChecked(true);
  } else if (optTextSize == 5) {
    setTextSize5Action->setChecked(true);
  } else if (optTextSize == 6) {
    setTextSize6Action->setChecked(true);
  } 

  QActionGroup *setTextSizeGroup = new QActionGroup(this);
  setTextSizeGroup->addAction(setTextSize1Action);
  setTextSizeGroup->addAction(setTextSize2Action);
  setTextSizeGroup->addAction(setTextSize3Action);
  setTextSizeGroup->addAction(setTextSize4Action);
  setTextSizeGroup->addAction(setTextSize5Action);
  setTextSizeGroup->addAction(setTextSize6Action);

  // Clone

  QAction *cloneAction = new QAction("&Clone", this);
  connect(cloneAction, SIGNAL(triggered()), 
          this, SIGNAL(cloneRequest()));

  // history

  setDiffStep1Action = new QAction("-2", this);
  setDiffStep1Action->setCheckable(true);
  connect(setDiffStep1Action, SIGNAL(triggered()), 
          this, SLOT(setDiffStep1()));

  setDiffStep2Action = new QAction("-1", this);
  setDiffStep2Action->setCheckable(true);
  connect(setDiffStep2Action, SIGNAL(triggered()), 
          this, SLOT(setDiffStep2()));

  setDiffStep3Action = new QAction("0", this);
  setDiffStep3Action->setCheckable(true);
  connect(setDiffStep3Action, SIGNAL(triggered()), 
          this, SLOT(setDiffStep3()));

  setDiffStep4Action = new QAction("1", this);
  setDiffStep4Action->setCheckable(true);
  connect(setDiffStep4Action, SIGNAL(triggered()), 
          this, SLOT(setDiffStep4()));

  setDiffStep5Action = new QAction("2", this);
  setDiffStep5Action->setCheckable(true);
  connect(setDiffStep5Action, SIGNAL(triggered()), 
          this, SLOT(setDiffStep5()));

  if (optDiffStep == -2) {
    setDiffStep1Action->setChecked(true);
  } else if (optDiffStep == -1) {
    setDiffStep2Action->setChecked(true);
  } else if (optDiffStep == 0) {
    setDiffStep3Action->setChecked(true);
  } else if (optDiffStep == 1) {
    setDiffStep4Action->setChecked(true);
  } else if (optDiffStep == 2) {
    setDiffStep5Action->setChecked(true);
  } 

  QActionGroup *setDiffStepGroup = new QActionGroup(this);
  setDiffStepGroup->addAction(setDiffStep1Action);
  setDiffStepGroup->addAction(setDiffStep2Action);
  setDiffStepGroup->addAction(setDiffStep3Action);
  setDiffStepGroup->addAction(setDiffStep4Action);
  setDiffStepGroup->addAction(setDiffStep5Action);



  /*** Create Menus ***/

  rightClickMenu = new QMenu(this);
  rightClickMenu->addAction(saveImageAction);
  rightClickMenu->addAction(setDefaultRangeAction);
  rightClickMenu->addAction(showLegendAction);  
  rightClickMenu->addAction(showGridAction);
  rightClickMenu->addAction(setLogYAction);
  QMenu *textSizeMenu = rightClickMenu->addMenu("Set &Text Size");
  textSizeMenu->addAction(setTextSize1Action);
  textSizeMenu->addAction(setTextSize2Action);
  textSizeMenu->addAction(setTextSize3Action);
  textSizeMenu->addAction(setTextSize4Action);
  textSizeMenu->addAction(setTextSize5Action);
  textSizeMenu->addAction(setTextSize6Action);
  rightClickMenu->addAction(cloneAction);
  // history

/*
  QMenu *diffStepMenu = rightClickMenu->addMenu("Set History &Diff Step");
  diffStepMenu->addAction(setDiffStep1Action);
  diffStepMenu->addAction(setDiffStep2Action);
  diffStepMenu->addAction(setDiffStep3Action);
  diffStepMenu->addAction(setDiffStep4Action);
  diffStepMenu->addAction(setDiffStep5Action);
*/
  
}

void TRP_Plot::contextMenuEvent(QContextMenuEvent *event) {
  if (event->reason() == QContextMenuEvent::Mouse) {
    if (changingRangeX || changingRangeY || selectingRegion) {
      changingRangeX = false;
      changingRangeY = false;
      selectingRegion = false;
      update();
    }
    rightClickMenu->exec(QCursor::pos());
  }
} 

void TRP_Plot::saveImage() {

  // Temporarily hide legend
  bool legend = optShowLegend;

  showLegend(false);
  QPixmap pixmap = QPixmap::grabWidget(this);
  //  if (legend) showLegend(true);
  
  TRP_Preview *preview = new TRP_Preview(pixmap, trends, 
                                         axis_value_font, axis_label_font,
					 plotTypeSelected,
                                         this);
  preview->show();
  if (legend) showLegend(true);
}

void TRP_Plot::setTextSizes() {
  if (optTextSize == 1) {
    line_width = 1;
    height_min = 150;
    left_margin   = 48;  right_margin  = 20;
    bottom_margin = 30;  raw_top_margin    = 6;
    tick_length = 2;
    axis_value_font = QFont("helvetica", 7);
    axis_label_font = QFont("helvetica", 9);
    text_rect_size = 14;
  } else if (optTextSize == 2) {
    line_width = 1;
    height_min = 175;
    left_margin   = 54;  right_margin  = 24;
    bottom_margin = 34;  raw_top_margin    = 6;
    tick_length = 2;
    axis_value_font = QFont("helvetica", 8);
    axis_label_font = QFont("helvetica", 10);
    text_rect_size = 15;
  } else if (optTextSize == 3) {
    line_width = 1;
    height_min = 195;
    left_margin   = 64;  right_margin  = 25;
    bottom_margin = 39;  raw_top_margin    = 9;
    tick_length = 3;
    axis_value_font = QFont("helvetica", 9);
    axis_label_font = QFont("helvetica", 11);
    text_rect_size = 17;
  } else if (optTextSize == 4) {
    line_width = 1;
    height_min = 205;
    left_margin   = 66;  right_margin  = 27;
    bottom_margin = 42;  raw_top_margin    = 10;
    tick_length = 3;
    axis_value_font = QFont("helvetica", 10);
    axis_label_font = QFont("helvetica", 12);
    text_rect_size = 19;
  } else if (optTextSize == 5) {
    line_width = 1;
    height_min = 230;
    left_margin   = 74;  right_margin  = 30;
    bottom_margin = 48;  raw_top_margin    = 10;
    tick_length = 3;
    axis_value_font = QFont("helvetica", 11);
    axis_label_font = QFont("helvetica", 13);
    text_rect_size = 21;
  } else if (optTextSize == 6) {
    line_width = 2;
    height_min = 250;
    left_margin   = 80;  right_margin  = 33;
    bottom_margin = 54;  raw_top_margin    = 11;
    tick_length = 3;
    axis_value_font = QFont("helvetica", 12);
    axis_label_font = QFont("helvetica", 14);
    text_rect_size = 24;
  }
  width_min = qRound(float(height_min*phi));
  setMinimumSize(QSize(width_min, height_min));

  legend_box->setFont(axis_value_font);
  legend_box->adjustSize();
  emit marginsChanged(left_margin, right_margin, top_margin, bottom_margin);
}

void TRP_Plot::setRangeX(qreal min, qreal max) {
  if (x_min == min && x_max == min) return;
  x_min = min;
  x_max = max;
  ensureRangeSanity();
  update();
}

void TRP_Plot::setRangeY(qreal min, qreal max) {
  if (y_min == min && y_max == max) return;
  y_min = min;
  y_max = max;
  ensureRangeSanity();
  update();
}

void TRP_Plot::ensureRangeSanity() {
  // Ensure that axis maximum is greater than axis minimum.
  if (x_min > x_max) {
    qreal true_min = x_max;
    qreal true_max = x_min;
    x_min = true_min;
    x_max = true_max;
  } 
  if (y_min > y_max) {
    qreal true_min = y_max;
    qreal true_max = y_min;
    y_min = true_min;
    y_max = true_max;
  } 
  // Ensure that log axis > 0
  if (optLogX) {
    if (x_min <= 0) {
      x_min = 0.01;
    }
    if (x_max < x_min) {
      x_max = 1;
    }
  }
  if (optLogY) {
    if (y_min <= 0) {
      y_min = 0.01;
    }
    if (y_max < y_min) {
      y_max = 1;
    }
  }
  // Expand axis if too small
  if (!optLogX && !optTimeX) {
    if ((x_min > 0. && ((x_max - x_min) / x_max) <  0.05) ||
        (x_max < 0. && ((x_max - x_min) / x_min) > -0.05)) {
      qreal new_x_max(0), new_x_min(0);
      if (x_min > 0) {
        new_x_max = (x_max + x_min) / (2. - 0.1);
        new_x_min = new_x_max * (1. - 0.1);
      } else if (x_max < 0) {
        new_x_min = (x_max + x_min) / (2. - 0.1);
        new_x_max = new_x_min * (1. - 0.1);
      }
      x_max = new_x_max;
      x_min = new_x_min;
    }
  }
  if (!optLogY && !optTimeY) {
    if ((y_min > 0. && ((y_max - y_min) / y_max) <  0.05) ||
        (y_max < 0. && ((y_max - y_min) / y_min) > -0.05)) {
      qreal new_y_max(0), new_y_min(0);
      if (y_min > 0) {
        new_y_max = (y_max + y_min) / (2. - 0.1);
        new_y_min = new_y_max * (1. - 0.1);
      } else if (y_max < 0) {
        new_y_min = (y_max + y_min) / (2. - 0.1);
        new_y_max = new_y_min * (1. - 0.1);
      }
      y_max = new_y_max;
      y_min = new_y_min;
    }
  }
}

void TRP_Plot::setTitle(QString string) {
  if (string == title) return;
  title = string;
  if (title == "") { // Make space for title at top if necessary
    top_margin = raw_top_margin;
  } else {
    top_margin = raw_top_margin + text_rect_size;
  }
  emit marginsChanged(left_margin, right_margin, top_margin, bottom_margin);  
  update();
}

void TRP_Plot::setLabelX(QString string) {
  if (string == x_label) return;
  x_label = string;
  update();
}

void TRP_Plot::setLabelY(QString string) {
  if (string == y_label) return;
  y_label = string;
  update();
}

void TRP_Plot::setLabels(QString x_string, QString y_string) {
  if (x_string == x_label && y_string == y_label) return;
  x_label = x_string;
  y_label = y_string;
  update();
}

/* ------------------------------------------------------------------- */
/* Functions to Set Options                                            */
/* ------------------------------------------------------------------- */

void TRP_Plot::showLegend(bool opt) {
  if (opt == true) {
    showLegendAction->setChecked(true);
    optShowLegend = true;
    legend_box->setVisible(optShowLegend);
    update();
  } else {
    showLegendAction->setChecked(false);
    optShowLegend = false;
    legend_box->setVisible(optShowLegend);
    update();
  }
}


void TRP_Plot::showGrid(bool opt) {
  if (opt == true) {
    if (optShowGrid) return;
    if (showGridAction->isChecked() == false) showGridAction->setChecked(true);
    optShowGrid = true;
    update();
  } else {
    if (!optShowGrid) return;
    if (showGridAction->isChecked() == true) showGridAction->setChecked(false);
    optShowGrid = false;
    update();
  }
}

void TRP_Plot::setTextSize(unsigned int size) {
  if (size == optTextSize) return;
  if (size > 6) return;
  optTextSize = size;
  setTextSizes();
  if (optTextSize == 1) {
    if (setTextSize1Action->isChecked() == false) {
      setTextSize1Action->setChecked(true);
    }
  } else if (optTextSize == 2) {
    if (setTextSize2Action->isChecked() == false) {
      setTextSize2Action->setChecked(true);
    }
  } else if (optTextSize == 3) {
    if (setTextSize3Action->isChecked() == false) {
      setTextSize3Action->setChecked(true);
    }
  } else if (optTextSize == 4) {
    if (setTextSize4Action->isChecked() == false) {
      setTextSize4Action->setChecked(true);
    }
  } else if (optTextSize == 5) {
    if (setTextSize5Action->isChecked() == false) {
      setTextSize5Action->setChecked(true);
    }
  } else if (optTextSize == 6) {
    if (setTextSize6Action->isChecked() == false) {
      setTextSize6Action->setChecked(true);
    }
  } 
  emit textSizeChanged();
  update();
}

void TRP_Plot::setDefaultRanges() {
  if (!(defaultMinX && defaultMaxX && defaultMinY && defaultMaxY)) {
    defaultMinX = true;
    defaultMaxX = true;
    defaultMinY = true;
    defaultMaxY = true;
    update();
  }
}

void TRP_Plot::setLogX(bool opt) {
  if (opt == true) {
    if (optLogX) return;
    optLogX = true;
    ensureRangeSanity();
    update();
  } else {
    if (!optLogX) return;
    optLogX = false;
    ensureRangeSanity();
    update();
  }
}

void TRP_Plot::setLogY(bool opt) {
  if (opt == true) {
    if (optLogY) return;
    if (setLogYAction->isChecked() == false) setLogYAction->setChecked(true);
    optLogY = true;
    ensureRangeSanity();
    update();
  } else {
    if (!optLogY) return;
    if (setLogYAction->isChecked() == true) setLogYAction->setChecked(false);
    optLogY = false;
    ensureRangeSanity();
    update();
  }
}

void TRP_Plot::setTimeX(bool opt) {
  if (opt == true) {
    if (optTimeX) return;
    optTimeX = true;
    update();
  } else {
    if (!optTimeX) return;
    optTimeX = false;
    update();
  }
}

void TRP_Plot::setTimeY(bool opt) {
  if (opt == true) {
    if (optTimeY) return;
    optTimeY = true;
    update();
  } else {
    if (!optTimeY) return;
    optTimeY = false;
    update();
  }
}

/* ------------------------------------------------------------------- */
/* Functions Related to Painting                                       */
/* ------------------------------------------------------------------- */

void TRP_Plot::paintEvent(QPaintEvent *) {

  QPainter painter(this);

  if (plotTypeSelected==standard){
    checkAxes();
    checkAxesLabels();
    drawAxes(painter);
    drawAxisValues(painter, "x");
    drawAxisValues(painter, "y");
    drawTitle(painter);
    drawPoints(painter);
  }
  
  if (plotTypeSelected==correlation){


    checkCorrelationAxes();
    drawAxes(painter);
    drawAxisValues(painter, "x");
    drawAxisValues(painter, "y");
    checkCorrelationAxesLabels();
    drawTitle(painter);
    drawCorrelationPoints(painter);
  }

  if (changingRangeX || changingRangeY) {
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0,245,255,50));
    QRect rect;
    if (changingRangeX &&
        ((x_i > left_margin && x_i < width()-right_margin) ||
         (x_f > left_margin && x_f < width()-right_margin))) {
      int x1, x2;
      if (x_i < x_f) {
        x1 = x_i;
        x2 = x_f;
      } else {
        x1 = x_f;
        x2 = x_i;
      }
      if (x1 < left_margin) x1 = left_margin;
      if (x2 > width()-right_margin) x2 = width()-right_margin;
      rect.setCoords (x1, top_margin, x2, height()-bottom_margin);
    } else if (changingRangeY &&
               ((y_i > top_margin && y_i < height()-bottom_margin) ||
                (y_f > top_margin && y_f < height()-bottom_margin))) {
      int y1, y2;
      if (y_i < y_f) {
        y1 = y_i;
        y2 = y_f;
      } else {
        y1 = y_f;
        y2 = y_i;
      }
      if (y1 < top_margin) y1 = top_margin;
      if (y2 > height()-bottom_margin) y2 = height()-bottom_margin;
      rect.setCoords (left_margin, y1, width()-right_margin, y2);
    }
    painter.drawRect(rect);
  } else if (selectingRegion) {
    QPen pen = QPen(Qt::black, line_width, Qt::DotLine);
    painter.setPen(pen);
    painter.setBrush(Qt::NoBrush);
    QRect rect;
    if (x_i < x_f) {
      if (y_i < y_f) {
        rect.setCoords (x_i, y_i, x_f, y_f);
      } else {
        rect.setCoords (x_i, y_f, x_f, y_i);
      }
    } else {
      if (y_i < y_f) {
        rect.setCoords (x_f, y_i, x_i, y_f);
      } else {
        rect.setCoords (x_f, y_f, x_i, y_i);
      }
    }
    painter.drawRect(rect);
  }
}

void TRP_Plot::checkAxes() {


  if (trends.size() == 0) return;
  if (!(defaultMinX || defaultMaxX || defaultMinY || defaultMaxY)) return;

  qreal xmin_data(0), xmax_data(0), ymin_data(0), ymax_data(0);
  qreal xmin_plot(0), xmax_plot(0), ymin_plot(0), ymax_plot(0);

  bool first_point = true;
  std::deque<TRP_Trend*>::const_iterator i_trend;
  for (i_trend=trends.begin(); i_trend!=trends.end(); ++i_trend) {
    std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;
    for (i_point=(*i_trend)->begin(); i_point!=(*i_trend)->end(); ++i_point) {
      if (first_point) {
        xmax_data = (*i_point)->x();
        xmin_data = (*i_point)->x();
        ymax_data = (*i_point)->y();
        ymin_data = (*i_point)->y();
        first_point = false;
      } else {
        if (!optLogX || (*i_point)->x() > 0) {
          if ((*i_point)->x() > xmax_data) xmax_data = (*i_point)->x();
          if ((*i_point)->x() < xmin_data) xmin_data = (*i_point)->x();
        }
        if (!optLogY || (*i_point)->y() > 0) {
          if ((*i_point)->y() > ymax_data) ymax_data = (*i_point)->y();
          if ((*i_point)->y() < ymin_data) ymin_data = (*i_point)->y();
        }
      }
    }
  }
  if (first_point) return; // no points read
  
  if (xmax_data == xmin_data) {
    if (optTimeX) {
      xmax_plot = xmax_data;
      xmin_plot = xmin_data - 3;
    } else {
      if (xmax_data == 0.) {
        xmax_plot =  1.0;
        xmin_plot = -1.0;
      } else {
        xmax_plot = (xmax_data > 0) ? 1.05*xmax_data : 0.95*xmax_data;
        xmin_plot = (xmin_data > 0) ? 0.95*xmin_data : 1.05*xmin_data;
      }
    }
  } else {
    if (optLogX) {
      xmax_plot = 1.10*xmax_data; 
      xmin_plot = 0.95*xmin_data; 
    } else if (optTimeX) {
      xmax_plot = xmax_data;
      xmin_plot = xmin_data;
      if (xmax_plot - xmin_plot < 3) {
        xmin_plot = xmax_plot - 3;
      }
    } else {
      xmax_plot = xmax_data + 0.05*(xmax_data - xmin_data);
      xmin_plot = xmin_data - 0.05*(xmax_data - xmin_data);
    }
  }

  if (ymax_data == ymin_data) {
    if (optTimeY) {
      ymax_plot = ymax_data;
      ymin_plot = ymin_data - 3;
    } else {
      if (ymax_data == 0.) {
        ymax_plot =  1.0;
        ymin_plot = -1.0;
      } else {
        ymax_plot = (ymax_data > 0) ? 1.05*ymax_data : 0.95*ymax_data;
        ymin_plot = (ymin_data > 0) ? 0.95*ymin_data : 1.05*ymin_data;
      }
    }
  } else {
    if (optLogY) {
      ymax_plot = 1.10*ymax_data; 
      ymin_plot = 0.95*ymin_data; 
    } else if (optTimeY) {
      ymax_plot = ymax_data;
      ymin_plot = ymin_data;
      if (ymax_plot - ymin_plot < 3) {
        ymin_plot = ymax_plot - 3;
      }
    } else {
      ymax_plot = ymax_data + 0.05*(ymax_data - ymin_data);
      ymin_plot = ymin_data - 0.05*(ymax_data - ymin_data);
    }
  }

  // Plot at most 48 hrs of data; if more is available, restrict axis range.
  const qreal max_duration = 172800.;

  // -- Restrict maximal range if necessariy
  if (optTimeX && (xmax_plot - xmin_plot) > max_duration) {
    xmin_plot = xmax_plot - max_duration;
  }

  if (optTimeY && (ymax_plot - ymin_plot) > max_duration) {
    ymin_plot = ymax_plot - max_duration;
  }

  // -- Determine whether to use default (restricted) range
  if (optTimeX && (xmax_plot - x_min)     > max_duration) {
    defaultMinX |= defaultMaxX;
  }

  if (optTimeY && (ymax_plot - y_min)     > max_duration) {
    defaultMinY |= defaultMaxY;
  }

  if (defaultMinX) x_min = xmin_plot;
  if (defaultMaxX) x_max = xmax_plot;
  if (defaultMinY) y_min = ymin_plot;
  if (defaultMaxY) y_max = ymax_plot;
  ensureRangeSanity();
}

void TRP_Plot::checkAxesLabels() {

  if (trends.size() == 0) return;
  QString unitName="Rate(Hz)";
  QString fullName="";
  QString nameX="";
  QString nameY="";

  std::deque<TRP_Trend*>::const_iterator t;
  t = trends.begin();

  unitName = (*t)->getUnitName();
  fullName = (*t)->getFullName();
  nameX    = (*t)->getNameX();
  nameY    = (*t)->getNameY();

  if (nameY=="DT") unitName = ""; //Deadtime fraction no units

  if (unitName=="Hz"){
    y_label = "Rate ("+unitName+")";
  }
  else y_label = unitName;

  setLabels(x_label, y_label);
  optShowLegend = true;
}

void TRP_Plot::checkCorrelationAxesLabels() {

  QString unitName="Rate(Hz)";
  QString fullName="";
  QString nameX="";
  QString nameY="";

  //check for when running over ntuple.
  if (trends.size()<2) return;

  std::deque<TRP_Trend*>::const_iterator t;
  t = trends.begin();

  unitName = (*t)->getUnitName();
  fullName = (*t)->getFullName();
  nameX    = (*t)->getNameX();
  nameY    = (*t)->getNameY();

  if (nameY=="DT")    x_label = nameX+nameY; //Deadtime fraction no units
  else x_label = nameX+nameY+"("+unitName+")";

  ++t;
  unitName = (*t)->getUnitName();
  fullName = (*t)->getFullName();
  nameX    = (*t)->getNameX();
  nameY    = (*t)->getNameY();

  bool yAxesSameUnits=true;

  //check that all y-axis trends have same units
  for (; t != trends.end(); ++t) {
    if (unitName != (*t)->getUnitName()) yAxesSameUnits=false;
  }
    
  if (yAxesSameUnits){
    if (nameY=="DT")    y_label = ""; //Deadtime fraction no units
    if (unitName=="Hz") y_label = "Rate(Hz)";
    else y_label = "("+unitName+")";
  }
  else{
    y_label = "";
  }
  optShowLegend = true;
  setLabels(x_label, y_label);
}


void TRP_Plot::checkCorrelationAxes() {

  if (!(defaultMinX || defaultMaxX || defaultMinY || defaultMaxY)) return;

  qreal xmin_data(0), xmax_data(0), ymin_data(0), ymax_data(0);
  qreal xmin_plot(0), xmax_plot(0), ymin_plot(0), ymax_plot(0);

  bool first_point = true;
  std::deque<TRP_Trend*>::const_iterator i_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;

  i_trend=trends.begin();  //first trend defines x-axis

  for (i_point=(*i_trend)->begin(); i_point!=(*i_trend)->end(); ++i_point) {

    if (first_point) {      
      if ((*i_point)->y()<0)continue;
      xmax_data = (*i_point)->y();
      xmin_data = (*i_point)->y();

      first_point = false;
    } else {
      if (!optLogX || (*i_point)->y() > 0) {
	if ((*i_point)->y() > xmax_data) xmax_data = (*i_point)->y();
	if ((*i_point)->y() < xmin_data) xmin_data = (*i_point)->y();
      }
    }
  }
  
  std::deque<TRP_Trend*>::const_iterator j_trend;
  j_trend=trends.begin();++j_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator j_point;

  first_point=true;     
  for (; j_trend!=trends.end(); ++j_trend) {
    for (j_point=(*j_trend)->begin(); j_point!=(*j_trend)->end(); ++j_point) {
      
      if (first_point) {      
	if ((*j_point)->y()<0)continue;
	ymax_data = (*j_point)->y();
	ymin_data = (*j_point)->y();
	
	first_point = false;
      } else {
	if (!optLogY || (*j_point)->y() > 0) {
	  if ((*j_point)->y() > ymax_data) ymax_data = (*j_point)->y();
	  if ((*j_point)->y() < ymin_data) ymin_data = (*j_point)->y();
	}
      }
    }
  }
    
  if (first_point) return; // no points read

  if (xmax_data == xmin_data) {

    if (xmax_data == 0.) {
      xmax_plot =  1.0;
      xmin_plot = -1.0;
    } else {
      xmax_plot = (xmax_data > 0) ? 1.05*xmax_data : 0.95*xmax_data;
      xmin_plot = (xmin_data > 0) ? 0.95*xmin_data : 1.05*xmin_data;
    }
  } else {
    if (optLogX) {
      xmax_plot = 1.10*xmax_data; 
      xmin_plot = 0.95*xmin_data; 
    } else {
      xmax_plot = xmax_data + 0.05*(xmax_data - xmin_data);
      xmin_plot = xmin_data - 0.05*(xmax_data - xmin_data);
    }
  }

  if (ymax_data == ymin_data) {
    if (ymax_data == 0.) {
      ymax_plot =  1.0;
      ymin_plot = -1.0;
    } else {
      ymax_plot = (ymax_data > 0) ? 1.05*ymax_data : 0.95*ymax_data;
      ymin_plot = (ymin_data > 0) ? 0.95*ymin_data : 1.05*ymin_data;
    }
  } else {
    if (optLogY) {
      ymax_plot = 1.10*ymax_data; 
      ymin_plot = 0.95*ymin_data; 
    } else {
      ymax_plot = ymax_data + 0.05*(ymax_data - ymin_data);
      ymin_plot = ymin_data - 0.05*(ymax_data - ymin_data);
    }
  }


  if (defaultMinX) x_min = xmin_plot;
  if (defaultMaxX) x_max = xmax_plot;
  if (defaultMinY) y_min = ymin_plot;
  if (defaultMaxY) y_max = ymax_plot;

  ensureRangeSanity();
}

void TRP_Plot::drawAxes(QPainter &painter) {
  QPen pen = QPen(Qt::black, line_width);
  painter.setPen(pen);

  QLine left_axis   = QLine(left_margin, height()-bottom_margin,
                            left_margin, top_margin);
  QLine right_axis  = QLine(width()-right_margin, height()-bottom_margin,
                            width()-right_margin, top_margin);
  QLine bottom_axis = QLine(left_margin,          height()-bottom_margin,
                            width()-right_margin, height()-bottom_margin);
  QLine top_axis    = QLine(left_margin,          top_margin,
                            width()-right_margin, top_margin);

  painter.drawLine(left_axis);
  painter.drawLine(right_axis);
  painter.drawLine(top_axis);
  painter.drawLine(bottom_axis);
}

void TRP_Plot::drawAxisValues(QPainter &painter, QString axis) {
  QPen pen = QPen(Qt::black, line_width);
  painter.setPen(pen);

  qreal axis_min(0), axis_max(0);
  bool log_axis(false), time_axis(false);
  if (axis == "x") { 
    axis_min = x_min;
    axis_max = x_max;
    if (optLogX) log_axis = true;
    if (optTimeX) time_axis = true;
  } else if (axis == "y") {
    axis_min = y_min;
    axis_max = y_max;
    if (optLogY) log_axis = true;
    if (optTimeY) time_axis = true;
  }
   
  bool log_ticks = false;
  if (log_axis && (log10(axis_max) - log10(axis_min)) > 1.0) {
    log_ticks = true;
  }

  bool time_ticks = false;
  if (time_axis && axis_min > 0.) { 
    time_ticks = true;
  }

  qreal range;
  if (log_ticks) {
   range = log10(axis_max) - log10(axis_min);
  } else {
   range = axis_max - axis_min;
  }

  qreal epsilon = 0.0001 * range;

  std::vector<qreal> major_ticks, minor_ticks;

  const char *axis_value_format(0);
  QString current_x_label = x_label;
  QString current_y_label = y_label;
  int divider = 0;

  if (log_ticks) {  // Log Axis
    // Calculate tick placement
    qreal previous_major_tick = pow(10, floor(log10(axis_min)));
    qreal tick = previous_major_tick;
    int i = 1; 
    while (tick <= axis_max + epsilon) {
      if (tick >= axis_min - epsilon) {
        if (i == 1) {
          major_ticks.push_back(tick);
        } else {
          minor_ticks.push_back(tick);
        }
      }
      i++;
      tick = i*previous_major_tick;
      if (i == 10) {
        previous_major_tick = tick;
        i = 1;
      }
    }

    // Determine axis-value format
    if (axis_max > 10000. || axis_min < 1.) {
      axis_value_format = "%2.0e";
    } else {
      axis_value_format = "%4.0f";
    }
    
  } else if (time_ticks) {  // Time Axis

    // Calculate tick placement
    qreal interval;
    unsigned int nsubdiv;
    if (range < 4.) {             // up to 4 s: 1-s intervals
      interval = 1.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 8.) {      // up to 8 s: 2-s intervals
      interval = 2.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 12.) {     // up to 12 s: 3-s intervals
      interval = 3.;
      nsubdiv = 3;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 20.) {     // up to 20 s: 5-s intervals
      interval = 5.;
      nsubdiv = 5;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 40.) {     // up to 40 s: 10-s intervals 
      interval = 10.;
      nsubdiv = 5;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 80.) {     // up to 80 s: 20-s intervals 
      interval = 20.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 120.) {    // up to 120 s: 30-s intervals 
      interval = 30.;
      nsubdiv = 3;
      axis_value_format = "%.2d:%.2d:%.2d";
    } else if (range < 240.) {    // up to 4 m: 1-m intervals
      interval = 60.;
      nsubdiv = 6;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 480.) {    // up to 8 m: 2-m intervals
      interval = 120.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 720.) {    // up to 12 m: 3-m intervals
      interval = 180.;
      nsubdiv = 3;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 1200.) {   // up to 20 m: 5-m intervals
      interval = 300.;
      nsubdiv = 5;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 2400.) {   // up to 40 m: 10-m intervals
      interval = 600.;
      nsubdiv = 5;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 4800.) {   // up to 80 m: 20-m intervals
      interval = 1200.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 7200.) {   // up to 120 m: 30-m intervals
      interval = 1800.;
      nsubdiv = 3;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 14400.) {  // up to 4 h: 1-h intervals
      interval = 3600.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 28800.) {  // up to 8 h: 2-h intervals
      interval = 7200.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 43200.) {  // up to 12 h: 3-h intervals
      interval = 10800.;
      nsubdiv = 3;
      axis_value_format = "%.2d:%.2d";
    } else if (range < 86400.) {  // up to 24 h: 6-h intervals
      interval = 21600.;
      nsubdiv = 6;
      axis_value_format = "%.2d:%.2d";
    } else {                      // up to 48 h: 12-h (3-h) intervals (ticks)
      interval = 43200.;
      nsubdiv = 4;
      axis_value_format = "%.2d:%.2d";
    }
   
    OWLTime min_time = OWLTime((time_t)axis_min);
    char date_string[100];
    sprintf(date_string,"%d/%d/%d", (int)min_time.day(),
                                    (int)min_time.month(),
                                    (int)min_time.year());


    //OWLDate date(date_string);    // tdaq-05-03-00 and older
    OWLDate date((time_t)axis_min); // fix for tdaq-05-04-00
    qreal tick = (qreal)date.c_time();
    int i = 0;
    while (tick <= axis_max + epsilon) {
      if (tick >= axis_min - epsilon) {
        if (i % nsubdiv == 0) {
          major_ticks.push_back(tick);
        } else {
          minor_ticks.push_back(tick);
        }
      }
      tick += interval/nsubdiv;
      ++i;
    }
    
  } else {  // Regular Axis

    // Calculate tick placement
    qreal range_base;
    int range_exponent;
    toScientificNotation(range, range_base, range_exponent);

    qreal interval, subinterval;
    unsigned int nsubdiv;
    if (range_base < 0.8) {
      interval = 0.1;
      nsubdiv = 4;
    } else if (range_base < 1.6) {
      interval = 0.2;
      nsubdiv = 4;
    } else if (range_base < 4.0) {
      interval = 0.5;
      nsubdiv = 5;
    } else if (range_base < 8.0) {
      interval = 1.;
      nsubdiv = 4;
    } else {
      interval = 2.;
      nsubdiv = 4;
    }
    interval *= pow(10, range_exponent);
    subinterval = interval / nsubdiv;

    qreal first_tick = calculateFirstTick(axis_min, axis_max, interval);
    qreal first_subtick = calculateFirstTick(axis_min, axis_max, subinterval);

    unsigned int npresubtick = 0;  // # of subticks that come before 1st tick
    while (first_subtick + (npresubtick+1)*subinterval <= first_tick) {
      ++npresubtick;
    }

    int i = nsubdiv - npresubtick;
    qreal tick = first_subtick;
    while (tick <= axis_max + epsilon) {
      if (i % nsubdiv == 0) {
        major_ticks.push_back(tick);
      } else {
        minor_ticks.push_back(tick);
      }
      tick += interval/nsubdiv;
      ++i;
    }

    // Determine axis-value format
    // Remove a factor of 10^{3} from printed value if necessary
    qreal interval_base;
    int interval_exponent;
    toScientificNotation(interval, interval_base, interval_exponent);

    divider = interval_exponent / 3;
    QString multiplier;
    multiplier.sprintf(" (x1e%d)", 3*divider);
    if (divider != 0) {
      if (axis == "x") {
        current_x_label += multiplier;
      } else if (axis == "y") {
        current_y_label += multiplier;
      }
    }
    if (interval_exponent - 3*divider >= 0) {
      axis_value_format = "%3.0f";
    } else if (interval_exponent - 3*divider == -1) {
      axis_value_format = "%3.1f";
    } else if (interval_exponent - 3*divider == -2) {
      axis_value_format = "%4.2f";
    } else if (interval_exponent - 3*divider == -3) {
      axis_value_format = "%5.3f";
    }
  }

  // Draw ticks, subticks, grid, and values

  painter.setFont(axis_value_font);

  for (unsigned int i = 0; i < major_ticks.size(); ++i) {
    qreal tick = major_ticks[i];
    int p_tick(0);
    if (axis == "x") { 
      p_tick = plotToPixelX(tick);
    } else if (axis == "y") {
      p_tick = plotToPixelY(tick);
    }

    QString axis_value;
    if (time_ticks) {
      OWLTime tick_time = OWLTime((time_t)tick);
      axis_value.sprintf(axis_value_format, 
                         tick_time.hour(), tick_time.min(), tick_time.sec());
    } else {
      axis_value.sprintf(axis_value_format, tick/pow(10,3*divider));
    }

    int x1(0), y1(0), x2(0), y2(0), alignment(0);
    if (axis == "x") {
      x1 = p_tick - qRound(1.5*text_rect_size);
      x2 = p_tick + qRound(1.5*text_rect_size);
      y1 = height() - bottom_margin + 2*tick_length;
      y2 = height() - bottom_margin + 2*tick_length + text_rect_size;
	    alignment = Qt::AlignHCenter|Qt::AlignVCenter;
    } else if (axis == "y") {
      x1 = 0;
      x2 = left_margin - 3*tick_length - 1;
      y1 = p_tick - qRound(text_rect_size/2.0f);
      y2 = p_tick + qRound(text_rect_size/2.0f);
	    alignment = Qt::AlignRight|Qt::AlignVCenter;
    }
    QPoint top_left(x1, y1);
    QPoint bottom_right(x2, y2);
    QRect value_rect(top_left, bottom_right);
    painter.drawText(value_rect, alignment, axis_value);
      
    if (optShowGrid) {
	    pen.setWidth(1);
	    pen.setStyle(Qt::DotLine);
      painter.setPen(pen);
	    QLine grid;
      if (axis == "x") {
        grid = QLine(p_tick, height()-bottom_margin,
                     p_tick, top_margin);
	    } else if (axis == "y") {
        grid = QLine(left_margin, p_tick, 
                     width()-right_margin, p_tick);
	    }
      painter.drawLine(grid);
	    pen.setStyle(Qt::SolidLine);
	    pen.setWidth(line_width);
      painter.setPen(pen);
    }

    QLine tick1, tick2; 
    if (axis == "x") {
      // bottom tick
      tick1 = QLine(p_tick, height()-bottom_margin,
                    p_tick, height()-bottom_margin+2*tick_length);
      // top tick
      tick2 = QLine(p_tick, top_margin,
                    p_tick, top_margin-2*tick_length);
    } else if (axis == "y") {
      // left tick
      tick1 = QLine(left_margin, p_tick,
                    left_margin-2*tick_length, p_tick);
      // right tick
      tick2 = QLine(width()-right_margin, p_tick,
                    width()-right_margin+2*tick_length, p_tick);
    }
    painter.drawLine(tick1);
    painter.drawLine(tick2);
  }

  for (unsigned int i = 0; i < minor_ticks.size(); ++i) {
    qreal tick = minor_ticks[i];
    int p_tick(0);
    if (axis == "x") { 
      p_tick = plotToPixelX(tick);
    } else if (axis == "y") {
      p_tick = plotToPixelY(tick);
    }

    QLine tick1, tick2; 
    if (axis == "x") {
      // bottom tick
      tick1 = QLine(p_tick, height()-bottom_margin,
                    p_tick, height()-bottom_margin+tick_length);
      // top tick
      tick2 = QLine(p_tick, top_margin,
                    p_tick, top_margin-tick_length);
    } else if (axis == "y") {
      // left tick
      tick1 = QLine(left_margin, p_tick,
                    left_margin-tick_length, p_tick);
      // right tick
      tick2 = QLine(width()-right_margin, p_tick,
                    width()-right_margin+tick_length, p_tick);
    }
    painter.drawLine(tick1);
    painter.drawLine(tick2);
  }

  // Print date on axis if time_axis

  if (time_ticks) {
    painter.setFont(axis_value_font);
    OWLTime axis_time = OWLTime((time_t)major_ticks[0]);
    QString axis_date;
    axis_date.sprintf("%d/%d/%d", (int)axis_time.day(),
                                  (int)axis_time.month(),
                                  (int)axis_time.year());
    int x1, y1, x2, y2;
    x1 = 10;
    x2 = x1 + 4*text_rect_size;
    y1 = height() - text_rect_size - 2;
    y2 = height() - 2;
    QPoint top_left(x1, y1);
    QPoint bottom_right(x2, y2);
    QRect date_rect(top_left, bottom_right);
    painter.drawText(date_rect, Qt::AlignLeft|Qt::AlignVCenter, axis_date);
  }

  if (plotTypeSelected==correlation){
    
    std::deque<TRP_Trend*>::const_iterator t;
    std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;
    
    t = trends.begin();
    i_point=(*t)->begin();

    if (i_point!=(*t)->end()){
    qreal axis_min(0);
    axis_min = (*i_point)->x();
    
    char date_string[100];    
    OWLTime min_time = OWLTime((time_t)axis_min);
    sprintf(date_string,"%d/%d/%d", (int)min_time.day(),
	    (int)min_time.month(),
	    (int)min_time.year());
    
    int x1, y1, x2, y2;
    x1 = 10;
    x2 = x1 + 4*text_rect_size;
    y1 = height() - text_rect_size - 2;
    y2 = height() - 2;
    QPoint top_left(x1, y1);
    QPoint bottom_right(x2, y2);
    QRect date_rect(top_left, bottom_right);
    painter.drawText(date_rect, Qt::AlignLeft|Qt::AlignVCenter, date_string);
    }
  }


  // Draw axis labels
  if ((axis == "x" && current_x_label != "") ||
      (axis == "y" && current_y_label != "")) {
    painter.setFont(axis_label_font);

    int x1(0), y1(0), x2(0), y2(0);
    QString label;
    if (axis == "x") {
      x1 = left_margin;
      x2 = width() - right_margin;
      y1 = height() - text_rect_size - 2;
      y2 = height() - 2;
      label = current_x_label;
    } else if (axis == "y") {
      x1 = bottom_margin;
      x2 = height() - top_margin;
      y1 = 0;
      y2 = text_rect_size;
      label = current_y_label;
    }
    QPoint top_left(x1, y1);
    QPoint bottom_right(x2, y2);
    QRect label_rect(top_left, bottom_right);

    if (axis == "y") {
      painter.save();
      painter.translate(0, height());
      painter.rotate(-90);
    }
    painter.drawText(label_rect, Qt::AlignRight|Qt::AlignVCenter, label);
    if (axis == "y") {
      painter.restore();
    }
  }
}

void TRP_Plot::drawTitle(QPainter &painter) {
  if (title != "") {
    painter.setFont(axis_label_font);
    
    int x1, y1, x2, y2;
    x1 = left_margin;
    x2 = width() - right_margin;
    y1 = 0;
    y2 = text_rect_size;
    QPoint top_left(x1, y1);
    QPoint bottom_right(x2, y2);
    QRect title_rect(top_left, bottom_right);
    painter.drawText(title_rect, Qt::AlignHCenter|Qt::AlignVCenter, title);
  }
}

void TRP_Plot::drawPoints(QPainter &painter) {

  QPen pen = QPen();
  bool inside, inside_old;
  qreal x(0), y(0), x_old(0), y_old(0), x_edge_1(0), y_edge_1(0), x_edge_2(0), y_edge_2(0);
  QPoint point, point_old, point_edge_1, point_edge_2;

  std::deque<TRP_Trend*>::const_iterator i_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;

  for (i_trend=trends.begin(); i_trend!=trends.end(); ++i_trend) {
    pen.setColor((*i_trend)->getColor());
    pen.setWidth(line_width);
    painter.setPen(pen);
    painter.setBrush((*i_trend)->getColor());
    inside = false;
    inside_old = false;
    for (i_point=(*i_trend)->begin(); i_point!=(*i_trend)->end(); ++i_point) {
      x = (*i_point)->x();
      y = (*i_point)->y();
      if (optLogX && (*i_point)->x() <= 0) x = x_min;
      if (optLogY && (*i_point)->y() <= 0) y = y_min;
      if (x >= x_min && x <= x_max && y >= y_min && y <= y_max) {
        inside = true;
      } else {
        inside = false;
      }

      if (inside) point = plotToPixel(QPointF(x, y)); 
      if (inside_old) point_old = plotToPixel(QPointF(x_old, y_old));

      // Draw line if necessary
      if (i_point != (*i_trend)->begin()) {
        if (!inside && !inside_old) {
          if (!((x < x_min && x_old < x_min) ||
                (x > x_max && x_old > x_max) ||
                (y < y_min && y_old < y_min) ||
                (y > y_max && y_old > y_max))){
            if (plotToPixelX(x) == plotToPixelX(x_old)) {
              point_edge_1 = plotToPixel(QPointF(x, y_min));
              point_edge_2 = plotToPixel(QPointF(x, y_max));
              painter.drawLine(point_edge_1, point_edge_2);
            } else if (plotToPixelY(y) == plotToPixelY(y_old)) {
              point_edge_1 = plotToPixel(QPointF(x_min, y));
              point_edge_2 = plotToPixel(QPointF(x_max, y));
              painter.drawLine(point_edge_1, point_edge_2);
            } else {
              double m = (y - y_old) / (x - x_old);
              double b = y_old - x_old * m;
              x_edge_1 = x_min;
              x_edge_2 = x_max;
              y_edge_1 = m * x_min + b;
              y_edge_2 = m * x_max + b;
              if ( (x > x_old && m > 0.) || (x < x_old && m < 0.) ) {
                if (y_edge_1 < y_max && y_edge_2 > y_min) {
                  if (y_edge_2 > y_max) {
                    y_edge_2 = y_max;
                    x_edge_2 = (y_edge_2 - b)/m;
                  }
                  if (y_edge_1 < y_min) {
                    y_edge_1 = y_min;
                    x_edge_1 = (y_edge_1 - b)/m;
                  }
                  point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
                  point_edge_2 = plotToPixel(QPointF(x_edge_2, y_edge_2));
                  painter.drawLine(point_edge_1, point_edge_2);
                } 
              } else {
                if (y_edge_1 > y_min && y_edge_2 < y_max) {
                  if (y_edge_2 < y_min) {
                    y_edge_2 = y_min;
                    x_edge_2 = (y_edge_2 - b)/m;
                  } 
                  if (y_edge_1 > y_max) {
                    y_edge_1 = y_max;
                    x_edge_1 = (y_edge_1 - b)/m;
                  }
                  point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
                  point_edge_2 = plotToPixel(QPointF(x_edge_2, y_edge_2));
                  painter.drawLine(point_edge_1, point_edge_2);
                }
              }
            }
          }
        } else {
          if (inside && inside_old) {
            painter.drawLine(point_old, point);
          } else {
            int quadrant(0);
            if (inside && !inside_old) {
              if      (x < x_old && y < y_old) quadrant = 1;
              else if (x > x_old && y < y_old) quadrant = 2;
              else if (x > x_old && y > y_old) quadrant = 3;
              else if (x < x_old && y > y_old) quadrant = 4;
              else if (x <  x_old && y == y_old) quadrant = -1;
              else if (x == x_old && y <  y_old) quadrant = -2;
              else if (x >  x_old && y == y_old) quadrant = -3;
              else if (x == x_old && y >  y_old) quadrant = -4;
              switch (quadrant) {
                case 1:
                  if ((y_old-y)/(x_old-x) > (y_max-y)/(x_max-x)) {
                    y_edge_1 = y_max;
                    x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
                  } else {
                    x_edge_1 = x_max;
                    y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
                  }
                  break;
                case 2:
                  if ((y_old-y)/(x-x_old) > (y_max-y)/(x-x_min)) {
                    y_edge_1 = y_max;
                    x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
                  } else {
                    x_edge_1 = x_min;
                    y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
                  }
                  break;
                case 3:
                  if ((y-y_old)/(x-x_old) > (y - y_min)/(x-x_min)) {
                    y_edge_1 = y_min;
                    x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
                  } else {
                    x_edge_1 = x_min;
                    y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
                  }
                  break;
                case 4:
                  if ((y-y_old)/(x_old-x) > (y-y_min)/(x_max-x)) {
                    y_edge_1 = y_min;
                    x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
                  } else {
                    x_edge_1 = x_max;
                    y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
                  }
                  break;
                case -1:
                  x_edge_1 = x_max;
                  y_edge_1 = y;
                  break;
                case -2:
                  y_edge_1 = y_max;
                  x_edge_1 = x;
                  break;
                case -3:
                  x_edge_1 = x_min;
                  y_edge_1 = y;
                  break;
                case -4:
                  y_edge_1 = y_min;
                  x_edge_1 = x;
                  break;
              }
              point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
              painter.drawLine(point_edge_1, point);
            } else if (!inside && inside_old) {
              if      (x > x_old && y > y_old) quadrant = 1;
              else if (x < x_old && y > y_old) quadrant = 2;
              else if (x < x_old && y < y_old) quadrant = 3;
              else if (x > x_old && y < y_old) quadrant = 4;
              else if (x >  x_old && y == y_old) quadrant = -1;
              else if (x == x_old && y >  y_old) quadrant = -2;
              else if (x <  x_old && y == y_old) quadrant = -3;
              else if (x == x_old && y <  y_old) quadrant = -4;
              switch (quadrant) {
                case 1:
                  if ((y-y_old)/(x-x_old) > (y_max-y_old)/(x_max-x_old)) {
                    y_edge_1 = y_max;
                    x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
                  } else {
                    x_edge_1 = x_max;
                    y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
                  }
                  break;
                case 2:
                  if ((y-y_old)/(x_old-x) > (y_max-y_old)/(x_old-x_min)) {
                    y_edge_1 = y_max;
                    x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
                  } else {
                    x_edge_1 = x_min;
                    y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
                  }
                  break;
                case 3:
                  if ((y_old-y)/(x_old-x) > (y_old-y_min)/(x_old-x_min)) {
                    y_edge_1 = y_min;
                    x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
                  } else {
                    x_edge_1 = x_min;
                    y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
                  }
                  break;
                case 4:
                  if ((y_old-y)/(x-x_old) > (y_old-y_min)/(x_max-x_old)) {
                    y_edge_1 = y_min;
                    x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
                  } else {
                    x_edge_1 = x_max;
                    y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
                  }
                  break;
                case -1:
                  x_edge_1 = x_max;
                  y_edge_1 = y_old;
                  break;
                case -2:
                  y_edge_1 = y_max;
                  x_edge_1 = x_old;
                  break;
                case -3:
                  x_edge_1 = x_min;
                  y_edge_1 = y_old;
                  break;
                case -4:
                  y_edge_1 = y_min;
                  x_edge_1 = x_old;
                  break;
              }
              point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
              painter.drawLine(point_old, point_edge_1);
            }
          }
        }
      }

      // Draw point
      if (inside) {
        painter.drawRect(point.x()-line_width, point.y()-line_width, 
                         2*line_width, 2*line_width);
      }

      inside_old = inside ? true : false;
      x_old = x;
      y_old = y;
    }
  }
}

void TRP_Plot::drawCorrelationPoints(QPainter &painter) {

  optTimeX=false;

  QPen pen = QPen();
  pen.setWidth(line_width);

  bool inside, inside_old;

  qreal x(0), y(0), x_old(0), y_old(0), x_edge_1(0), y_edge_1(0), x_edge_2(0), y_edge_2(0);
  QPoint point, point_old, point_edge_1, point_edge_2;
  qreal t1(0), t2(0), t3(0);

  inside = false;
  inside_old = false;

  std::deque<TRP_Trend*>::const_iterator x_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator x_point;

  std::deque<TRP_Trend*>::const_iterator y_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator y_point;

  ///////////////
  //or should these be <TimePoint_IS>
  std::vector<qreal> tps;
  unsigned int tpsCount(0);

  int xCount(0);

  //push back time steps for x-axis
  //uses first item in trends.
  x_trend=trends.begin(); 

  if ((*x_trend)->begin()!=(*x_trend)->end()) {

    for (x_point=(*x_trend)->begin(); x_point!=(*x_trend)->end(); ++x_point) {
      tps.push_back((*x_point)->x());    
    }
    
    //all other trends
    y_trend=trends.begin(); ++y_trend;
    
    for (; y_trend!=trends.end(); ++y_trend) {
      
      pen.setColor((*y_trend)->getColor());      
      painter.setBrush((*y_trend)->getColor());
      painter.setPen(pen);
      
      x_point=(*x_trend)->begin();
      
      for (tpsCount=0; tpsCount<(tps.size()-1);++tpsCount){
	
	t1 = tps[tpsCount];
	t2 = tps[tpsCount+1];
	
	++xCount;    
	++x_point;
	
	//match y-axis values, which in same time stamp range as x-axis values. 	
	for (y_point=(*y_trend)->begin(); y_point!=(*y_trend)->end(); ++y_point) {
	  
	  t3 = (*y_point)->x();      
	  if (t3< t1) continue;
	  if (t3> t2) break;
	  
	  x = (*x_point)->y();
	  y = (*y_point)->y();
	  
	  ///////////
	  
	  
	  if (optLogX && x <= 0) x = x_min;
	  if (optLogY && y <= 0) y = y_min;
	  if (x >= x_min && x <= x_max && y >= y_min && y <= y_max) {
	    inside = true;
	  } else {
	    inside = false;
	  }
	  
	  if (inside) point = plotToPixel(QPointF(x, y)); 
	  if (inside_old) point_old = plotToPixel(QPointF(x_old, y_old));
	  
	  // Draw line
	  if (x_point != (*x_trend)->begin()) {
	    
	    if (!inside && !inside_old) {
	      if (!((x < x_min && x_old < x_min) ||
		    (x > x_max && x_old > x_max) ||
		    (y < y_min && y_old < y_min) ||
		    (y > y_max && y_old > y_max))){
		if (plotToPixelX(x) == plotToPixelX(x_old)) {
		  point_edge_1 = plotToPixel(QPointF(x, y_min));
		  point_edge_2 = plotToPixel(QPointF(x, y_max));
		  //painter.drawLine(point_edge_1, point_edge_2);
		} else if (plotToPixelY(y) == plotToPixelY(y_old)) {
		  point_edge_1 = plotToPixel(QPointF(x_min, y));
		  point_edge_2 = plotToPixel(QPointF(x_max, y));
		  //painter.drawLine(point_edge_1, point_edge_2);
		} else {
		  double m = (y - y_old) / (x - x_old);
		  double b = y_old - x_old * m;
		  x_edge_1 = x_min;
		  x_edge_2 = x_max;
		  y_edge_1 = m * x_min + b;
		  y_edge_2 = m * x_max + b;
		  if ( (x > x_old && m > 0.) || (x < x_old && m < 0.) ) {
		    if (y_edge_1 < y_max && y_edge_2 > y_min) {
		      if (y_edge_2 > y_max) {
			y_edge_2 = y_max;
			x_edge_2 = (y_edge_2 - b)/m;
		      }
		      if (y_edge_1 < y_min) {
			y_edge_1 = y_min;
			x_edge_1 = (y_edge_1 - b)/m;
		      }
		      point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
		      point_edge_2 = plotToPixel(QPointF(x_edge_2, y_edge_2));
		      //painter.drawLine(point_edge_1, point_edge_2);
		    } 
		  } else {
		    if (y_edge_1 > y_min && y_edge_2 < y_max) {
		      if (y_edge_2 < y_min) {
			y_edge_2 = y_min;
			x_edge_2 = (y_edge_2 - b)/m;
		      } 
		      if (y_edge_1 > y_max) {
			y_edge_1 = y_max;
			x_edge_1 = (y_edge_1 - b)/m;
		      }
		      point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
		      point_edge_2 = plotToPixel(QPointF(x_edge_2, y_edge_2));
		      //painter.drawLine(point_edge_1, point_edge_2);
		    }
		  }
		}
	      }
	    } else {
	      if (inside && inside_old) {
		//painter.drawLine(point_old, point);
	      } else {
		int quadrant(0);
		if (inside && !inside_old) {
		  if      (x < x_old && y < y_old) quadrant = 1;
		  else if (x > x_old && y < y_old) quadrant = 2;
		  else if (x > x_old && y > y_old) quadrant = 3;
		  else if (x < x_old && y > y_old) quadrant = 4;
		  else if (x <  x_old && y == y_old) quadrant = -1;
		  else if (x == x_old && y <  y_old) quadrant = -2;
		  else if (x >  x_old && y == y_old) quadrant = -3;
		  else if (x == x_old && y >  y_old) quadrant = -4;
		  switch (quadrant) {
		  case 1:
		    if ((y_old-y)/(x_old-x) > (y_max-y)/(x_max-x)) {
		      y_edge_1 = y_max;
		      x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
		    } else {
		      x_edge_1 = x_max;
		      y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
		    }
		    break;
		  case 2:
		    if ((y_old-y)/(x-x_old) > (y_max-y)/(x-x_min)) {
		      y_edge_1 = y_max;
		      x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
		    } else {
		      x_edge_1 = x_min;
		      y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
		    }
		    break;
		  case 3:
		    if ((y-y_old)/(x-x_old) > (y - y_min)/(x-x_min)) {
		      y_edge_1 = y_min;
		      x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
		    } else {
		      x_edge_1 = x_min;
		      y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
		    }
		    break;
		  case 4:
		    if ((y-y_old)/(x_old-x) > (y-y_min)/(x_max-x)) {
		      y_edge_1 = y_min;
		      x_edge_1 = x + (y_edge_1-y)*(x_old-x)/(y_old-y);
		    } else {
		      x_edge_1 = x_max;
		      y_edge_1 = y + (x_edge_1-x)*(y_old-y)/(x_old-x);
		    }
		    break;
		  case -1:
		    x_edge_1 = x_max;
		    y_edge_1 = y;
		    break;
		  case -2:
		    y_edge_1 = y_max;
		    x_edge_1 = x;
		    break;
		  case -3:
		    x_edge_1 = x_min;
		    y_edge_1 = y;
		    break;
		  case -4:
		    y_edge_1 = y_min;
		    x_edge_1 = x;
		    break;
		  }
		  point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
		  //              painter.drawLine(point_edge_1, point);
		} else if (!inside && inside_old) {
		  if      (x > x_old && y > y_old) quadrant = 1;
		  else if (x < x_old && y > y_old) quadrant = 2;
		  else if (x < x_old && y < y_old) quadrant = 3;
		  else if (x > x_old && y < y_old) quadrant = 4;
		  else if (x >  x_old && y == y_old) quadrant = -1;
		  else if (x == x_old && y >  y_old) quadrant = -2;
		  else if (x <  x_old && y == y_old) quadrant = -3;
		  else if (x == x_old && y <  y_old) quadrant = -4;
		  switch (quadrant) {
		  case 1:
		    if ((y-y_old)/(x-x_old) > (y_max-y_old)/(x_max-x_old)) {
		      y_edge_1 = y_max;
		      x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
		    } else {
		      x_edge_1 = x_max;
		      y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
		    }
		    break;
		  case 2:
		    if ((y-y_old)/(x_old-x) > (y_max-y_old)/(x_old-x_min)) {
		      y_edge_1 = y_max;
		      x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
		    } else {
		      x_edge_1 = x_min;
		      y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
		    }
		    break;
		  case 3:
		    if ((y_old-y)/(x_old-x) > (y_old-y_min)/(x_old-x_min)) {
		      y_edge_1 = y_min;
		      x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
		    } else {
		      x_edge_1 = x_min;
		      y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
		    }
		    break;
		  case 4:
		    if ((y_old-y)/(x-x_old) > (y_old-y_min)/(x_max-x_old)) {
		      y_edge_1 = y_min;
		      x_edge_1 = x_old + (y_edge_1-y_old)*(x-x_old)/(y-y_old);
		    } else {
		      x_edge_1 = x_max;
		      y_edge_1 = y_old + (x_edge_1-x_old)*(y-y_old)/(x-x_old);
		    }
		    break;
		  case -1:
		    x_edge_1 = x_max;
		    y_edge_1 = y_old;
		    break;
		  case -2:
		    y_edge_1 = y_max;
		    x_edge_1 = x_old;
		    break;
		  case -3:
		    x_edge_1 = x_min;
		    y_edge_1 = y_old;
		    break;
		  case -4:
		    y_edge_1 = y_min;
		    x_edge_1 = x_old;
		    break;
		  }
		  point_edge_1 = plotToPixel(QPointF(x_edge_1, y_edge_1));
		  //painter.drawLine(point_old, point_edge_1);
		}
	      }
	    }
	  }
	  // Draw point
	  if (inside) {
	    painter.drawRect(point.x()-line_width, point.y()-line_width, 
			     2*line_width, 2*line_width);
	  }
	  
	  inside_old = inside ? true : false;
	  x_old = x;
	  y_old = y;
	}
      }
      
    }  
    tps.clear();
  }
}


qreal TRP_Plot::calculateFirstTick(qreal axis_min, qreal axis_max, 
                                   qreal interval) const{ 
  qreal epsilon = 0.0001 * (axis_max - axis_min);

  if (int(axis_min/interval) == 0) {
    if (axis_min < epsilon) {
      return 0.;
    } else {
      return interval;
    }
  } else if (axis_min > 0.) {
    if (axis_min - interval*((int)(axis_min/interval)) < epsilon) {
      return interval*((int)(axis_min/interval));
    } else {
      return interval*((int)(axis_min/interval) + 1);
    }
  } else {
    if (axis_min - interval*((int)(axis_min/interval)-1) < epsilon) {
      return interval*((int)(axis_min/interval) - 1);
    } else {
      return interval*((int)(axis_min/interval));
    }
  }
}

/* ------------------------------------------------------------------- */
/* Functions for Coordinate Conversion                                 */
/* ------------------------------------------------------------------- */

int TRP_Plot::plotToPixelX(qreal plot_x) const {
  int pixel_x;
  if (optLogX) {
    pixel_x = qRound(left_margin 
                     + (width() - (left_margin + right_margin)) 
                     * (log10(plot_x) - log10(x_min)) 
                     / (log10(x_max) - log10(x_min)));
  } else {
    pixel_x = qRound(left_margin 
                     + (width() - (left_margin + right_margin)) 
                     * (plot_x - x_min) / (x_max - x_min));
  }
  return pixel_x;
}

int TRP_Plot::plotToPixelY(qreal plot_y) const {
  int pixel_y;
  if (optLogY) {
    pixel_y = qRound(top_margin
                     + (height() - (bottom_margin + top_margin))
                     * (log10(y_max) - log10(plot_y)) 
                     / (log10(y_max) - log10(y_min)));
  } else {
    pixel_y = qRound(top_margin
                     + (height() - (bottom_margin + top_margin))
                     * (y_max - plot_y) / (y_max - y_min));
  }
  return pixel_y;
}

QPoint TRP_Plot::plotToPixel(QPointF plotPoint) const {
  qreal plot_x = plotPoint.x();
  qreal plot_y = plotPoint.y();
  int pixel_x = plotToPixelX(plot_x);
  int pixel_y = plotToPixelY(plot_y);
  return QPoint(pixel_x, pixel_y);
}

qreal TRP_Plot::pixelToPlotX(int pixel_x) const {
  qreal plot_x;
  if (optLogX) {
    plot_x = log10(x_min)
           + ((qreal)(pixel_x - left_margin))
           * (log10(x_max) - log10(x_min)) 
           / ((qreal)(width() - (left_margin + right_margin)));
    plot_x = pow(10, plot_x);
  } else {
    plot_x = x_min 
           + ((qreal)(pixel_x - left_margin))
           * (x_max - x_min) 
           / ((qreal)(width() - (left_margin + right_margin)));
  }
  return plot_x;
}

qreal TRP_Plot::pixelToPlotY(int pixel_y) const {
  qreal plot_y;
  if (optLogY) {
    plot_y = log10(y_max)
           - ((qreal)(pixel_y - top_margin))
           * (log10(y_max) - log10(y_min))
	   / ((qreal)(height() - (bottom_margin + top_margin)));
    plot_y = pow(10, plot_y);
  } else {
    plot_y = y_max
           - ((qreal)(pixel_y - top_margin))
           * (y_max - y_min) 
	   / ((qreal)(height() - (bottom_margin + top_margin)));
  }
  return plot_y;
}

QPointF TRP_Plot::pixelToPlot(QPoint pixelPoint) const {
  int pixel_x = pixelPoint.x();
  int pixel_y = pixelPoint.y();
  qreal plot_x = pixelToPlotX(pixel_x);
  qreal plot_y = pixelToPlotY(pixel_y);
  return QPointF(plot_x, plot_y);
}

/* ------------------------------------------------------------------- */
/* Functions to Work with Trends                                       */
/* ------------------------------------------------------------------- */

void TRP_Plot::addTrend(TRP_Trend *new_trend) {
  unsigned int i_trend = trends.size() + 1;
  if ( plotTypeSelected==correlation){i_trend = trends.size();}

  QColor new_color;
  if      (i_trend == 1)   new_color = QColor(255,   0,   0); // red
  else if (i_trend == 2)   new_color = QColor(  0, 255,   0); // green
  else if (i_trend == 3)   new_color = QColor(  0,   0, 255); // blue
  else if (i_trend == 4)   new_color = QColor(  0, 255, 255); // cyan
  else if (i_trend == 5)   new_color = QColor(255,   0, 255); // magenta
  else if (i_trend == 6)   new_color = QColor(255, 191,   0); // yellow
  else if (i_trend == 7)   new_color = QColor(  0, 100,   0); // dark green
  else if (i_trend == 8)   new_color = QColor(255, 105,   0); // orange
  else if (i_trend == 9)   new_color = QColor(160,  32, 240); // purple
  else if (i_trend == 10)  new_color = QColor( 99, 184, 255); // light blue
  else if (i_trend == 11)  new_color = QColor(255, 132, 194); // pink
  else if (i_trend == 12)  new_color = QColor(200,   8,  21); // dark red
  else if (i_trend == 13)  new_color = QColor(140, 140, 140); // gray
  else if (i_trend == 14)  new_color = QColor(100,  60,  40); // brown
  else if (i_trend == 15)  new_color = QColor(  0,   0, 128); // navy
  else if (i_trend == 16)  new_color = QColor(128, 128,   0); // olive
  else if (i_trend == 17)  new_color = QColor(128,   0, 128); // dark magenta
  else if (i_trend == 18)  new_color = QColor(  0, 128, 128); // dark cyan
  else {
    new_color = QColor(rand() % 255, rand() % 255, rand() % 255);
  }
  new_trend->setColor(new_color);
  trends.push_back(new_trend);
  //legend_box->addTrend(new_trend);

  if (plotTypeSelected==standard){
    legend_box->addTrend(new_trend);
  }
  else if ( plotTypeSelected==correlation && i_trend >0){
    legend_box->addTrend(new_trend);
  }

}

TRP_Trend* TRP_Plot::trendAt(const QPoint &position) const {
  // Loop through trends in order opposite to that of drawing
  std::deque<TRP_Trend*>::const_iterator i_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;
  for (i_trend=trends.end()-1; i_trend>=trends.begin(); --i_trend) {
    qreal point_x, point_y;
    QPoint point;
    unsigned int pointcounter=0;
    for (i_point=(*i_trend)->begin(); i_point!=(*i_trend)->end(); ++i_point) {
      point_x = (*i_point)->x();
      point_y = (*i_point)->y();
      if (optLogX && (*i_point)->x() <= 0) point_x = x_min;
      if (optLogY && (*i_point)->y() <= 0) point_y = y_min;
      if (point_x >= x_min && point_x <= x_max &&
          point_y >= y_min && point_y <= y_max) {
        point = plotToPixel(QPointF(point_x, point_y));
        if (point.x()-line_width <= position.x() && 
            point.x()+line_width >= position.x() &&
            point.y()-line_width <= position.y() &&
            point.y()+line_width >= position.y()) {
            // history
            emit pointDoubleClicked(pointcounter);
          return *i_trend;
        }
      }
      pointcounter++;
    }
  }
  return NULL;
}

std::deque<TRP_Trend*> TRP_Plot::trendsIn(const QRect &rect) const {
  qreal region_x_min = pixelToPlotX(rect.left());
  qreal region_x_max = pixelToPlotX(rect.right());
  qreal region_y_min = pixelToPlotY(rect.bottom());
  qreal region_y_max = pixelToPlotY(rect.top());
  qreal point_x, point_y;
  std::deque<TRP_Trend*> trends_in_region;
  std::deque<TRP_Trend*>::const_iterator i_trend;
  std::deque<std::shared_ptr<QPointF>>::const_iterator i_point;
  for (i_trend=trends.begin(); i_trend!=trends.end(); ++i_trend) {
    for (i_point=(*i_trend)->begin(); i_point!=(*i_trend)->end(); ++i_point) {
      point_x = (*i_point)->x();
      point_y = (*i_point)->y();
      if (optLogX && (*i_point)->x() <= 0) point_x = x_min;
      if (optLogY && (*i_point)->y() <= 0) point_y = y_min;
      if (point_x >= region_x_min && point_x <= region_x_max &&
          point_y >= region_y_min && point_y <= region_y_max) {
        trends_in_region.push_back(*i_trend);
        break;
      }
    }
  }
  return trends_in_region;
}

/* ------------------------------------------------------------------- */
/* Mouse Functions                                                     */
/* ------------------------------------------------------------------- */

void TRP_Plot::mousePressEvent(QMouseEvent *event) {
  if (changingRangeX || changingRangeY || selectingRegion) {
    changingRangeX = false;
    changingRangeY = false;
    selectingRegion = false;
    update();
    return;
  }
  QPoint position = event->pos();
  if (event->button() == Qt::LeftButton) {
    if (position.x() > left_margin && 
        position.x() < (width() - right_margin) &&
        position.y() > top_margin &&
        position.y() < (height() - bottom_margin) ) { 
      QString info_text;
      const TRP_Trend *t = trendAt(position);
      if (t != NULL) {
        info_text = t->getFullName();
      }
      if (!info_text.isEmpty()) {
        info_box->setText(info_text);
        int x = position.x()-info_box->sizeHint().width();
        int y = position.y();
        if (x < 0)  {
          x = position.x();
          y = position.y()-info_box->sizeHint().height();
        }
        if (y < 0) {
          y = position.y();
        }
        info_box->move(x, y);
        info_box->raise();
        info_box->show();
      }
    } else {
      if (position.y() > (height() - bottom_margin)) {
        x_i = position.x();
        changingRangeX = true;
      } 
      if (position.x() < left_margin) {
        y_i = position.y();
        changingRangeY = true;
      }
    }
  } else if (event->button() == Qt::MidButton) {
    if (position.x() >= left_margin && 
        position.x() <= (width() - right_margin) &&
        position.y() >= top_margin &&
        position.y() <= (height() - bottom_margin) ) { 
      x_i = position.x();
      y_i = position.y();
      selectingRegion = true;
    }
  }

}

void TRP_Plot::mouseMoveEvent(QMouseEvent *event) {
  QPoint position = event->pos();
  if (event->buttons() & Qt::LeftButton) {
    if (changingRangeX) {
      if (position.y() > (height() - bottom_margin)) {
        x_f = position.x();
        update();
      } else {
        changingRangeX = false;
        update();
      }
    } 
    if (changingRangeY) {
      if (position.x() < left_margin) {
        y_f = position.y();
        update();
      } else {
        changingRangeY = false;
        update();
      }
    }
  } else if (event->buttons() & Qt::MidButton) {
    if (selectingRegion) {
      if (position.x() >= left_margin && 
          position.x() <= (width() - right_margin) &&
          position.y() >= top_margin &&
          position.y() <= (height() - bottom_margin) ) { 
        x_f = position.x();
        y_f = position.y();
        update();
      } else {
        selectingRegion = false;
        update();
      }
    }
  }
}

void TRP_Plot::mouseReleaseEvent(QMouseEvent *event) {
  QPoint position = event->pos();
  if (event->button() == Qt::LeftButton) {
    if (!info_box->isHidden()) {
      info_box->hide();
    } else if (changingRangeX) {
      changingRangeX = false;
      if (position.y() > (height() - bottom_margin)) {
        x_f = position.x();
        qreal p_i = pixelToPlotX(x_i);
        qreal p_f = pixelToPlotX(x_f);
        if (x_i >= left_margin && x_i <= width()-right_margin &&
            x_f >= left_margin && x_f <= width()-right_margin) {
          if (abs(x_f - x_i) > 1 &&
              (!optTimeX || fabs(p_f - p_i) > 3)) {
            defaultMinX = false;
            defaultMaxX = false;
            setRangeX(p_i, p_f);
          } else {
            update();
          }
        } else if (x_i < left_margin && 
                   x_f >= left_margin && x_f <= width()-right_margin) {
          if (x_f - left_margin > 1 &&
              (!optTimeX || p_f - x_min > 3)) {
            defaultMinX = false;
            defaultMaxX = false;
            setRangeX(x_min, p_f);
          } else {
            update();
          }
        } else if (x_f < left_margin && 
                   x_i >= left_margin && x_i <= width()-right_margin) {
          if (x_i - left_margin > 1 &&
              (!optTimeX || p_i - x_min > 3)) {
            defaultMinX = false;
            defaultMaxX = false;
            setRangeX(x_min, p_i);
          } else {
            update();
          }
        } else if (x_i >= left_margin && x_i <= width()-right_margin && 
                   x_f > width()-right_margin) {
          if (width() - right_margin - x_i > 1 &&
              (!optTimeX || x_max - p_i > 3)) {
            defaultMinX = false;
            setRangeX(p_i, x_max);
          } else {
            update();
          }
        } else if (x_f >= left_margin && x_f <= width()-right_margin &&
                   x_i > width()-right_margin) {
          if (width() - right_margin - x_f > 1 &&
              (!optTimeX || x_max - p_f > 3)) {
            defaultMinX = false;
            setRangeX(p_f, x_max);
          } else {
            update();
          }
        }
      } else {
        update();
      }

      if (optTimeX && defaultMaxX == false) {
        defaultMinY = false;
        defaultMaxY = false;
      }

    } else if (changingRangeY) {
      changingRangeY = false;
      if (position.x() < left_margin) {
        y_f = position.y();
        qreal p_i = pixelToPlotY(y_i);
        qreal p_f = pixelToPlotY(y_f);
        if (y_i >= top_margin && y_i <= height()-bottom_margin &&
            y_f >= top_margin && y_f <= height()-bottom_margin) {
          if (abs(y_f - y_i) > 1 &&
              (!optTimeY || fabs(p_f - p_i) > 3)) {
            defaultMinY = false;
            defaultMaxY = false;
            setRangeY(p_i, p_f);
          } else {
            update();
          }
        } else if (y_i >= top_margin && y_i <= height()-bottom_margin &&
                   y_f > height()-bottom_margin) {
          if (height() - bottom_margin - y_i > 1 &&
              (!optTimeY || p_i - y_min > 3)) {
            defaultMinY = false;
            defaultMaxY = false;
            setRangeY(y_min, p_i);
          } else {
            update();
          }
        } else if (y_f >= top_margin && y_f <= height()-bottom_margin &&
                   y_i > height()-bottom_margin) {
          if (height() - bottom_margin - y_f > 1 &&
              (!optTimeY || p_f - y_min > 3)) {
            defaultMinY = false;
            defaultMaxY = false;
            setRangeY(y_min, p_f);
          } else {
            update();
          }
        } else if (y_i < top_margin && 
                   y_f >= top_margin && y_f <= height()-bottom_margin) {
          if (y_f - top_margin > 1 &&
              (!optTimeY || y_max - p_f > 3)) {
            defaultMinY = false;
            setRangeY(p_f, y_max);
          } else {
            update();
          }
        } else if (y_f < top_margin && 
                   y_i >= top_margin && y_i <= height()-bottom_margin) {
          if (y_i - top_margin > 1 &&
              (!optTimeY || y_max - p_i > 3)) {
            defaultMinY = false;
            setRangeY(p_i, y_max);
          } else {
            update();
          }
        }
      } else {
        update();
      }

      if (optTimeY && defaultMaxY == false) {
        defaultMinX = false;
        defaultMaxX = false;
      }

    }
  } else if (event->button() == Qt::MidButton) {
    if (selectingRegion) {
      selectingRegion = false;
      update();
      if (position.x() >= left_margin && 
          position.x() <= (width() - right_margin) &&
          position.y() >= top_margin &&
          position.y() <= (height() - bottom_margin) ) { 
        x_f = position.x();
        y_f = position.y();
        if (x_i != x_f && y_i != y_f) {
          QRect rect;
          if (x_i < x_f) {
            if (y_i < y_f) {
              rect.setCoords (x_i, y_i, x_f, y_f);
            } else {
              rect.setCoords (x_i, y_f, x_f, y_i);
            }
          } else {
            if (y_i < y_f) {
              rect.setCoords (x_f, y_i, x_i, y_f);
            } else {
              rect.setCoords (x_f, y_f, x_i, y_i);
            }
          }

          std::deque<TRP_Trend*> trends_in_region = trendsIn(rect);

          QString header_text, main_text;

          if (trends_in_region.size() == 0) {
            header_text = "The selected region does not contain any points.";
          } else if (trends_in_region.size() == 1) {
            header_text = "The selected region contains the trend: ";
          } else if (trends_in_region.size() < 20) {
            header_text = "The selected region contains the trends: ";
          } else {
            header_text = QString("The selected region contains %1 trends.")
                          .arg(trends_in_region.size());
          }

          std::deque<TRP_Trend*>::const_iterator i_trend;
          for (i_trend  = trends_in_region.begin(); 
               i_trend != trends_in_region.end(); 
               ++i_trend) {
            if (i_trend != trends_in_region.begin()) {
              main_text += '\n';
            }
            main_text += (*i_trend)->getFullName();
          }

          QMessageBox message(this);
          message.setWindowTitle("Information");
          message.setText(header_text);
          if (trends_in_region.size() < 20) {
            message.setInformativeText(main_text);
          } else {
            message.setInformativeText
             ("Click 'Show Details...' to view the list of trends.");
            message.setDetailedText(main_text);
          }
          message.exec();
        }
      }
    }
  }
}
// history

void TRP_Plot::setDiffStep(int step) {
  if (step == optDiffStep) return;
  if (step < -2 || step > 2) return;
  optDiffStep = step;
  if (optDiffStep == -2) {
    if (setDiffStep1Action->isChecked() == false) {
      setDiffStep1Action->setChecked(true);
    }
  } else if (optDiffStep == -1) {
    if (setDiffStep2Action->isChecked() == false) {
      setDiffStep2Action->setChecked(true);
    }
  } else if (optDiffStep == 0) {
    if (setDiffStep3Action->isChecked() == false) {
      setDiffStep3Action->setChecked(true);
    }
  } else if (optDiffStep == 1) {
    if (setDiffStep4Action->isChecked() == false) {
      setDiffStep4Action->setChecked(true);
    }
  } else if (optDiffStep == 2) {
    if (setDiffStep5Action->isChecked() == false) {
      setDiffStep5Action->setChecked(true);
    }
  }
  emit diffStepChanged();
  update();
}
