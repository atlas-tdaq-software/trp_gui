#include <cmath>
#include <iostream>
#include "trp_utility.h"

void toScientificNotation(double value, double &base, int &exponent) {
  double sign = 1.0;
  if (value < 0.) {
    sign = -1.0;
    value = -value;
  }

  exponent = (int)floor(log10(value));

  base = sign * value / pow(10,exponent);
}

/*
bool sortPointX(const QPointF *p1, const QPointF *p2) {
  return p1->x() < p2->x();
}
*/

bool sortPointX(std::shared_ptr<QPointF> p1, std::shared_ptr<QPointF> p2) {
  return p1->x() < p2->x();
}
