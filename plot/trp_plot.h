/** @file trp_plot.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief TRP_Plot: A plot widget with two axes
 */

#ifndef TRP_PLOT_H
#define TRP_PLOT_H

#include <time.h>
#include <iostream>

#include <deque>
#include <QWidget>
#include "trp_trend.h"

class QMenu;
class QLabel;
class TRP_Legend;

class TRP_Plot : public QWidget {
  Q_OBJECT

public:

  enum plotType{standard,correlation};
  enum dataSource{conf,root};

  TRP_Plot(QWidget *parent = 0, TRP_Plot::plotType selectedPlotType=standard, TRP_Plot::dataSource selectedDataSource=conf);

  //  TRP_Plot(QWidget *parent = 0);
  virtual ~TRP_Plot();

private: 
  TRP_Plot(const TRP_Plot & , TRP_Plot::plotType selectedPlotType=standard, TRP_Plot::dataSource selectedDataSource=conf);
  //TRP_Plot(const TRP_Plot &);
  TRP_Plot& operator = (const TRP_Plot &);

  plotType plotTypeSelected;
  dataSource dataSourceSelected;

public:

  dataSource getDataSource(){return dataSourceSelected;}
  void setDataSource(int option){if(option==0){dataSourceSelected=conf;} if(option==1){dataSourceSelected=root;}}

  void addTrend(TRP_Trend *new_trend);

  std::deque<TRP_Trend*>::const_iterator begin() const {
    return trends.begin();
  }
  std::deque<TRP_Trend*>::const_iterator end() const {
    return trends.end();
  }

  unsigned int getTextSize() const {return optTextSize;}
  int getMinHeight() const {return height_min;}
  int getMinWidth() const {return width_min;}
  QString getTitle() const {return title;}
  QString getUnit() const {return unit;}

public slots:
  void setRangeX(qreal min, qreal max);
  void setRangeY(qreal min, qreal max);
  void setTitle(QString string);
  void setLabelX(QString string);
  void setLabelY(QString string);
  void setLabels(QString x_string, QString y_string);
  void setTextSizes();

  void showLegend(bool opt);  
  void showGrid(bool opt);
  void setLogX(bool opt);
  void setLogY(bool opt);
  void setTimeX(bool opt);
  void setTimeY(bool opt);
  void setDefaultRanges();
  void setTextSize(unsigned int size);

private slots:
  void saveImage();
  void setTextSize1() {setTextSize(1);}
  void setTextSize2() {setTextSize(2);}
  void setTextSize3() {setTextSize(3);}
  void setTextSize4() {setTextSize(4);}
  void setTextSize5() {setTextSize(5);}
  void setTextSize6() {setTextSize(6);}

signals:
  void textSizeChanged();
  void cloneRequest();
  void marginsChanged(int left, int right, int top, int bottom);

protected:
  void paintEvent(QPaintEvent *event);
  void contextMenuEvent(QContextMenuEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);

private:
  void createActionsAndMenus();

  void checkAxes();
  void checkCorrelationAxes();
  void checkAxesLabels();
  void checkCorrelationAxesLabels();
  void drawAxes(QPainter &painter);
  void drawAxisValues(QPainter &painter, QString axis);
  void drawTitle(QPainter &painter);
  void drawPoints(QPainter &painter);
  void drawCorrelationPoints(QPainter &painter);

  qreal calculateFirstTick(qreal axis_min, qreal axis_max, 
                           qreal interval) const;

  void ensureRangeSanity();

  int plotToPixelX(qreal plotPoint) const;
  int plotToPixelY(qreal plotPoint) const;
  QPoint plotToPixel(QPointF plotPoint) const;
  qreal pixelToPlotX(int pixelPoint) const;
  qreal pixelToPlotY(int pixelPoint) const;
  QPointF pixelToPlot(QPoint pixelPoint) const;

  TRP_Trend* trendAt(const QPoint &position) const;
  std::deque<TRP_Trend*> trendsIn(const QRect &rect) const; 

  int height_min, width_min;
  int left_margin, right_margin, bottom_margin, raw_top_margin, top_margin;
  int tick_length, line_width;
  int text_rect_size;
  QFont axis_value_font, axis_label_font;

  unsigned int optTextSize;
  bool optShowLegend, optShowGrid, optTimeX, optTimeY, optLogX, optLogY;

  qreal x_min, x_max, y_min, y_max;
  QString title, x_label, y_label, unit;

  bool defaultMinX, defaultMaxX, defaultMinY, defaultMaxY;
  bool changingRangeX, changingRangeY, selectingRegion;
  int x_i, x_f, y_i, y_f;

  std::deque<TRP_Trend*> trends;

  QMenu *rightClickMenu;
  QAction *showLegendAction;  
  QAction *showGridAction;
  QAction *setLogYAction, *setDefaultRangeAction;
  QAction *setTextSize1Action, *setTextSize2Action, *setTextSize3Action,
          *setTextSize4Action, *setTextSize5Action, *setTextSize6Action;


  QLabel *info_box;

  TRP_Legend* legend_box;

// history
public:
   int getDiffStep() const {return optDiffStep;}
public slots:
  void setDiffStep(int step);
private slots:
  void setDiffStep1() {setDiffStep(-2);}
  void setDiffStep2() {setDiffStep(-1);}
  void setDiffStep3() {setDiffStep(0);}
  void setDiffStep4() {setDiffStep(1);}
  void setDiffStep5() {setDiffStep(2);}
signals:
  void diffStepChanged();
  void pointDoubleClicked(unsigned int) const;
private:
  int optDiffStep;
  QAction *setDiffStep1Action, *setDiffStep2Action, *setDiffStep3Action,
          *setDiffStep4Action, *setDiffStep5Action;
};

#endif  // define TRP_PLOT_H

