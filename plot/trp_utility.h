/** @file trp_utility.h
 *  @author Hulya Guler (Hulya.Guler@cern.ch)
 *  @brief Utilities for TRP
 */

#ifndef TRP_UTILITY_H
#define TRP_UTILITY_H

#include <cmath>
#include <memory>
#include <QPointF>

const double phi = (1. + sqrt(5.)) / 2.;  // golden ratio is 1:phi

void toScientificNotation(double value, double &base, int &exponent);

// bool sortPointX(const QPointF *p1, const QPointF *p2);
bool sortPointX(std::shared_ptr<QPointF>, std::shared_ptr<QPointF>);

#endif  // define TRP_UTILITY_H

