#include <QPointF>
#include <QColor>

#include "trp_isthread.h"
#include "trp_trend.h"
#include "trp_utility.h"

#include <algorithm>

TRP_Trend::TRP_Trend(QString nx, QString ny, TRP_TPDataPtr t) {
  if (t == NULL) {
    reads_from_thread = false;
  } else {
    reads_from_thread = true;
  }
  thread = t;
  name_x = nx;
  name_y = ny;
  color = Qt::black;
}

TRP_Trend::~TRP_Trend() {
  // deletePoints();
}

/*
void TRP_Trend::deletePoints() {
  std::deque<std::shared_ptr<QPointF>>::const_iterator it;
  for (it = points.begin(); it != points.end(); ++it) {
    delete *it;
  }
  points.clear();
}
*/

void TRP_Trend::setPoints(std::deque<std::shared_ptr<QPointF>> new_points) {
  // deletePoints();
  m_points.clear();
  m_points = new_points;
  std::sort(m_points.begin(), m_points.end(), sortPointX);
}

void TRP_Trend::updatePoints() {
  if (reads_from_thread == false) return;
  // deletePoints();
  m_points.clear();
  thread->getPoints(name_x, name_y, m_points);
  std::sort(m_points.begin(), m_points.end(), sortPointX);
}

QString TRP_Trend::getFullName() const {
  QString full_name = "";
  if (reads_from_thread) {
    full_name += thread->getAlias();
    full_name += ": ";
  }
  if (!(name_x.isEmpty() && name_y.isEmpty())) {
    full_name += name_x + ", " + name_y;
  }
  return full_name;
}
QString TRP_Trend::getUnitName() const {

  QString unit_name = "";
  if (reads_from_thread) {
    unit_name = thread->getUnit();
   }
  return unit_name;
}


std::ostream& operator << (std::ostream& ostr, const TRP_Trend t){
  ostr << "TRP_Trend: " << t.getFullName().toStdString();
  return ostr;
}
