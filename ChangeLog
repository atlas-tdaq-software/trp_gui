2016-06-20   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* shift/trp_progressdialogwidget.h (QThread): new code to run
	a progress dialog widget in a sperate thread.

	* shift/trp_rootfilereader.cpp (readRootFile): add a dialog box
	to show the progress of looping through root branches. This can take
	a considerbale time.

	* shift/trp_rootfilereader.h (QObject): add 2 signals to 
	communicate with the progress dialog thread
	
	* shift/trp_mainwindow.cpp (runRootFileReaders): run progressDialogWidget
	in a separate thread.

	* shift/trp_mainwindow.h (QMainWindow): added 2 more signals
	to communicate with the progress dialog thread

2016-06-18   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* shift/trp_rootfilereader.cpp (readRootFile): 
	removed some debug output as doen in the caller.

2016-06-16   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* separated TRP_ISThread and TRP_RootThreadReader.
	Root file appears to be reading ok.

2016-06-15   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* TRP_TPData now sent to TRP_Trend onjects. Lots of 
	code vitied on the way: reformatted and/pr cleaned up.

	TRP_IPCThread sees the TRP_ISThreads, the data displayers
	only see the TRP_TPData objects.

	Code ran at P1, showed data.

	Root file reding on lxplus seemed to hang - possibly
	a DB problem.

	Next: removed the TRP_ISThread forwards to the TRP_TPData
	attribute.

	* shift/trp_tpdata.cpp: added more mutex protection
	while accessing the time point buffers.

	* Pushed the time point handling in TRP_ISThread into 
	TRP_TP_Data. TRP_ISThread uses this through composition.
	Added better mutex protection, and some diagnostics.

	The idea is to send have ROOT file reading and IS listening
	done by different classes (thus disentangling the two), where
	they both use a TRP_TPdata instance. This will be sent 
	to the objects reponsible for data display, which avoids
	the need for polymorphism. Trying to use a class heirarchy
	to separate ROOT file reading and IS listening has proven to 
	be difficult.

	So far, the code runs, and prints out debug info from TRP_TPData.
	The next steps are 1) send the TRP_TPData instance to the client
	functions instead of TRP_ISThread and 2) Place the ROOT file reading
	into a separate class which also uses a TRP_TPData instance.

2016-06-13   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* shift/trp_isthread.cpp (TRP_ISThread): Generalised signal handling.
	The handlers set up by ROOT are removed. For thr the durration of
	the ROOT file reading, SignalHandler is used to handle all signals.
	The default signal handlers are reinstalled after ROOT file reading.


	(TRP_ISThread): disintall ROOT handlers

	(global gSignalStatus) stores the signal number, can be read by
	the TRP_ISThread

	(signalHandler): On receipt of a thread, sets a global gSignalStatus.
	This does not work for SIGFPE (writing to gSignalStatus causes an
	infiintie loop). If this signal is received, exit.
	
	(class SignalRouter): Sets the signal handlers to SignalHandler on
	construction, resets previopus handlers on destruction.
.
	(run): Instantiate SignalRouter to install/disintall SignalHandler
	during ROOT file reading, Add checks as to whether a signal has 
	been received. Terminate the therad if it has.

	
2016-06-12   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* shift/trp_isthread.cpp (TRP_ISThread):
	Added code suggested by S Kolos to prevent hangs. Apparantly
	ROOT tried to run the gdb debugger on some ROOT error, possibly
	an attempt to read a corrupt file. GDB does not come up and the thread
	hangs. See JIRA report ATR-14233.

2016-06-05   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>
	* Moving towards being able to disentangle reading root files
	from reading from IS. This will mean separate classes for
	the two functionalities.
	
	* shift/trp_isthread.cpp: used shared pointers for TimePoints
	(store_unique_timepoints): new private method - handles 
	checking of duplicate timepoints.

	* shift/trp_isthread.h (QThread): used shared pointers for TimePoints

2016-06-04   Peter Sherwood  <peter@MacBook-Pro-7.fritz.box>

	* shift/trp_isthread.cpp (run): If calling
	readRootFile throws, return rather than blindly carrying on.

	* shift/trp_mainwindow.cpp (openFileLocallyDialog): 
	simplify logic, add a try block when attempting
	to open a Root File. make error messages more explicit.

2015-09-29  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_isthread.cpp:
        * Remove debug printout statement
	* trp_gui-00-00-39

2015-07-07  Ivana Hristova <ivana.hristova@cern.ch>
        * shift/trp_isthread.cpp:
	* Adapt ROOT file upload to new branch
        * trp_gui-00-00-38

2015-06-26  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_mainwindow.cpp, shift/trp_config.cpp:
	* Edited "Confgurarion error" popup window
	 - Degraded error to issue and added Press OK to continue 
	   because some users did not continue 
	* trp_gui-00-00-37

2015-06-14  Frank Winklmeier  <fwinkl@cern>
	* share/draw_rates.py: Many fixes for run-2 and general cleanup
	* trp_gui-00-00-36

2015-05-26  Ivana Hristova <ivana.hristova@cern.ch>
	* share/trp_gui_conf.xml: added configuration for test folders
	* shift/trp_mainwindow.cpp: added a check for HLT_Rate folder
	* shift/trp_isthread.cpp: fix trpgui crash (ATR-11282)
	* trp_gui-00-00-35

2015-04-29  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_config.cpp 
	 - fix default configuration file path (ATR-9557)
	 - trp_gui-00-00-34 (2015-05-16)

2015-03-25  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_tablepage.cpp: remove debug information
	* share/trp_gui_conf.xml: update default configuration for post-M9 operation
	* trp_gui-00-00-33

2015-03-14  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_table.cpp, trp_tablepage.h, trp_base_model.cpp, trp_table.h
	 - fix adverse interference between L1 Enabled checkbox and Filter line
	* trp_gui-00-00-32

2015-03-13  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_table.cpp
	 - account for non-integer L1 prescale values 
	 - suppress (unneeded) call to applyFilter
	* shift/trp_mainwindow.cpp
	 - temporary workaround for L1 table showing zero rates at start up
	 
2015-02-15  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_tablepage.cpp: fix plotting problem introduced when Chains table was enabled
	* trp_gui-00-00-31

2015-02-15  Ivana Hristova <ivana.hristova@cern.ch>
	* share: adjusted column widths, may need to be tuned further
	* trp_gui-00-00-30

2015-02-15  Ivana Hristova <ivana.hristova@cern.ch>
	* share: updated configuration file (version M8)
	* trp_gui-00-00-29

2015-02-15  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_tablepage, shift/trp_table
	* shift/trp_base_model, shift/trp_chain_model
	 - Enabled the Chains table
	 - Migrated to Run 2 menu from TRIGGERDBR2
	* trp_gui-00-00-28

2015-01-20  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_config: allow to find default configuration file from installed area at runtime
	* trp_gui-00-00-27

2014-12-12  Ivana Hristova <ivana.hristova@cern.ch>
	* cmt/requirements: remove 'set QTDIR' as wrong on -dbg platform
	* trp_gui-00-00-26

2014-12-08  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_config: fix TRP crashes without configuration file (ATR-9557)
	* trp_gui-00-00-25

2014-12-03  Ivana Hristova <ivana.hristova@cern.ch>
	* plot/trp_tablepage, plot/trp_table: 
	 - fix prescale values for disabled L1 items (ATR-9553)
	 - add checkbox to show only enabled L1 items
	* trp_gui-00-00-24

2014-09-05  Ivana Hristova <ivana.hristova@cern.ch>
	* plot/trp_plot.cpp: fixes crash in TR9 with tdaq-05-04-00, tag for M5
	* trp_gui-00-00-23

2014-03-10  Ivana Hristova <ivana.hristova@cern.ch>
	* Fix GUI crash on startup (Qt-4.8.4) (bug #104449)

2014-03-10  Ivana Hristova <ivana.hristova@cern.ch>
	* Fix crash at Table item selection (Qt-4.8.4)
	* trp_model.cpp: 
	 - replace signal layoutChanged() -> layoutAboutToBeChanged()
	 - emit layoutChanged() in update()
	* trp_table.cpp: catch layoutAboutToBeChanged() and call update()

2014-01-14  Ivana Hristova <ivana.hristova@cern.ch>
	* Fixed ./cmt/requirements
	- binary.trp -> binary.trpgui
	- updated twiki url

2013-09-12  Ivana Hristova <ivana.hristova@cern.ch>
	* Fix deletePlots function to prevent crashes

2013-08-21  Raphaël Prentki <raphael.prentki@cern.ch>
	More work required to fix deletePlots function.  Currently trp crashes when:
	close ntuple run tables tab, then close the trp.
	close the ntuple run plots tab, then close the ntuple run tables tab.
	
	* trp_mainwindow.cpp, trp_mainwindow.h
	A new tab containing plots created when loading an archived run n-tuple.
	Name change from "threads" to "branches" in pop-up window when opening an archived run n-tuple.
	Display an error window when selecting no branches from an archived run n-tuple.
	Pop-up windows for empty plot & empty correlation plot request now both warnings.
	Modified deletePlots() to avoid crash when closing an archived run tables tab while many plot pop-up windows open.
	Removed cancel button in branch list window when opening an archived run n-tuple, as it would act just as the Ok button.
	* trp_plot.cpp, trp_plot.h:
	ShowLegend() fixed.  Implemented dataSource, getDataSource() and setDataSource() as to prevent the plots contained in an archived run plots tab to be deleted when closing the corresponding archived run tables tab
	* general: compatible with SLC6

2012-07-19  Ivana Hristova <ivana.hristova@cern.ch>
	* shift/trp_mainwindow, shift/trp_isthread:
	*  bug fix in constructor initialisation (#93742) 

2013-01-10  Sue Cheatham <susan.cheatham@cern.ch> 
	* trp_table.cpp filtered plotting.
	* trp_plot.cpp bug fix:prevent crash when display correlation plots tab when no data. 
	* trp_mainwindow.cpp bug fix:prevent crash if someone selects zero branches when reading ntuple.
	* trp_gui-00-00-16

	
2012-12-19  Sue Cheatham <susan.cheatham@cern.ch> 
	* trp_config.cpp, trp_mainwindow.cpp Correct tab labelling when config file not specified and using ROOT file.
	* trp_gui-00-00-15
	
2012-12-17  Sue Cheatham <susan.cheatham@cern.ch> 
	* trp_config.cpp .h  Correlation Plot Tab added.  Configurable from config file. 
	* trp_mainwindow.cpp  createPlotPages has possibility for correlation plots tab.
	* trp_plot.cpp correlation plot colours now inline with standard plots.
	* trp_gui-00-00-14
	
2012-11-23  Sue Cheatham <susan.cheatham@cern.ch> 
	* trp_table.cpp Revert to previous filtering method.  Quicker.
	* trp_plot.cpp  Correct order to draw axes values.
	* trp_gui-00-00-13

2012-11-21 Sue Cheatham <susan.cheatham@cern.ch>
	* trp_basemodel.cpp:  Color coded prediction flags now also have text.
	* trp_plot.cpp, trp_table.cpp, trp_table_page.cpp Correlation plot improved to support  multiple trend plotting, with double click to select y-axis trend.  Date also displayed on plot.
	* trp_help.html Correlation plotting explained for help.
	* trp_table.cpp Filtering method for chains table allows filtered plotting.  Hopefully quick enough.
	* trp_preview.cpp Legend for correlation plot saving.
	* trp_gui-00-00-12
	
2012-11-15  Sue Cheatham <susan.cheatham@cern.ch>
	* trp_isthread.cpp/.h Added thread_unit
	* trp_mainwindow.cpp  Thread constructor now includes unit
	* trp_basemodel.cpp:  Color coded prediction flags
	* trp_tablepage.cpp, trp_mainwindow.cpp, trp_plot:  Very basic correlation plot.  Essentially placeholder.  Will be improved. 
	* trp_table.cpp:      Filtering method now supports filtered plotting.  But on chains table, filtering on all columns, so slow.  Will be improved.
	* trp_gui-00-00-11
	
2012-09-14  Ivana Hristova <ivana.hristova@cern.ch>
	* share/draw_rates.py: access to archived rates restricted to CoCa (EOS)
	* Storage on the DQM server/AFS will be possibly discontinued
	* trp_gui-00-00-10

2012-09-04  Sue Cheatham <susan.cheatham@cern.ch>
	* trp_chain_model.cpp/.h: Stream information reinstated.  TRP package needs to be checked out and compiled.

2012-09-03  Sue Cheatham <susan.cheatham@cern.ch>
	* trp_chain_model.cpp/.h: Stream information removed from Chain table.
	* trp_table.cpp: Filtering on all columns returned to previous startegy - faster, but filtered plotting not work.

2012-08-30  Martin Spangenberg <martin.spangenberg@cern.ch>
	* trp_mainwindow.cpp, trp_tabwidget.cpp: Fixed the deletion of tablepage data when closing multiple tabs.
	* trp_mainwindow.cpp: Replaced the history_point spinbox with QDateTime and lumiblock spinboxes.
	  Changing the value in a spinbox calls a function to search for the nearest history point to be displayed.
	  It automatically updates tha value in the other spinbox.
	* Added progress bar when loading .root files.

2012-08-24  Sue Cheatham <susan.cheatham@cern.ch>
	* trp_chain_model.cpp/.h: Stream information added to Chain table.  Type added to Stream Name, using new map in TRP confadapter.
		* trp_table.cpp: Filtering on all columns enabled only on chains table.  

2012-08-23  Frank Winklmeier <fwinkl@cern>
	* Add newlines to help message

2012-08-15 2012-08-13  Martin Spangenberg <martin.spangenberg@cern.ch>
    * trp_isthread.cpp: Fixed a bug in the reading of branches from .root files. The
	  constructor of TimePoint_IS gives Data.at(0) a value of -1. This is now removed in
	  the function that loads branches.

2012-08-15  Sue Cheatham <susan.cheatham@cern.ch>
    * trp_table.cpp: Regular expression filtering now works.
	
2012-08-13  Martin Spangenberg <martin.spangenberg@cern.ch>
    * trp_mainwindow.cpp/.h, trp_tablepage.cpp: Fixed a bug where clicking a history point
	  in a plot caused a crash.
	* trp_mainwindow.cpp/.h: Extended "plots" to be a map over both tableID and thread number.

2012-08-10  Sue Cheatham <susan.cheatham@cern.ch>
    * trp_table.cpp/.h: If multiple table items selected after filtering, just filtered items plotted. 
    * trp_base_model.cpp: Hide disabled check box removed.
	
2012-08-08  Martin Spangenberg <martin.spangenberg@cern.ch>
    * trp_mainwindow.cpp/.h: Added popup menu when loading a .root file for choosing
	  which threads to load.

2012-08-06  Martin Spangenberg <martin.spangenberg@cern.ch>
    * added custom trp_tabwidget.cpp/.h for further functionality
	* trp_mainwindow.cpp/.h: Added menus to open .root files through MDA or locally.
	  A map of vectors with pointers to all threads in each TRP_TablePage has been added,
	  and the tab index of the current tablepage is used as an argument (tableID) to the makePlot function.
	* trp_isthread.cpp/.h: Overloaded constructor to be able to make a thread from a .root file
	  and added functionality to load a ROOT tree into a TimePoint_IS class
	* trp_tablepage.cpp/.h: int tableID added to class for use in makePlot function.
	* main.cpp: Added the ability to load root file from command line through -f [RunNumber]
	* requirements: Added "use mda" as well as "-lmda_read_back" under common_linkopts
	
2012-07-19  Ivana Hristova <ivana.hristova@cern.ch>
	* share/draw_rates.py: fix for P1 
	* crash was due to attempt to access external DQM server 

2012-06-29  Martin Spangenberg <martin.spangenberg@cern.ch>
	* shift/trpconfig.cpp, shift/trpconfig.h: trpgui now runs without specifying a config file

2012-05-02  Ivana Hristova <ivana.hristova@cern.ch>
	* share/draw_rates.py: added wget to download root files from the https DQ server

2012-04-13  Ivana Hristova <ivana.hristova@cern.ch>
	* share/draw_rates.py: imported script from the TRP package
	* cmt/requirements: added statement to install the script

2012-01-25 Ivana Hristova <ivana.hristova@cern.ch>
	* cmt/requirements:  attempt to fix compilation error due to undefined 
	*  reference to library trpdata
	* trp_gui-00-00-04

2011-11-01  Ivana Hristova <ivana.hristova@cern.ch>
	* renamed library and executable to not overwrite the TRP ones 
	* trp_gui-00-00-03

2011-11-01  Ivana Hristova <ivana.hristova@cern.ch>
	* Added in chain aware tables (from Nathan Triplett <triplett@cern.ch>)
	* cmt/requirements: renamed library and executable to not overwrite the TRP ones
	* trp_gui-00-00-02

2011-09-07  Ivana Hristova <ivana.hristova@cern.ch>
	* cmt/requirements: use TRP package to fix dependencies
	* shift/trp_isthread.cpp, shift/trp_model.h: use header files from the TRP package
	* trp_gui-00-00-01

2011-09-01  Ivana Hristova <ivana.hristova@cern.ch>
	* Initial commit: TRP/gui separated into a new package trp_gui
	* trp_gui-00-00-00
